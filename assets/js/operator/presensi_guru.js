let baseUrl = `${window.origin}/presensi`
$(document).ready(function () {
    komponenTabel()
    button()
    tabel_data.api().ajax.reload()
    get_edit()
})

let tabel_data = $('#tabel_data').dataTable({
    "processing": true,
    "serverSide": true,

    "ajax": {
        "url": `${baseUrl}/operator/presensi_guru/datatables`,
        "type": "POST",
        "data": function (data) {
            data.tanggal    = moment($('#tanggal').val(), 'DD-MM-YYYY').format("YYYY-MM-DD");
            data.hari       = $('#hari').val();
            data.pengajar   = $('#pengajar').val();
            data.kelas   	= $('#kelas').val();
        }
    },
    "bPaginate": true,
    "bLengthChange": false,
    "bFilter": false,
    "bInfo": true,
    "stateSave": true,
    "bAutoWidth": false,
    "ordering": false,
    "pageLength": 12,
    "pagingType": "simple_numbers",
    "scrollX": true,
    "scrollY": "67vh",
    "columnDefs": [
        { className: "text-center", "targets": [1,5] }
    ],
});

function komponenTabel(){
	if (!$('#tanggal').val()) $('#tanggal').val(moment().format('DD-MM-YYYY'))
	$('#tanggal').datetimepicker({
		format: 'DD-MM-YYYY'
	}).on('dp.change', function (e) {
		$("#tanggal").data("DateTimePicker").hide();
        ambil_hari()
        tabel_data.api().ajax.reload()
	})

    ambil_hari()
    ambil_kelas()
	ambil_pengajar()
    get_cari()
}

function get_edit(){
	$('#e_no_urut').change((e) => {
		let e_hadir = $(`#e_hadir`).val()
		let form_absen = $("#form_absen").serialize();

		if(e_hadir){
			$.ajax({
				method: 'POST',
				dataType: 'JSON',
				data: form_absen,
				url: `${baseUrl}/operator/presensi_guru/update`,
				success: function (result) {
					if (result.status == 200) {
						tabel_data.api().ajax.reload(null, false)
						$('#modal').modal('hide')
						$('#e_no_urut').val('').trigger('change')
						$('#e_tanggal').val('')
						$('#e_kelas').val('')
						$('#e_pengajar').val('')
						$('#e_jam').val('')
						$('#e_hadir').val('')
					}else{
						Swal.fire({
							icon: 'warning',
							title: `${result.keterangan}`,
						})
					}
				}
			})
		}
    })
}

function ambil_hari(){
    let tanggal     = $(`#tanggal`).val()

    $.ajax({
        method: 'POST',
        dataType: 'JSON',
        async: false,
        data: { tanggal },
        url: `${baseUrl}/operator/presensi_guru/ambil_hari`,
        success: function (data) {
            $(`#hari`).val(data.data)
        }
    })
}

function ambil_pengajar(){
    $.ajax({
        method: 'GET',
        dataType: 'JSON',
        url: `${baseUrl}/operator/presensi_guru/ambil_pengajar`,
        success: function (result) {
            let pengajar = result.pengajar.map((e) => { return { id: e.id, text: `${e.nama}` } });
            $('#pengajar').select2({
                placeholder: 'Pilih pengajar',
                allowClear: true,
                theme: "classic",
                data: pengajar
            })
        },
        error: function (jqXHR, textStatus, errorThrown) {
            Swal.fire({
                icon: 'error',
                title: `Gagal, error pada ajax [${textStatus} - ${errorThrown}]`
            })
        },
    });
}

function ambil_kelas(){
    $.ajax({
        method: 'GET',
        dataType: 'JSON',
        url: `${baseUrl}/operator/presensi_guru/ambil_kelas`,
        success: function (result) {
            let kelas = result.kelas.map((e) => { return { id: e.id, text: `${e.kelas}` } });
            $('#kelas').select2({
                placeholder: 'Pilih kelas',
                allowClear: true,
                theme: "classic",
                data: kelas
            })
        },
        error: function (jqXHR, textStatus, errorThrown) {
            Swal.fire({
                icon: 'error',
                title: `Gagal, error pada ajax [${textStatus} - ${errorThrown}]`
            })
        },
    });
}

function get_cari(){
    $('#hari').change(function () {
        tabel_data.api().ajax.reload()
    })
    $('#pengajar').change(function () {
        tabel_data.api().ajax.reload()
    })
    $('#kelas').change(function () {
        tabel_data.api().ajax.reload()
    })
}

function lihat_data(id_jadwal, tanggal, kelas, pelajaran, pengajar, hari, txt_kelas, txt_pelajaran){

}
function g1(tanggal, pengajar, kelas, satu, hadir){
	if(hadir){
		$('#modal').modal('show')
		$('#e_no_urut').val(satu).trigger('change')
		$('#e_tanggal').val(tanggal)
		$('#e_kelas').val(kelas)
		$('#e_pengajar').val(pengajar)
		$('#e_hadir').val(hadir)
		$('#e_jam').val(satu)
	}else{
		$('#e_no_urut').val('').trigger('change')
		$('#e_tanggal').val('')
		$('#e_kelas').val('')
		$('#e_pengajar').val('')
		$('#e_jam').val('')
		$('#e_hadir').val('')
	}
}
function g2(tanggal, pengajar, kelas, dua, hadir){
	if(hadir){
		$('#modal').modal('show')
		$('#e_no_urut').val(dua).trigger('change')
		$('#e_tanggal').val(tanggal)
		$('#e_kelas').val(kelas)
		$('#e_pengajar').val(pengajar)
		$('#e_hadir').val(hadir)
		$('#e_jam').val(dua)
	}else{
		$('#e_no_urut').val('').trigger('change')
		$('#e_tanggal').val('')
		$('#e_kelas').val('')
		$('#e_pengajar').val('')
		$('#e_jam').val('')
		$('#e_hadir').val('')
	}
}
function g3(tanggal, pengajar, kelas, tiga, hadir){
	if(hadir){
		$('#modal').modal('show')
		$('#e_no_urut').val(tiga).trigger('change')
		$('#e_tanggal').val(tanggal)
		$('#e_kelas').val(kelas)
		$('#e_pengajar').val(pengajar)
		$('#e_hadir').val(hadir)
		$('#e_jam').val(tiga)
	}else{
		$('#e_no_urut').val('').trigger('change')
		$('#e_tanggal').val('')
		$('#e_kelas').val('')
		$('#e_pengajar').val('')
		$('#e_jam').val('')
		$('#e_hadir').val('')
	}
}
function g4(tanggal, pengajar, kelas, empat, hadir){
	if(hadir){
		$('#modal').modal('show')
		$('#e_no_urut').val(empat).trigger('change')
		$('#e_tanggal').val(tanggal)
		$('#e_kelas').val(kelas)
		$('#e_pengajar').val(pengajar)
		$('#e_hadir').val(hadir)
		$('#e_jam').val(empat)
	}else{
		$('#e_no_urut').val('').trigger('change')
		$('#e_tanggal').val('')
		$('#e_kelas').val('')
		$('#e_pengajar').val('')
		$('#e_jam').val('')
		$('#e_hadir').val('')
	}
}
function g5(tanggal, pengajar, kelas, lima, hadir){
	if(hadir){
		$('#modal').modal('show')
		$('#e_no_urut').val(lima).trigger('change')
		$('#e_tanggal').val(tanggal)
		$('#e_kelas').val(kelas)
		$('#e_pengajar').val(pengajar)
		$('#e_hadir').val(hadir)
		$('#e_jam').val(lima)
	}else{
		$('#e_no_urut').val('').trigger('change')
		$('#e_tanggal').val('')
		$('#e_kelas').val('')
		$('#e_pengajar').val('')
		$('#e_jam').val('')
		$('#e_hadir').val('')
	}
}
function g6(tanggal, pengajar, kelas, enam, hadir){
	if(hadir){
		$('#modal').modal('show')
		$('#e_no_urut').val(enam).trigger('change')
		$('#e_tanggal').val(tanggal)
		$('#e_kelas').val(kelas)
		$('#e_pengajar').val(pengajar)
		$('#e_hadir').val(hadir)
		$('#e_jam').val(enam)
	}else{
		$('#e_no_urut').val('').trigger('change')
		$('#e_tanggal').val('')
		$('#e_kelas').val('')
		$('#e_pengajar').val('')
		$('#e_jam').val('')
		$('#e_hadir').val('')
	}
}
function g7(tanggal, pengajar, kelas, tujuh, hadir){
	if(hadir){
		$('#modal').modal('show')
		$('#e_no_urut').val(tujuh).trigger('change')
		$('#e_tanggal').val(tanggal)
		$('#e_kelas').val(kelas)
		$('#e_pengajar').val(pengajar)
		$('#e_hadir').val(hadir)
		$('#e_jam').val(tujuh)
	}else{
		$('#e_no_urut').val('').trigger('change')
		$('#e_tanggal').val('')
		$('#e_kelas').val('')
		$('#e_pengajar').val('')
		$('#e_jam').val('')
		$('#e_hadir').val('')
	}
}
function g8(tanggal, pengajar, kelas, delapan, hadir){
	if(hadir){
		$('#modal').modal('show')
		$('#e_no_urut').val(delapan).trigger('change')
		$('#e_tanggal').val(tanggal)
		$('#e_kelas').val(kelas)
		$('#e_pengajar').val(pengajar)
		$('#e_hadir').val(hadir)
		$('#e_jam').val(delapan)
	}else{
		$('#e_no_urut').val('').trigger('change')
		$('#e_tanggal').val('')
		$('#e_kelas').val('')
		$('#e_pengajar').val('')
		$('#e_jam').val('')
		$('#e_hadir').val('')
	}
}
function g9(tanggal, pengajar, kelas, sembilan, hadir){
	if(hadir){
		$('#modal').modal('show')
		$('#e_no_urut').val(sembilan).trigger('change')
		$('#e_tanggal').val(tanggal)
		$('#e_kelas').val(kelas)
		$('#e_pengajar').val(pengajar)
		$('#e_hadir').val(hadir)
		$('#e_jam').val(sembilan)
	}else{
		$('#e_no_urut').val('').trigger('change')
		$('#e_tanggal').val('')
		$('#e_kelas').val('')
		$('#e_pengajar').val('')
		$('#e_jam').val('')
		$('#e_hadir').val('')
	}
}
function g10(tanggal, pengajar, kelas, sepuluh, hadir){
	if(hadir){
		$('#modal').modal('show')
		$('#e_no_urut').val(sepuluh).trigger('change')
		$('#e_tanggal').val(tanggal)
		$('#e_kelas').val(kelas)
		$('#e_pengajar').val(pengajar)
		$('#e_hadir').val(hadir)
		$('#e_jam').val(sepuluh)
	}else{
		$('#e_no_urut').val('').trigger('change')
		$('#e_tanggal').val('')
		$('#e_kelas').val('')
		$('#e_pengajar').val('')
		$('#e_jam').val('')
		$('#e_hadir').val('')
	}
}
function g11(tanggal, pengajar, kelas, sebelas, hadir){
	if(hadir){
		$('#modal').modal('show')
		$('#e_no_urut').val(sebelas).trigger('change')
		$('#e_tanggal').val(tanggal)
		$('#e_kelas').val(kelas)
		$('#e_pengajar').val(pengajar)
		$('#e_hadir').val(hadir)
		$('#e_jam').val(sebelas)
	}else{
		$('#e_no_urut').val('').trigger('change')
		$('#e_tanggal').val('')
		$('#e_kelas').val('')
		$('#e_pengajar').val('')
		$('#e_jam').val('')
		$('#e_hadir').val('')
	}
}

function button(){
	$('#simpan').click(function (e) {
		e.preventDefault()

		let peringatan = '';
	    if (!$('#tanggal').val()) peringatan += 'Tanggal, '
	    if (!$('#pengajar').val()) peringatan += 'pengajar, '
	    if (!$('#kelas').val()) peringatan += 'kelas, '
	    if (!$('#jam').val()) peringatan += 'jam, '
	    if (peringatan) {
	    	Swal.fire({
	    		icon: 'error',
	    		title: `${peringatan} harus diisi !`
	    	})
	    } else {
	    	let input_data = $("#input_data").serialize();

	    	$.ajax({
	    		url: `${baseUrl}/operator/presensi_guru/simpan`,
	    		type : "post",
	    		data: input_data,
	    		dataType: 'JSON',
	    		success: function(result){
	    			if (result.status == 200) {
	    				tabel_data.api().ajax.reload(null, false)
	    				$(`#jam`).val('').trigger('change')
	    			}else{
	    				Swal.fire({
	    					icon: 'info',
	    					title: `${result.keterangan}`,
	    				})
	    			}
	    		}
	    	});
	    }
	});

	$('#batal').click(function (e) {
        e.preventDefault()
        reset()
    });

	$('#cetak').click(function (e) {
        e.preventDefault()
        let tanggal     = $('#tanggal').val()
        let hari        = $('#hari').val()

        if(tanggal != ''){
            printJS({
                printable: `${baseUrl}/operator/presensi_guru/cetak/${tanggal}/${hari}`,
                type: 'pdf',
                showModal: true,
                modalMessage: "Proses printing, Mohon tunggu ..."
            })
        }else{
            Swal.fire({
                icon: 'warning',
                title: `pilih tanggal dahulu !!`,
            })
        }
    });
}

function reset(){
	$(`#kelas`).val('').trigger('change')
	$(`#pengajar`).val('').trigger('change')
	$(`#jam`).val('').trigger('change')
}