let baseUrl = `${window.origin}/presensi`
$(document).ready(function () {
	komponen_data()
    button()
})

function komponen_data(){
	data()
	get_guru()
}

function data(){
	$.ajax({
		method: 'POST',
		dataType: 'JSON',
		url: `${baseUrl}/operator/set_profil/ambil_data`,
		success: function (data) {
            let d = new Date();
			$('#id').val(data.data.id)
			$('#sekolah').val(data.data.sekolah)
			// $(`#kepala`).val(data.data.kepala).trigger('change')
			// $(`#wakil`).val(data.data.wakil).trigger('change')
			// $(`#kesiswaan`).val(data.data.kepala).trigger('change')
			$(`#kepala`).append(new Option(data.kepala.nama, data.data.kepala, true, true)).trigger('change');
			$(`#wakil`).append(new Option(data.wakil.nama, data.data.wakil, true, true)).trigger('change');
			$(`#kesiswaan`).append(new Option(data.kesiswaan.nama, data.data.kesiswaan, true, true)).trigger('change');
			$('#alamat').val(data.data.alamat)
			$('#kode_pos').val(data.data.kode_pos)
			$('#telepone').val(data.data.telepone)
			$('#npsn').val(data.data.npsn)
			$('#thn_ajaran').val(data.data.thn_ajaran)
			image_sekolah = data.data.image_sekolah
			image_prov = data.data.image_prov
			$('#logo_prov').prop('src', `${image_prov ? baseUrl + '/file/' + image_prov + '?v=' + d.getTime() : baseUrl + '/file/default_img.png' + '?v=' + d.getTime()}`)
			$('#logo_sekolah').prop('src', `${image_sekolah ? baseUrl + '/file/' + image_sekolah + '?v=' + d.getTime() : baseUrl + '/file/default_img.png' + '?v=' + d.getTime()}`)
		},
		error: function (jqXHR, textStatus, errorThrown) {
			Swal.fire({
				icon: 'error',
				title: `Gagal, error pada ajax [${textStatus} - ${errorThrown}]`
			})
		},
	})
}

function get_guru(){
	$.ajax({
		method: 'GET',
		dataType: 'JSON',
		url: `${baseUrl}/operator/set_profil/ambil_guru`,
		success: function (result) {
			let guru = result.guru.map((e) => { return { id: e.id, text: `${e.nama}` } });
			$('#kepala').select2({
				placeholder: 'Silahkan pilih',
				allowClear: true,
				theme: "classic",
				data: guru
			})
			$('#wakil').select2({
				placeholder: 'Silahkan pilih',
				allowClear: true,
				theme: "classic",
				data: guru
			})
			$('#kesiswaan').select2({
				placeholder: 'Silahkan pilih',
				allowClear: true,
				theme: "classic",
				data: guru
			})
		},
		error: function (jqXHR, textStatus, errorThrown) {
			Swal.fire({
				icon: 'error',
				title: `Gagal, error pada ajax [${textStatus} - ${errorThrown}]`
			})
		},
	});
}

function button(){
	$('#simpan').click(() => {
        	let test = new FormData($('#test')[0])

            $.ajax({
                method: 'POST',
                dataType: 'JSON',
                data: test,
                url: `${baseUrl}/operator/set_profil/simpan`,
                processData: false,
                contentType: false,
                success: function (result) {
                    if (result.status == 200) {
                        Swal.fire({
                            icon: 'success',
                            title: `${result.keterangan}`,
                            showConfirmButton: false,
                            timer: 1500
                        }).then(() => {
                        	data()
                        })
                    }else{
                        Swal.fire({
                            icon: 'warning',
                            title: `${result.keterangan}`,
                        })
                    }
                }
            })
    })
}