let baseUrl = `${window.origin}/presensi`
$(document).ready(function () {
    tabel_data.api().ajax.reload()
    komponenTabel()
    button()
})

function onlyNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode == 46 && evt.srcElement.value.split('.').length > 1) return false;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) return false;
    return true;
}

let tabel_data = $('#tabel_data').dataTable({
    "processing": true,
    "serverSide": true,

    "ajax": {
        "url": `${baseUrl}/operator/set_jadwal/datatables`,
        "type": "POST",
        "data": function (data) {
            data.hari       = $('#cari_hari').val();
            data.kelas      = $('#cari_kelas').val();
            data.pelajaran  = $('#cari_pelajaran').val();
            data.pengajar   = $('#cari_pengajar').val();
        }
    },
    "bPaginate": true,
    "bLengthChange": false,
    "bFilter": false,
    "bInfo": true,
    "stateSave": true,
    "bAutoWidth": false,
    "ordering": false,
    "pageLength": 12,
    "pagingType": "simple_numbers",
    "scrollX": true,
    "scrollY": "67vh",
    "columnDefs": [
        { className: "text-center", "targets": [1,5] }
    ],
});

function komponenTabel(){
	get_kelas()
	get_pelajaran()
	get_pengajar()
    get_cari()
}

function get_kelas(){
    $.ajax({
        method: 'GET',
        dataType: 'JSON',
        url: `${baseUrl}/operator/set_jadwal/ambil_kelas`,
        success: function (result) {
            let kelas = result.kelas.map((e) => { return { id: e.id, text: `${e.kelas}` } });
            $('#kelas').select2({
                placeholder: 'Pilih kelas',
                allowClear: true,
                theme: "classic",
                data: kelas
            })
            $('#cari_kelas').select2({
                placeholder: 'Cari kelas',
                allowClear: true,
                theme: "classic",
                data: kelas
            })
            $('#cari_hari').select2({
                placeholder: 'Cari hari',
                allowClear: true,
                theme: "classic"
            })
        },
        error: function (jqXHR, textStatus, errorThrown) {
            Swal.fire({
                icon: 'error',
                title: `Gagal, error pada ajax [${textStatus} - ${errorThrown}]`
            })
        },
    });
}

function get_pelajaran(){
    $.ajax({
        method: 'GET',
        dataType: 'JSON',
        url: `${baseUrl}/operator/set_jadwal/ambil_pelajaran`,
        success: function (result) {
            let pelajaran = result.pelajaran.map((e) => { return { id: e.id, text: `${e.pelajaran}` } });
            $('#pelajaran').select2({
                placeholder: 'Pilih pelajaran',
                allowClear: true,
                theme: "classic",
                data: pelajaran
            })
            $('#cari_pelajaran').select2({
                placeholder: 'Cari pelajaran',
                allowClear: true,
                theme: "classic",
                data: pelajaran
            })
        },
        error: function (jqXHR, textStatus, errorThrown) {
            Swal.fire({
                icon: 'error',
                title: `Gagal, error pada ajax [${textStatus} - ${errorThrown}]`
            })
        },
    });
}

function get_pengajar(){
    $.ajax({
        method: 'GET',
        dataType: 'JSON',
        url: `${baseUrl}/operator/set_jadwal/ambil_pengajar`,
        success: function (result) {
            let pengajar = result.pengajar.map((e) => { return { id: e.id, text: `${e.nama}` } });
            $('#pengajar').select2({
                placeholder: 'Pilih pengajar',
                allowClear: true,
                theme: "classic",
                data: pengajar
            })
            $('#cari_pengajar').select2({
                placeholder: 'Cari pengajar',
                allowClear: true,
                theme: "classic",
                data: pengajar
            })
        },
        error: function (jqXHR, textStatus, errorThrown) {
            Swal.fire({
                icon: 'error',
                title: `Gagal, error pada ajax [${textStatus} - ${errorThrown}]`
            })
        },
    });
}

function get_cari(){
    $('#cari_hari').change(function () {
        tabel_data.api().ajax.reload()
    })
    $('#cari_kelas').change(function () {
        tabel_data.api().ajax.reload()
    })
    $('#cari_pelajaran').change(function () {
        tabel_data.api().ajax.reload()
    })
    $('#cari_pengajar').change(function () {
        tabel_data.api().ajax.reload()
    })
}

function edit_data(id){
    $.ajax({
        method: 'POST',
        data: { id },
        dataType: 'JSON',
        url: `${baseUrl}/operator/set_jadwal/get_data`,
        success: function (data) {
            $('#id').val(data.data.id)
            $(`#kelas`).val(data.data.kelas).trigger('change')
            $(`#pelajaran`).val(data.data.pelajaran).trigger('change')
            $(`#pengajar`).val(data.data.pengajar).trigger('change')
            $(`#hari`).val(data.data.hari).trigger('change')
            $('#jumlah_jam').val(data.data.jumlah_jam)
            $('#jam_mulai').val(data.data.jam_mulai)
            $('#jam_akhir').val(data.data.jam_akhir)
        }
    });
}

function hapus_data(id){
    Swal.fire({
        title: 'Konfimasi hapus ?',
        icon: 'warning',
        showCancelButton: true,
        allowEscapeKey: false,
        allowOutsideClick: false,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Tidak',
        confirmButtonText: 'Ya, Lanjutkan!'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                method: 'POST',
                dataType: 'JSON',
                data: { id },
                url: `${baseUrl}/operator/set_jadwal/hapus`,
                success: function (result) {
                    if (result.status == 200) {
                        Swal.fire({
                            icon: 'success',
                            title: `${result.keterangan}`,
                            showConfirmButton: false,
                            timer: 1500
                        }).then(() => {
                            tabel_data.api().ajax.reload()
                        })
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: `${result.keterangan}`,
                        })
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    Swal.fire({
                        icon: 'error',
                        title: `Gagal simpan, error pada ajax[${textStatus} - ${errorThrown}]`
                    })
                },
            });
        }
    })
}

function button(){
    $('#simpan').click(() => {
        let peringatan = '';
        if (!$('#kelas').val()) peringatan += 'kelas, '
        if (!$('#pelajaran').val()) peringatan += 'pelajaran, '
        if (!$('#pengajar').val()) peringatan += 'pengajar, '
        if (!$('#hari').val()) peringatan += 'hari, '
        if (!$('#jumlah_jam').val()) peringatan += 'jumlah_jam, '
        if (!$('#jam_mulai').val()) peringatan += 'jam_mulai, '
        if (!$('#jam_akhir').val()) peringatan += 'jam_akhir, '
        if (peringatan) {
            Swal.fire({
                icon: 'error',
                title: `${peringatan} harus diisi !`
            })
        } else {
            let input_data = $("#input_data").serialize();

            $.ajax({
                method: 'POST',
                dataType: 'JSON',
                data: input_data,
                url: `${baseUrl}/operator/set_jadwal/simpan`,
                success: function (result) {
                    if (result.status == 200) {
                        Swal.fire({
                            icon: 'success',
                            title: `${result.keterangan}`,
                            showConfirmButton: false,
                            timer: 1500
                        }).then(() => {
                            tabel_data.api().ajax.reload(null, false)
                            reset()
                        })
                    }else{
                        Swal.fire({
                            icon: 'warning',
                            title: `${result.keterangan}`,
                        })
                    }
                }
            })
        }
    })

    $('#batal').click(() => {
        reset()
    })
}

function reset(){
    $('#id').val('')
    $(`#kelas`).val('').trigger('change')
    $(`#pelajaran`).val('').trigger('change')
    $(`#pengajar`).val('').trigger('change')
    $(`#hari`).val('').trigger('change')
    $('#jumlah_jam').val('')
    $('#jam_mulai').val('')
    $('#jam_akhir').val('')
}