let baseUrl = `${window.origin}/presensi`
$(document).ready(function () {
    get_location()
    button()
})

function onlyNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode == 46 && evt.srcElement.value.split('.').length > 1) return false;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) return false;
    return true;
}

function get_location(){
    $.ajax({
        method: 'GET',
        dataType: 'JSON',
        url: `${baseUrl}/operator/set_lokasi_absen/get_location`,
        success: function (data) {
            // for(i=0; i<data.lokasi.length; i++){
                $('#id_lokasi').val(data.lokasi[0].id)
                $('#latitude').val(data.lokasi[0].latitude)
                $('#longitude').val(data.lokasi[0].longitude)
                $('#radius').val(data.lokasi[0].radius)
                $('#nama_tempat').val(data.lokasi[0].nama_tempat)
                $('#keterangan').val(data.lokasi[0].keterangan)
                initialize()
            // }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            Swal.fire({
                icon: 'error',
                title: `Gagal, error pada ajax [${textStatus} - ${errorThrown}]`
            })
        },
    });
}

function button(){
	$('#simpan').click((e) => {
        e.preventDefault()
        
        let peringatan = '';
        if (!$('#latitude').val()) peringatan += 'latitude, '
        if (!$('#longitude').val()) peringatan += 'longitude, '
        if (!$('#radius').val()) peringatan += 'radius, '
        if (!$('#nama_tempat').val()) peringatan += 'nama tempat, '
        if (peringatan) {
            Swal.fire({
                icon: 'warning',
                title: `${peringatan} harus diisi !`
            })
        } else {
            let input_data = $("#input_data").serialize();
            $.ajax({
                method: 'POST',
                dataType: 'JSON',
                data: input_data,
                url: `${baseUrl}/operator/set_lokasi_absen/save`,
                beforeSend(xhr) {
                    swal.fire({
                        title: `Tunggu, sedang menyimpan data...`,
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                        didOpen: () => {
                            swal.showLoading();
                        }
                    })
                },
                success: function (result) {
                    if (result.status == 200) {
                        Swal.fire({
                            icon: 'success',
                            title: `Berhasil simpan data`
                        }).then((r) => {
                            get_location()
                        });
                    } else {
                        Swal.fire({
                            icon: 'warning',
                            title: `${result.keterangan}`
                        })
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    Swal.fire({
                        icon: 'error',
                        title: `Gagal, error pada ajax [${textStatus} - ${errorThrown}]`
                    })
                },
            });
        }
    });
}

function initialize() {
    let lat_absen   = $('#latitude').val()
    let long_absen  = $('#longitude').val()
    let lat_saya    = $('#latitude').val()
    let long_saya   = $('#longitude').val()
    let radius   	= $('#radius').val()

    var center = new google.maps.LatLng(lat_absen, long_absen); //rsi
    var radius_circle = parseInt(radius); // 30km
    var markerPositions = [
        {lat: lat_saya, lng: long_saya} //rumah
    ];

    var markers=[];
    var mapOptions = {
        center: center,
        zoom: 18,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById('map'), mapOptions);
    var circle = drawCircle(mapOptions.center, radius_circle);


    for (var i=0; i<markerPositions.length; i++) {
        markers.push(
            new google.maps.Marker({
                position: new google.maps.LatLng(markerPositions[i].lat, markerPositions[i].lng),
                map: map,
                draggable: false
            })
        );
    }

    for (var i=0; i<markerPositions.length; i++) {
        var distance = calculateDistance(
            markers[i].getPosition().lat(),
            markers[i].getPosition().lng(),
            circle.getCenter().lat(),
            circle.getCenter().lng(),
            "K"
            );
        // if (distance * 1000 < radius_circle) {  // radius is in meter; distance in km
        //     // Swal.fire({
        //     //     icon: 'success',
        //     //     title: `Absen Berhasil`,
        //     //     showConfirmButton: false,
        //     //     timer: 1000
        //     // }).then((r) => {
        //         upload_foto()
        //     // });
        // }else {
        //     Swal.fire({
        //         icon: 'warning',
        //         title: `Lokasi anda diluar jangkauan`,
        //         showConfirmButton: false,
        //         timer: 1000
        //     })
        // }
    }

    function drawCircle(center, radius) {
      return new google.maps.Circle({
        strokeColor: '#0000FF',
        strokeOpacity: 0.7,
        strokeWeight: 1,
        fillColor: '#0000FF',
        fillOpacity: 0.15,
        draggable: false,
        map: map,
        center: center,
        radius: radius
      });
    }

    function calculateDistance(lat1, lon1, lat2, lon2, unit) {
      var radlat1 = Math.PI * lat1/180;
      var radlat2 = Math.PI * lat2/180;
      var radlon1 = Math.PI * lon1/180;
      var radlon2 = Math.PI * lon2/180;
      var theta = lon1-lon2;
      var radtheta = Math.PI * theta/180;
      var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
      dist = Math.acos(dist);
      dist = dist * 180/Math.PI;
      dist = dist * 60 * 1.1515;
      if (unit=="K") { dist = dist * 1.609344; }
      if (unit=="N") { dist = dist * 0.8684; }
      return dist;
    }
}