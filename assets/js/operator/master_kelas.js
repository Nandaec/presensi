let baseUrl = `${window.origin}/presensi`
$(document).ready(function () {
    tabel_data.api().ajax.reload()
    komponenTabel()
    tambah_data()
    button()
})

let tabel_data = $('#tabel_data').dataTable({
    "processing": true,
    "serverSide": true,

    "ajax": {
        "url": `${baseUrl}/operator/master_kelas/datatables`,
        "type": "POST",
        "data": function (data) {
            data.cari = $('#cari').val();
        }
    },
    "bPaginate": true,
    "bLengthChange": false,
    "bFilter": false,
    "bInfo": true,
    "stateSave": true,
    "bAutoWidth": false,
    "ordering": false,
    "pageLength": 12,
    "pagingType": "simple_numbers",
    "scrollX": true,
    "scrollY": "67vh",
    "columnDefs": [
        { className: "text-center", "targets": [1] }
    ],
});

function komponenTabel() {
    $('#cari').keyup(() => {
        tabel_data.api().ajax.reload()
    })
    get_wali()
}

function get_wali(){
    $.ajax({
        method: 'GET',
        dataType: 'JSON',
        url: `${baseUrl}/operator/master_kelas/ambil_wali`,
        success: function (result) {
            let wali = result.wali.map((e) => { return { id: e.id, text: `${e.nama}` } });
            $('#wali').select2({
                placeholder: 'Pilih wali',
                allowClear: true,
                theme: "classic",
                data: wali
            })
        },
        error: function (jqXHR, textStatus, errorThrown) {
            Swal.fire({
                icon: 'error',
                title: `Gagal, error pada ajax [${textStatus} - ${errorThrown}]`
            })
        },
    });
}

function tambah_data(){
    $('#tambah').click(() => {
        $('#modal_data').modal('show')
        $('#teks').text('Tambah Data Kelas')
    })
}

function edit_data(id){
    $('#modal_data').modal('show')
    $('#teks').text('Edit Data Kelas')

    $.ajax({
        method: 'POST',
        data: { id },
        dataType: 'JSON',
        url: `${baseUrl}/operator/master_kelas/get_data`,
        success: function (data) {
            $('#id').val(data.data.kelas)
            $('#kelas').val(data.data.kelas)
            $(`#wali`).val(data.data.wali).trigger('change')
        }
    });
}

function hapus_data(id){            
    Swal.fire({
        title: 'Konfimasi hapus ?',
        icon: 'warning',
        showCancelButton: true,
        allowEscapeKey: false,
        allowOutsideClick: false,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Tidak',
        confirmButtonText: 'Ya, Lanjutkan!'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                method: 'POST',
                dataType: 'JSON',
                data: { id },
                url: `${baseUrl}/operator/master_kelas/hapus`,
                success: function (result) {
                    if (result.status == 200) {
                        Swal.fire({
                            icon: 'success',
                            title: `${result.keterangan}`,
                            showConfirmButton: false,
                            timer: 1500
                        }).then(() => {
                            tabel_data.api().ajax.reload()
                        })
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: `${result.keterangan}`,
                        })
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    Swal.fire({
                        icon: 'error',
                        title: `Gagal simpan, error pada ajax[${textStatus} - ${errorThrown}]`
                    })
                },
            });
        }
    })
}

function button(){
    $('#simpan').click(() => {
        let peringatan = '';
        if (!$('#kelas').val()) peringatan += 'Kelas, '
        if (!$('#wali').val()) peringatan += 'Kelas, '
        if (peringatan) {
            Swal.fire({
                icon: 'error',
                title: `${peringatan} harus diisi !`
            })
        } else {
            let input_data = $("#input_data").serialize();

            $.ajax({
                method: 'POST',
                dataType: 'JSON',
                data: input_data,
                url: `${baseUrl}/operator/master_kelas/simpan`,
                success: function (result) {
                    if (result.status == 200) {
                        Swal.fire({
                            icon: 'success',
                            title: `${result.keterangan}`,
                            showConfirmButton: false,
                            timer: 1500
                        }).then(() => {
                            tabel_data.api().ajax.reload(null, false)
                            $('#modal_data').modal('hide')
                            reset()
                        })
                    }else{
                        Swal.fire({
                            icon: 'warning',
                            title: `${result.keterangan}`,
                        })
                    }
                }
            })
        }
    })

    $('#batal').click(() => {
        $('#modal_data').modal('hide')
        reset()
    })
}

function reset(){
    $('#id').val('')
    $('#kelas').val('')
    $(`#wali`).val('').trigger('change')
}