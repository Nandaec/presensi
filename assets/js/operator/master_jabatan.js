let baseUrl = `${window.origin}/presensi`
$(document).ready(function () {
    tabel_data.api().ajax.reload()
    komponenTabel()
    tambah_data()
    button()
})

let tabel_data = $('#tabel_data').dataTable({
    "processing": true,
    "serverSide": true,

    "ajax": {
        "url": `${baseUrl}/operator/master_jabatan/datatables`,
        "type": "POST",
        "data": function (data) {
            data.cari = $('#cari').val();
        }
    },
    "bPaginate": true,
    "bLengthChange": false,
    "bFilter": false,
    "bInfo": true,
    "stateSave": true,
    "bAutoWidth": false,
    "ordering": false,
    "pageLength": 12,
    "pagingType": "simple_numbers",
    "scrollX": true,
    "scrollY": "67vh",
    "columnDefs": [
        { className: "text-center", "targets": [1] }
    ],
});

function komponenTabel() {
    $('#cari').keyup(() => {
        tabel_data.api().ajax.reload()
    })
}

function tambah_data(){
    $('#tambah').click(() => {
        $('#modal_data').modal('show')
        $('#teks').text('Tambah Data Pelajaran')
    })
}

function edit_data(id){
    $('#modal_data').modal('show')
    $('#teks').text('Edit Data Pelajaran')

    $.ajax({
        method: 'POST',
        data: { id },
        dataType: 'JSON',
        url: `${baseUrl}/operator/master_jabatan/get_data`,
        success: function (data) {
            $('#id').val(data.data.jabatan)
            $('#jabatan').val(data.data.jabatan)
        }
    });
}

function hapus_data(id){            
    Swal.fire({
        title: 'Konfimasi hapus ?',
        icon: 'warning',
        showCancelButton: true,
        allowEscapeKey: false,
        allowOutsideClick: false,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Tidak',
        confirmButtonText: 'Ya, Lanjutkan!'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                method: 'POST',
                dataType: 'JSON',
                data: { id },
                url: `${baseUrl}/operator/master_jabatan/hapus`,
                success: function (result) {
                    if (result.status == 200) {
                        Swal.fire({
                            icon: 'success',
                            title: `${result.keterangan}`,
                            showConfirmButton: false,
                            timer: 1500
                        }).then(() => {
                            tabel_data.api().ajax.reload()
                        })
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: `${result.keterangan}`,
                        })
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    Swal.fire({
                        icon: 'error',
                        title: `Gagal simpan, error pada ajax[${textStatus} - ${errorThrown}]`
                    })
                },
            });
        }
    })
}

function button(){
    $('#simpan').click(() => {
        let peringatan = '';
        if (!$('#jabatan').val()) peringatan += 'Jabatan, '
        if (peringatan) {
            Swal.fire({
                icon: 'error',
                title: `${peringatan} harus diisi !`
            })
        } else {
            let input_data = $("#input_data").serialize();

            $.ajax({
                method: 'POST',
                dataType: 'JSON',
                data: input_data,
                url: `${baseUrl}/operator/master_jabatan/simpan`,
                success: function (result) {
                    if (result.status == 200) {
                        Swal.fire({
                            icon: 'success',
                            title: `${result.keterangan}`,
                            showConfirmButton: false,
                            timer: 1500
                        }).then(() => {
                            tabel_data.api().ajax.reload(null, false)
                            $('#modal_data').modal('hide')
                            reset()
                        })
                    }else{
                        Swal.fire({
                            icon: 'warning',
                            title: `${result.keterangan}`,
                        })
                    }
                }
            })
        }
    })

    $('#batal').click(() => {
        $('#modal_data').modal('hide')
        reset()
    })
}

function reset(){
    $('#id').val('')
    $('#jabatan').val('')
}