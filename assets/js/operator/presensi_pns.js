let baseUrl = `${window.origin}/presensi`
$(document).ready(function () {
    tabel_data.api().ajax.reload()
    komponenTabel()
    button()
})

let tabel_data = $('#tabel_data').dataTable({
    "processing": true,
    "serverSide": true,

    "ajax": {
        "url": `${baseUrl}/operator/presensi_pns/datatables`,
        "type": "POST",
        "data": function (data) {
            data.tanggal    = moment($('#cari_tanggal').val(), 'DD-MM-YYYY').format("YYYY-MM-DD");
            data.cari       = $('#cari').val();
        }
    },
    "bPaginate": false,
    "bLengthChange": false,
    "bFilter": false,
    "bInfo": true,
    "stateSave": true,
    "bAutoWidth": false,
    "ordering": false,
    // "pageLength": 12,
    "pagingType": "simple_numbers",
    "scrollX": true,
    "scrollY": "67vh",
    "columnDefs": [
        { className: "text-center", "targets": [1,5] }
    ],
});

function komponenTabel(){
	if (!$('#cari_tanggal').val()) $('#cari_tanggal').val(moment().format('DD-MM-YYYY'))
	$('#cari_tanggal').datetimepicker({
		format: 'DD-MM-YYYY'
	}).on('dp.change', function (e) {
		$("#cari_tanggal").data("DateTimePicker").hide();
		tabel_data.api().ajax.reload()
	})

	$('#cari').keyup(() => {
        tabel_data.api().ajax.reload()
    })
}

function lihat_data(id, tanggal){
    $('#modal_data').modal('show')

    $.ajax({
        method: 'POST',
        data: { id, tanggal},
        dataType: 'JSON',
        url: `${baseUrl}/operator/presensi_pns/get_data`,
        success: function (data) {
            if(data){
                let d = new Date();
                $('#nip_berangkat').text(data.berangkat.nip)
                $('#nama_berangkat').text(data.berangkat.nama)
                $('#golongan_berangkat').text(data.berangkat.golongan)
                $('#jabatan_berangkat').text(data.berangkat.jabatan)
                $('#waktu_berangkat').text(data.berangkat.waktu)
                $('#latitude_berangkat').text(data.berangkat.latitude)
                $('#longitude_berangkat').text(data.berangkat.longitude)
                $('#foto_berangkat').prop('src', `${baseUrl}/file/pns/${data.berangkat.image}?v=${d.getTime()}`)

                if(data.berangkat.id != data.pulang.id){
                    $('#nip_pulang').text(data.pulang.nip)
                    $('#nama_pulang').text(data.pulang.nama)
                    $('#golongan_pulang').text(data.pulang.golongan)
                    $('#jabatan_pulang').text(data.pulang.jabatan)
                    $('#waktu_pulang').text(data.pulang.waktu)
                    $('#latitude_pulang').text(data.pulang.latitude)
                    $('#longitude_pulang').text(data.pulang.longitude)
                    $('#foto_pulang').prop('src', `${baseUrl}/file/pns/${data.pulang.image}?v=${d.getTime()}`)
                }else{
                    reset_pulang()
                }
            }else{
                let d = new Date();
                $('#nip_berangkat').text('')
                $('#nama_berangkat').text('')
                $('#golongan_berangkat').text('')
                $('#jabatan_berangkat').text('')
                $('#waktu_berangkat').text('')
                $('#latitude_berangkat').text('')
                $('#longitude_berangkat').text('')
                $('#foto_berangkat').prop('src', `${baseUrl}/file/default_img.png?v=${d.getTime()}`)

                reset_pulang()
            }
        }
    });
}

function reset_pulang(){
    let d = new Date();
    $('#nip_pulang').text('')
    $('#nama_pulang').text('')
    $('#golongan_pulang').text('')
    $('#jabatan_pulang').text('')
    $('#waktu_pulang').text('')
    $('#latitude_pulang').text('')
    $('#longitude_pulang').text('')
    $('#foto_pulang').prop('src', `${baseUrl}/file/default_img.png?v=${d.getTime()}`)
}

function button(){
    $('#cetak').click(function (e) {
        e.preventDefault()
        let tanggal     = $('#cari_tanggal').val()

        if(tanggal != ''){
            printJS({
                printable: `${baseUrl}/operator/presensi_pns/cetak/${tanggal}`,
                type: 'pdf',
                showModal: true,
                modalMessage: "Proses printing, Mohon tunggu ..."
            })
        }else{
            Swal.fire({
                icon: 'warning',
                title: `pilih tanggal dan kelas dahulu !!`,
            })
        }
    });
}