let baseUrl = `${window.origin}/presensi`
$(document).ready(function () {
    tabel_user.api().ajax.reload()
    komponenTabel()
    tambah_data()
    button()
})

let tabel_user = $('#tabel_user').dataTable({
    "processing": true,
    "serverSide": true,

    "ajax": {
        "url": `${baseUrl}/operator/master_siswa/datatables`,
        "type": "POST",
        "data": function (data) {
            data.kelas= $('#cari_kelas').val();
            data.cari = $('#cari').val();
        }
    },
    "bPaginate": true,
    "bLengthChange": false,
    "bFilter": false,
    "bInfo": true,
    "stateSave": true,
    "bAutoWidth": false,
    "ordering": false,
    "pageLength": 12,
    "pagingType": "simple_numbers",
    "scrollX": true,
    "scrollY": "67vh",
    "columnDefs": [
        { className: "text-center", "targets": [0,2,4,7,8] }
    ],
});

function komponenTabel() {
    $('#cari').keyup(() => {
        tabel_user.api().ajax.reload()
    })
    $('#cari_kelas').change(function () {
        tabel_user.api().ajax.reload()
    })
    get_kelas()
}

function get_kelas(){
    $.ajax({
        method: 'GET',
        dataType: 'JSON',
        url: `${baseUrl}/operator/master_siswa/ambil_kelas`,
        success: function (result) {
            let kelas = result.kelas.map((e) => { return { id: e.id, text: `${e.kelas}` } });
            $('#kelas').select2({
                placeholder: 'Silahkan pilih',
                allowClear: true,
                theme: "classic",
                data: kelas
            })
            $('#cari_kelas').select2({
                placeholder: 'Cari kelas',
                allowClear: true,
                theme: "classic",
                data: kelas
            })
            $('#import_kelas').select2({
                placeholder: 'Cari kelas',
                allowClear: true,
                theme: "classic",
                data: kelas
            })
        },
        error: function (jqXHR, textStatus, errorThrown) {
            Swal.fire({
                icon: 'error',
                title: `Gagal, error pada ajax [${textStatus} - ${errorThrown}]`
            })
        },
    });
}

function tambah_data(){
    $('#tambah').click(() => {
        reset()
        $('#modal_data').modal('show')
        $('#teks').text('Tambah Data Siswa')
    })
}

function edit_data(id){
    $('#modal_data').modal('show')
    $('#teks').text('Edit Data Siswa')

    $.ajax({
        method: 'POST',
        data: { id },
        dataType: 'JSON',
        url: `${baseUrl}/operator/master_siswa/get_data`,
        success: function (data) {
            $('#id').val(data.data.id)
            $('#username21').val(data.data.username)
            $('#password').val(data.data.password)
            $('#nisn').val(data.data.nisn)
            $('#nama').val(data.data.nama)
            $('#alamat').val(data.data.alamat)
            $('#no_telp').val(data.data.no_telp)
            $(`#kelas`).val(data.data.kelas).trigger('change')
            $(`#jenis_kelamin`).val(data.data.jenis_kelamin).trigger('change')
        }
    });
}

function hapus_data(id){            
    Swal.fire({
        title: 'Konfimasi hapus ?',
        icon: 'warning',
        showCancelButton: true,
        allowEscapeKey: false,
        allowOutsideClick: false,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Tidak',
        confirmButtonText: 'Ya, Lanjutkan!'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                method: 'POST',
                dataType: 'JSON',
                data: { id },
                url: `${baseUrl}/operator/master_siswa/hapus`,
                success: function (result) {
                    if (result.status == 200) {
                        Swal.fire({
                            icon: 'success',
                            title: `${result.keterangan}`,
                            showConfirmButton: false,
                            timer: 1500
                        }).then(() => {
                            tabel_user.api().ajax.reload()
                        })
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: `${result.keterangan}`,
                        })
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    Swal.fire({
                        icon: 'error',
                        title: `Gagal simpan, error pada ajax[${textStatus} - ${errorThrown}]`
                    })
                },
            });
        }
    })
}

function data_import(){
    $.ajax({
        method: 'GET',
        dataType: 'JSON',
        async: false,
        url: `${baseUrl}/operator/master_siswa/get_excel`,
        success: function (result) {
            let html = ""
            no=1;
            if (result.data.length > 0) {
                result.data.forEach( e =>{
                    html += `<tr>
                        <td>${no}</td>
                        <td>${e.username}</td>
                        <td>${e.password}</td>
                        <td>${e.nisn}</td>
                        <td>${e.nama}</td>
                        <td>${e.jenis_kelamin}</td>
                        <td>${e.alamat}</td>
                        <td>${e.no_telp}</td>
                        <td>${e.kelas}</td>                        
                    </tr>`
                    no++
                })
                
            }
            $('#tbl_import_excel').html(html)
            $('#import_file').val('')
        },
        error: function (jqXHR, textStatus, errorThrown) {
            Swal.fire({
                icon: 'error',
                title: `Gagal [${textStatus} - ${errorThrown}]`
            })
        },
    });
}

function button(){
    $('#simpan').click(() => {
        let peringatan = '';
        if (!$('#username21').val()) peringatan += 'Username, '
        if (!$('#password').val()) peringatan += 'Password, '
        if (peringatan) {
            Swal.fire({
                icon: 'error',
                title: `${peringatan} harus diisi !`
            })
        } else {
            let input_data = $("#input_data").serialize();

            $.ajax({
                method: 'POST',
                dataType: 'JSON',
                data: input_data,
                url: `${baseUrl}/operator/master_siswa/simpan`,
                success: function (result) {
                    if (result.status == 200) {
                        Swal.fire({
                            icon: 'success',
                            title: `${result.keterangan}`,
                            showConfirmButton: false,
                            timer: 1500
                        }).then(() => {
                            tabel_user.api().ajax.reload(null, false)
                            $('#modal_data').modal('hide')
                            reset()
                        })
                    }else{
                        Swal.fire({
                            icon: 'warning',
                            title: `${result.keterangan}`,
                        })
                    }
                }
            })
        }
    })

    $('#batal').click(() => {
        $('#modal_data').modal('hide')
        reset()
    })

    $('#import').click(() => {
        $('#modal_import').modal('show')
        data_import()
    })

    $('#btn_import_excel_import').click(() => {
        let file  = $('#import_file')[0].files[0]
        let kelas = $('#import_kelas').val()
        let form = new FormData($('#formulir_import')[0])

        let peringatan = ""
        if (!kelas) peringatan += 'kelas, '
        if (!file) peringatan += 'file, '
        if (peringatan) {
            Swal.fire({
                icon: 'error',
                title: `${peringatan} harus diisi !`
            })
        } else {
            $.ajax({
                method: 'POST',
                dataType: 'JSON',
                data: form,
                url: `${baseUrl}/operator/master_siswa/import`,
                processData: false,
                contentType: false,
                beforeSend(xhr) {
                    swal.fire({
                        title: `Tunggu, sedang menyimpan data...`,
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                        width: 450,
                        didOpen: () => {
                            swal.showLoading();
                        }
                    })
                },
                success: function (result) {
                    if (result.status) {
                        Swal.fire({
                            icon: 'success',
                            title: `Data berhasil disimpan`,
                            showConfirmButton: false,
                            allowEscapeKey: false,
                            allowOutsideClick: false,
                            timer: 1500
                        }).then(() => {
                            data_import()
                            $('#import_kelas').val('')
                        })
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: `${result.ket}`,
                        })
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    Swal.fire({
                        icon: 'error',
                        title: `Gagal, error pada ajax[${textStatus} - ${errorThrown}]`
                    })
                },
            });
        }
    })

    $('#btn_modal_import_close').click( e => {
        Swal.fire({
            title: 'Yakin ?',
            icon: 'warning',
            showCancelButton: true,
            allowEscapeKey: false,
            allowOutsideClick: false,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Belum',
            confirmButtonText: 'Ya!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    method: 'GET',
                    dataType: 'JSON',
                    url: `${baseUrl}/operator/master_siswa/destroy_excel`,
                    beforeSend(xhr) {
                        swal.fire({
                            title: `Tunggu, sedang bersihkan data...`,
                            allowEscapeKey: false,
                            allowOutsideClick: false,
                            width: 450,
                            didOpen: () => {
                                swal.showLoading();
                            }
                        })
                    },
                    success: function (result) {
                        if (result.status) {
                            Swal.fire({
                                icon: 'success',
                                title: `Data berhasil dibersihkan`,
                                showConfirmButton: false,
                                allowEscapeKey: false,
                                allowOutsideClick: false,
                                timer: 1500
                            }).then( e =>{
                                tabel_user.api().ajax.reload()
                                $('#modal_import').modal('hide')
                            })
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        Swal.fire({
                            icon: 'error',
                            title: `Gagal, error pada ajax[${textStatus} - ${errorThrown}]`
                        })
                    },
                });
            }})
    })

    $('#btn_import_excel_update').click( e =>{
        Swal.fire({
            title: 'Pastikan data sudah benar',
            icon: 'warning',
            showCancelButton: true,
            allowEscapeKey: false,
            allowOutsideClick: false,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Belum',
            confirmButtonText: 'Ya, Sudah!'
        }).then((result) => {
            if (result.isConfirmed) {
                if($('#tbl_import_excel').children().length != 0 ){
                    $.ajax({
                        method: 'GET',
                        dataType: 'JSON',
                        url: `${baseUrl}/operator/master_siswa/update_excel`,
                        beforeSend(xhr) {
                            swal.fire({
                                title: `Tunggu, sedang update data...`,
                                allowEscapeKey: false,
                                allowOutsideClick: false,
                                width: 450,
                                didOpen: () => {
                                    swal.showLoading();
                                }
                            })
                        },
                        success: function (result) {
                            if (result.status) {
                                Swal.fire({
                                    icon: 'success',
                                    title: `Data berhasil diupdate`,
                                    showConfirmButton: false,
                                    allowEscapeKey: false,
                                    allowOutsideClick: false,
                                    timer: 1500
                                })
                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: `${result.keterangan}`,
                                })
                            }
                            tabel_user.api().ajax.reload()
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            Swal.fire({
                                icon: 'error',
                                title: `Gagal, error pada ajax[${textStatus} - ${errorThrown}]`
                            })
                        },
                    });
                }else{
                    Swal.fire({
                        icon: 'error',
                        title: `Data excel kosong !`
                    })
                }
        }})
    })
}

function reset(){
    $('#id').val('')
    $('#username21').val('')
    $('#password').val('')
    $('#nisn').val('')
    $('#nama').val('')
    $('#alamat').val('')
    $('#no_telp').val('')
    $(`#kelas`).val('').trigger('change')
    $(`#jenis_kelamin`).val('').trigger('change')
}