let baseUrl = `${window.origin}/presensi`
$(document).ready(function () {
    tabel_data.api().ajax.reload()
    komponenTabel()
    button()
})

let tabel_data = $('#tabel_data').dataTable({
    "processing": true,
    "serverSide": true,

    "ajax": {
        "url": `${baseUrl}/operator/presensi_siswa/datatables`,
        "type": "POST",
        "data": function (data) {
            data.tanggal    = moment($('#cari_tanggal').val(), 'DD-MM-YYYY').format("YYYY-MM-DD");
            data.hari       = $('#cari_hari').val();
            data.kelas      = $('#cari_kelas').val();
            data.pelajaran  = $('#cari_pelajaran').val();
            data.pengajar   = $('#cari_pengajar').val();
        }
    },
    "bPaginate": true,
    "bLengthChange": false,
    "bFilter": false,
    "bInfo": true,
    "stateSave": true,
    "bAutoWidth": false,
    "ordering": false,
    "pageLength": 12,
    "pagingType": "simple_numbers",
    "scrollX": true,
    "scrollY": "67vh",
    "columnDefs": [
        { className: "text-center", "targets": [1,5] }
    ],
});

function komponenTabel(){
	if (!$('#cari_tanggal').val()) $('#cari_tanggal').val(moment().format('DD-MM-YYYY'))
	$('#cari_tanggal').datetimepicker({
		format: 'DD-MM-YYYY'
	}).on('dp.change', function (e) {
		$("#cari_tanggal").data("DateTimePicker").hide();
        ambil_hari()
        get_pelajaran()
        tabel_data.api().ajax.reload()
	})

    ambil_hari()
	get_kelas()
	get_pelajaran()
	get_pengajar()
    get_cari()
}

function ambil_hari(){
    let tanggal     = $(`#cari_tanggal`).val()

    $.ajax({
        method: 'POST',
        dataType: 'JSON',
        async: false,
        data: { tanggal },
        url: `${baseUrl}/operator/presensi_siswa/ambil_hari`,
        success: function (data) {
            $(`#cari_hari`).val(data.data)
        }
    })
}

function get_kelas(){
    $.ajax({
        method: 'GET',
        dataType: 'JSON',
        url: `${baseUrl}/operator/presensi_siswa/ambil_kelas`,
        success: function (result) {
            let kelas = result.kelas.map((e) => { return { id: e.id, text: `${e.kelas}` } });
            $('#cari_kelas').select2({
                placeholder: 'Pilih kelas',
                allowClear: true,
                theme: "classic",
                data: kelas
            })
        },
        error: function (jqXHR, textStatus, errorThrown) {
            Swal.fire({
                icon: 'error',
                title: `Gagal, error pada ajax [${textStatus} - ${errorThrown}]`
            })
        },
    });
}

function get_pelajaran(){
    $.ajax({
        method: 'GET',
        dataType: 'JSON',
        url: `${baseUrl}/operator/presensi_siswa/ambil_pelajaran`,
        success: function (result) {
            let pelajaran = result.pelajaran.map((e) => { return { id: e.id, text: `${e.pelajaran}` } });
            $('#cari_pelajaran').select2({
                placeholder: 'Pilih pelajaran',
                allowClear: true,
                theme: "classic",
                data: pelajaran
            })
        },
        error: function (jqXHR, textStatus, errorThrown) {
            Swal.fire({
                icon: 'error',
                title: `Gagal, error pada ajax [${textStatus} - ${errorThrown}]`
            })
        },
    });
}

function get_pengajar(){
    $.ajax({
        method: 'GET',
        dataType: 'JSON',
        url: `${baseUrl}/operator/presensi_siswa/ambil_pengajar`,
        success: function (result) {
            let pengajar = result.pengajar.map((e) => { return { id: e.id, text: `${e.nama}` } });
            $('#cari_pengajar').select2({
                placeholder: 'Pilih pengajar',
                allowClear: true,
                theme: "classic",
                data: pengajar
            })
        },
        error: function (jqXHR, textStatus, errorThrown) {
            Swal.fire({
                icon: 'error',
                title: `Gagal, error pada ajax [${textStatus} - ${errorThrown}]`
            })
        },
    });
}

function get_cari(){
    $('#cari_hari').change(function () {
        tabel_data.api().ajax.reload()
    })
    $('#cari_kelas').change(function () {
        tabel_data.api().ajax.reload()
    })
    $('#cari_pelajaran').change(function () {
        tabel_data.api().ajax.reload()
    })
    $('#cari_pengajar').change(function () {
        tabel_data.api().ajax.reload()
    })
}

function lihat_data(id_jadwal, tanggal, kelas, pelajaran, pengajar, hari, txt_kelas, txt_pelajaran){
	$('#modal_data').modal('show')
	$('#txt_kelas').text(txt_kelas)
	$('#txt_pelajaran').text(txt_pelajaran)
    $('#get_tanggal').val(tanggal)
    $('#get_kelas').val(kelas)
    $('#get_pelajaran').val(pelajaran)
    $('#get_pengajar').val(pengajar)
    $('#get_hari').val(hari)

	$.ajax({
        method: 'POST',
        data: { id_jadwal, tanggal, kelas, pelajaran, pengajar, hari },
        dataType: 'JSON',
        url: `${baseUrl}/operator/presensi_siswa/get_data`,
        success: function (data) {
            var html = '';
            no=1;
            for(i=0; i<data.data.length; i++){
                id          = data.data[i].id
                nisn        = data.data[i].nisn
                nama        = data.data[i].nama
                jenis_kelamin = data.data[i].jenis_kelamin
                no_telp     = data.data[i].no_telp
                status      = cek_status(data.data[i].status)
                if(data.data[i].status == 'A'){
                    status = `<button type="button" class="btn btn-danger btn-xs">${status}</button>`
                }else if(data.data[i].status == 'S'){
                    status = `<button type="button" class="btn btn-warning btn-xs">${status}</button>`
                }else{
                    status = `<button type="button" class="btn btn-success btn-xs">${status}</button>`
                }

                html += `
                    <tr>
                        <td class="text-center" width="%">${i+1}</td>
                        <td class="text-left" width="%">${nisn}</td>
                        <td class="text-center" width="%">${nama}</td>
                        <td class="text-center" width="%">${jenis_kelamin}</td>
                        <td class="text-center" width="%">${no_telp}</td>
                        <td class="text-center" width="%">${status}</td>
                        <td class="text-center" width="%" class="ikon-edit">
                            <a href="https://wa.me/${no_telp}?text=Hallo ${nama}" target="_blank">
                                <img src="${baseUrl}/file/wa.png" style="height: 20px; width: auto;"/>
                            </a>
                        </td>
                    </tr>
                `
                no++
            }
            $('#data_kelas').html(html);

            tamp_tanda()
        }
    });
}

function cek_status(sts){
    if(sts=='KL'){
        status = 'Kultum'
    }else if(sts=='UP'){
        status = 'Upacara'
    }else if(sts=='AP'){
        status = 'Apel'
    }else if(sts=='X'){
        status = 'Tidak hadir'
    }else if(sts=='L'){
        status = '9'
    }else if(sts=='P'){
        status = '10'
    }else if(sts=='J'){
        status = '19'
    }else if(sts=='H'){
        status = 'Hadir'
    }else if(sts=='A'){
        status = 'Alfa'
    }else if(sts=='S'){
        status = 'Sakit'
    }else{
        status = 'Tidak Ada'
    }

    return status
}

function button(){
    $('#tandai').click(() => {
        let tanggal     = $(`#get_tanggal`).val()
        let kelas       = $(`#get_kelas`).val()
        let pelajaran   = $(`#get_pelajaran`).val()
        let pengajar    = $(`#get_pengajar`).val()
        let hari        = $(`#get_hari`).val()

        $.ajax({
            method: 'POST',
            data: { tanggal, kelas, pelajaran, pengajar, hari },
            dataType: 'JSON',
            url: `${baseUrl}/operator/presensi_siswa/simpan_tanda`,
            success: function (result) {
                if (result.status == 200) {
                    Swal.fire({
                        icon: 'success',
                        title: `${result.keterangan}`,
                        showConfirmButton: false,
                        timer: 1500
                    }).then(() => {
                        tabel_data.api().ajax.reload()
                        tamp_tanda()
                    })
                } else {
                    Swal.fire({
                        icon: 'warning',
                        title: `${result.keterangan}`,
                    })
                }
            }
        });
    })

    $('#cetak').click(function (e) {
        e.preventDefault()
        let tanggal     = $('#cari_tanggal').val()
        let kelas       = $('#cari_kelas').val()
        let hari        = $('#cari_hari').val()

        if(tanggal != '' && kelas != ''){
            printJS({
                printable: `${baseUrl}/operator/presensi_siswa/cetak/${tanggal}/${kelas}/${hari}`,
                type: 'pdf',
                showModal: true,
                modalMessage: "Proses printing, Mohon tunggu ..."
            })
        }else{
            Swal.fire({
                icon: 'warning',
                title: `pilih tanggal dan kelas dahulu !!`,
            })
        }
    });
}

function tamp_tanda(){
    let tanggal     = $(`#get_tanggal`).val()
    let kelas       = $(`#get_kelas`).val()
    let pelajaran   = $(`#get_pelajaran`).val()
    let pengajar    = $(`#get_pengajar`).val()
    let hari        = $(`#get_hari`).val()

    $.ajax({
        method: 'POST',
        data: { tanggal, kelas, pelajaran, pengajar, hari },
        dataType: 'JSON',
        url: `${baseUrl}/operator/presensi_siswa/cek_status_tanda`,
        success: function (data) {
            if(data.data=='ada'){
                $('#text_tanda2').addClass('d-none');
                $('#text_tanda1').removeClass('d-none');
            }else{
                $('#text_tanda1').addClass('d-none');
                $('#text_tanda2').removeClass('d-none');
            }
        }
    });
}