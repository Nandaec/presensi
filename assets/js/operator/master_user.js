let baseUrl = `${window.origin}/presensi`
$(document).ready(function () {
    tabel_user.api().ajax.reload()
    komponenTabel()
    tambah_data()
    button()
})

let tabel_user = $('#tabel_user').dataTable({
    "processing": true,
    "serverSide": true,

    "ajax": {
        "url": `${baseUrl}/operator/master_user/datatables`,
        "type": "POST",
        "data": function (data) {
            data.cari = $('#cari').val();
        }
    },
    "bPaginate": true,
    "bLengthChange": false,
    "bFilter": false,
    "bInfo": true,
    "stateSave": true,
    "bAutoWidth": false,
    "ordering": false,
    "pageLength": 12,
    "pagingType": "simple_numbers",
    "scrollX": true,
    "scrollY": "67vh",
    "columnDefs": [
        { className: "text-center", "targets": [8] }
    ],
});

function komponenTabel() {
    $('#cari').keyup(() => {
        tabel_user.api().ajax.reload()
    })
    get_pelajaran()
    get_jabatan()
}

function get_pelajaran(){
    $.ajax({
        method: 'GET',
        dataType: 'JSON',
        url: `${baseUrl}/operator/master_user/ambil_pelajaran`,
        success: function (result) {
            let pelajaran = result.pelajaran.map((e) => { return { id: e.pelajaran, text: `${e.pelajaran}` } });
            $('#pelajaran').select2({
                placeholder: 'Silahkan pilih',
                allowClear: true,
                theme: "classic",
                data: pelajaran
            })
        },
        error: function (jqXHR, textStatus, errorThrown) {
            Swal.fire({
                icon: 'error',
                title: `Gagal, error pada ajax [${textStatus} - ${errorThrown}]`
            })
        },
    });
}

function get_jabatan(){
    $.ajax({
        method: 'GET',
        dataType: 'JSON',
        url: `${baseUrl}/operator/master_user/ambil_jabatan`,
        success: function (result) {
            let jabatan = result.jabatan.map((e) => { return { id: e.jabatan, text: `${e.jabatan}` } });
            $('#jabatan').select2({
                placeholder: 'Silahkan pilih',
                allowClear: true,
                theme: "classic",
                data: jabatan
            })
        },
        error: function (jqXHR, textStatus, errorThrown) {
            Swal.fire({
                icon: 'error',
                title: `Gagal, error pada ajax [${textStatus} - ${errorThrown}]`
            })
        },
    });
}

function tambah_data(){
    $('#tambah').click(() => {
        reset()
        $('#modal_data').modal('show')
        $('#teks').text('Tambah Data User')
    })
}

function edit_data(id){
    $('#modal_data').modal('show')
    $('#teks').text('Edit Data User')

    $.ajax({
        method: 'POST',
        data: { id },
        dataType: 'JSON',
        url: `${baseUrl}/operator/master_user/get_data`,
        success: function (data) {
            let d = new Date();
            $('#id').val(data.data.id)
            $('#username21').val(data.data.username)
            $('#password').val(data.data.password)
            $('#nip').val(data.data.nip)
            $('#nama').val(data.data.nama)
            $('#no_telp').val(data.data.no_telp)
            $('#nuptk').val(data.data.nuptk)
            $('#golongan').val(data.data.golongan)
            ttd = data.data.ttd
            foto = data.data.foto
            $('#images').prop('src', `${foto ? baseUrl + '/file/foto/' + foto + '?v=' + d.getTime() : baseUrl + '/file/default_img.png' + '?v=' + d.getTime()}`)
            $('#ttd_placeholder').prop('src', `${ttd ? baseUrl + '/file/ttd/' + ttd + '?v=' + d.getTime() : baseUrl + '/file/default_img.png' + '?v=' + d.getTime()}`)
            $(`#jabatan`).val(data.data.jabatan).trigger('change')
            $(`#pelajaran`).val(data.data.pelajaran).trigger('change')
            $(`#akses`).val(data.data.akses).trigger('change')
            $(`#sts_absen`).val(data.data.sts_absen).trigger('change')
        }
    });
}

function hapus_data(id){            
    Swal.fire({
        title: 'Konfimasi hapus ?',
        icon: 'warning',
        showCancelButton: true,
        allowEscapeKey: false,
        allowOutsideClick: false,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Tidak',
        confirmButtonText: 'Ya, Lanjutkan!'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                method: 'POST',
                dataType: 'JSON',
                data: { id },
                url: `${baseUrl}/operator/master_user/hapus`,
                success: function (result) {
                    if (result.status == 200) {
                        Swal.fire({
                            icon: 'success',
                            title: `${result.keterangan}`,
                            showConfirmButton: false,
                            timer: 1500
                        }).then(() => {
                            tabel_user.api().ajax.reload()
                        })
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: `${result.keterangan}`,
                        })
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    Swal.fire({
                        icon: 'error',
                        title: `Gagal simpan, error pada ajax[${textStatus} - ${errorThrown}]`
                    })
                },
            });
        }
    })
}

function button(){
    $('#simpan').click(() => {
        let peringatan = '';
        if (!$('#username21').val()) peringatan += 'Username, '
        if (!$('#password').val()) peringatan += 'Password, '
        if (peringatan) {
            Swal.fire({
                icon: 'error',
                title: `${peringatan} harus diisi !`
            })
        } else {
            // let input_data = $("#input_data").serialize();
            let input_data = new FormData($('#input_data')[0])

            $.ajax({
                method: 'POST',
                dataType: 'JSON',
                data: input_data,
                url: `${baseUrl}/operator/master_user/simpan`,
                processData: false,
                contentType: false,
                success: function (result) {
                    if (result.status == 200) {
                        Swal.fire({
                            icon: 'success',
                            title: `${result.keterangan}`,
                            showConfirmButton: false,
                            timer: 1500
                        }).then(() => {
                            tabel_user.api().ajax.reload(null, false)
                            $('#modal_data').modal('hide')
                            reset()
                        })
                    }else{
                        Swal.fire({
                            icon: 'warning',
                            title: `${result.keterangan}`,
                        })
                    }
                }
            })
        }
    })

    $('#batal').click(() => {
        $('#modal_data').modal('hide')
        reset()
    })
}

function reset(){
    $('#id').val('')
    $('#username21').val('')
    $('#password').val('')
    $('#nip').val('')
    $('#nama').val('')
    $('#nuptk').val('')
    $('#golongan').val('')
    $('#foto').val('')
    $('#ttd').val('')
    $(`#jabatan`).val('').trigger('change')
    $(`#pelajaran`).val('').trigger('change')
    $(`#akses`).val('').trigger('change')
    $(`#sts_absen`).val('').trigger('change')
}