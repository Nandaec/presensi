let baseUrl = `${window.origin}/presensi`
$(document).ready(function () {
    // tabel_data.api().ajax.reload()
    komponen_data()
    button()
})

function komponen_data(){
	if (!$('#tanggal').val()) $('#tanggal').val(moment().format('DD-MM-YYYY'))
    $('#tanggal').datetimepicker({
        format: 'DD-MM-YYYY'
    }).on('dp.change', function (e) {
        $("#tanggal").data("DateTimePicker").hide();
        data_absen()
        // hit_data()
    })
    get_kelas()
    get_pelajaran()
	get_pengajar()
	get_data()
}

function get_kelas(){
    $.ajax({
        method: 'GET',
        dataType: 'JSON',
        url: `${baseUrl}/guru/absen_mapel/ambil_kelas`,
        success: function (result) {
            let kelas = result.kelas.map((e) => { return { id: e.id, text: `${e.kelas}` } });
            $('#kelas').select2({
                placeholder: 'Pilih kelas',
                allowClear: true,
                theme: "classic",
                data: kelas
            })
        },
        error: function (jqXHR, textStatus, errorThrown) {
            Swal.fire({
                icon: 'error',
                title: `Gagal, error pada ajax [${textStatus} - ${errorThrown}]`
            })
        },
    });
}

function get_pelajaran(){
	$('#kelas').change(function () {
		let kelas  = $(`#kelas`).val()
		$(`#pelajaran`).val('').trigger('change')
		$(`#pengajar`).val('').trigger('change')

		$('#pelajaran').select2({
			placeholder: 'Pilih pelajaran',
			allowClear: true,
			theme: "classic",
			ajax :{
				method: 'POST',
				dataType: 'JSON',
				data: { kelas },
				url: `${baseUrl}/guru/absen_mapel/ambil_pelajaran`,
				data: function (params) {
					return {
						q: params.term,
						p: kelas,
					};
				},
				processResults: function (data, params) {
					return {
						results: $.map(data.data, function (e) { return {id: e.id, text: `${e.pelajaran}`}}),
						pagination: {

						}
					};
				},
				cache: false
			}
		})
    });
}

function get_pengajar(){
	$('#pelajaran').change(function () {
		let kelas  = $(`#kelas`).val()
		let pelajaran  = $(`#pelajaran`).val()

		$(`#pengajar`).val('').trigger('change')
		$('#pengajar').select2({
			placeholder: 'Pilih pengajar',
			allowClear: true,
			theme: "classic",
			ajax :{
				method: 'POST',
				dataType: 'JSON',
				data: { kelas, pelajaran },
				url: `${baseUrl}/guru/absen_mapel/ambil_pengajar`,
				data: function (params) {
					return {
						q: params.term,
						p: pelajaran,
						r: kelas,
					};
				},
				processResults: function (data, params) {
					return {
						results: $.map(data.data, function (e) { return {id: e.id, text: `${e.nama}`}}),
						pagination: {

						}
					};
				},
				cache: false
			}
		})
    });
}

function get_data(){
	$('#pengajar').change(function () {
		data_absen()
		// hit_data()
    });
}

function hit_data(){
	let tanggal  	= $(`#tanggal`).val()
	let kelas  		= $(`#kelas`).val()
	let pelajaran  	= $(`#pelajaran`).val()
	let pengajar  	= $(`#pengajar`).val()

	if(kelas!='' && pelajaran!='' && pengajar!=''){
		$.ajax({
			method: 'POST',
			async:false,
			dataType: 'JSON',
			data: { tanggal, kelas, pelajaran, pengajar },
			url: `${baseUrl}/guru/absen_mapel/hit_data`,
			success: function (data) {
				$(`#tot_siswa`).text(data.siswa)
				$(`#tot_hadir`).text(data.absen)
				$(`#tot_x`).text(data.siswa - data.absen)	
			}
		})
	}else{
		$(`#tot_siswa`).text('0')
		$(`#tot_hadir`).text('0')
		$(`#tot_ijin`).text('0')
	}
}

function data_absen(){
	let tanggal  	= $(`#tanggal`).val()
	let kelas  		= $(`#kelas`).val()
	let pelajaran  	= $(`#pelajaran`).val()
	let pengajar  	= $(`#pengajar`).val()

	if(kelas!='' && pelajaran!='' && pengajar!=''){
		$.ajax({
			method: 'POST',
			async:false,
			dataType: 'JSON',
			data: { tanggal, kelas, pelajaran, pengajar },
			url: `${baseUrl}/guru/absen_mapel/ambil_data`,
			success: function (data) {
				var data_presensi = '';
				var i;
				no = 1;
				jum_data = 0
				if(data.data != 'zonk'){
					jum_data = data.data.length
					for (i = 0; i < data.data.length; i++) {
						nisn 	= data.data[i].nisn
						nama 	= data.data[i].nama
						hadir1 	= data.data[i].hadir1
						hadir2 	= data.data[i].hadir2
						waktu 	= data.data[i].waktu

						data_presensi += `
							<tr>
								<td width="10%">
									<input type="text" class="form-control bg-white" name="inp_no[]" id="inp_no${i}" value="${no}" readonly>
									<input type="hidden" class="form-control bg-white" name="inp_id[]" id="inp_id${i}" value="${data.data[i].id}" readonly>
									<input type="hidden" class="form-control bg-white" name="inp_id_siswa[]" id="inp_id_siswa${i}" value="${data.data[i].id_siswa}" readonly>
									<input type="hidden" class="form-control bg-white" name="inp_username[]" id="inp_username${i}" value="${data.data[i].username}" readonly>
									<input type="hidden" class="form-control bg-white" name="inp_kelas[]" id="inp_kelas${i}" value="${data.data[i].kelas}" readonly>
									<input type="hidden" class="form-control bg-white" name="inp_pelajaran[]" id="inp_pelajaran${i}" value="${data.data[i].pelajaran}" readonly>
									<input type="hidden" class="form-control bg-white" name="inp_image[]" id="inp_image${i}" value="${data.data[i].image}" readonly>
									<input type="hidden" class="form-control bg-white" name="inp_latitude[]" id="inp_latitude${i}" value="${data.data[i].latitude}" readonly>
									<input type="hidden" class="form-control bg-white" name="inp_longitude[]" id="inp_longitude${i}" value="${data.data[i].longitude}" readonly>
									<input type="hidden" class="form-control bg-white" name="inp_waktu[]" id="inp_waktu${i}" value="${data.data[i].waktu}" readonly>
									<input type="hidden" class="form-control bg-white" name="inp_jam[]" id="inp_jam${i}" value="${data.data[i].jam}" readonly>
								</td>
								<td width="10%">
									<input type="text" class="form-control bg-white" name="inp_nisn[]" id="inp_nisn${i}" value="${nisn}" readonly>
								</td>
								<td width="35%">
									<input type="text" class="form-control bg-white" name="inp_nama[]" id="inp_nama${i}" value="${nama}" readonly>
								</td>
								<td width="20%" class="body-billing td-bill">
									<select class="form-control search-select-hadir" name="inp_hadir[]" id="inp_hadir${i}" onchange="jum_kehadiran(${jum_data})">
						                <option value="${hadir1}">${hadir2}</option>
						            </select>
								</td>
								<td width="25%">
									<input type="text" class="form-control bg-white" name="inp_waktu[]" id="inp_waktu${i}" value="${waktu}" readonly>
								</td>
							</tr>
						`;
						no++
					}
					$('#data_absen').html(data_presensi)
				}else{
					data_presensi += `
						<tr>
							<td colspan="5">
								<input type="text" class="form-control text-center bg-white" value="Tidak ada presensi kelas" readonly>
							</td>
						</tr>
					`;
					$('#data_absen').html(data_presensi)
				}
				select2()
				jum_kehadiran(jum_data)
				cek_status()
			}
		})
	}else{
		$('#data_absen').html('')
		$(`#tot_siswa`).text('0')
		$(`#tot_hadir`).text('0')
		$(`#tot_x`).text('0')
	}
}

function jum_kehadiran(jum_data){
	hadir=0
	x=0
	for (i = 0; i < jum_data; i++) {
		let hadirnya = $(`#inp_hadir${i}`).val()

		if(hadirnya == 'H'){
			hadir = hadir+1
		}else{
			x = x+1
		}
	}
	$(`#tot_siswa`).text(jum_data)
	$(`#tot_hadir`).text(hadir)
	$(`#tot_x`).text(x)
}

function cek_status(){
	let tanggal  	= $(`#tanggal`).val()
	let kelas  		= $(`#kelas`).val()
	let pelajaran  	= $(`#pelajaran`).val()
	let pengajar  	= $(`#pengajar`).val()

	if(kelas!='' && pelajaran!='' && pengajar!=''){
		$.ajax({
			method: 'POST',
			async:false,
			dataType: 'JSON',
			data: { tanggal, kelas, pelajaran, pengajar },
			url: `${baseUrl}/guru/absen_mapel/cek_status`,
			success: function (data) {
				var i;
				for (i = 0; i < data.data.length; i++) {
					$(`#txt_kelas`).val(data.data[0].kelas)
					$(`#txt_pelajaran`).val(data.data[0].pelajaran)
					$(`#txt_jam_mulai`).val(data.data[0].jam_mulai)
					$(`#txt_jam_akhir`).val(data.data[0].jam_akhir)
					$(`#txt_verifikasi`).val(data.data[0].status)	
				}
			}
		})
	}else{
		$(`#txt_kelas`).val('')
		$(`#txt_pelajaran`).val('')
		$(`#txt_jam_mulai`).val('')
		$(`#txt_jam_akhir`).val('')
		$(`#txt_verifikasi`).val('')
	}
}

function select2(){
	$('.search-select-hadir').select2({
        placeholder: 'Silahkan pilih',
        allowClear: true,
        theme: "classic",
        ajax :{
            dataType: 'JSON',
            url: `${baseUrl}/guru/absen_mapel/select2`,
            data: function (params) {
                return {
                    q: params.term,
                };
            },
            processResults: function (data, params) {
                return {
                    results: $.map(data.select, function (e) { return {id: e.select_id, text: `${e.select_nama}`}}),
                    pagination: {

                    }
                };
            },
            cache: false
        }
    })
}

function button(){
	$('#verifikasi').click(() => {
		let tanggal  	= $(`#tanggal`).val()
		let kelas  		= $(`#kelas`).val()
		let pelajaran  	= $(`#pelajaran`).val()
		let pengajar  	= $(`#pengajar`).val()

		let peringatan = '';
        if (!$('#tanggal').val()) peringatan += 'tanggal, '
        if (!$('#kelas').val()) peringatan += 'kelas, '
        if (!$('#pelajaran').val()) peringatan += 'pelajaran, '
        if (!$('#pengajar').val()) peringatan += 'pengajar, '
        if (peringatan) {
            Swal.fire({
                icon: 'error',
                title: `${peringatan} harus diisi !`
            })
        } else {
        	$.ajax({
        		method: 'POST',
        		dataType: 'JSON',
        		data: { tanggal, kelas, pelajaran, pengajar },
        		url: `${baseUrl}/guru/absen_mapel/cek_verif`,
        		success: function (result) {
        			if (result.data == 'sama') {
        				verifikasi_absen()
        			}else if(result.data == 'terverifikasi'){
        				Swal.fire({
        					icon: 'warning',
        					title: `Presensi sudah terverifikasi`,
        				})
        			}else if(result.data == 'zong'){
        				Swal.fire({
        					icon: 'warning',
        					title: `Absensi kelas belum dilakukan siswa`,
        				})
        			}else{
        				Swal.fire({
        					icon: 'warning',
        					title: `Maaf, bukan akses anda!!`,
        				})
        			}
        		}
        	})
        }
	})
}

function verifikasi_absen(){
	let input_data = $("#input_data").serialize();
	let master_data  = $("#master_data").serialize();

	Swal.fire({
		title: 'Verifikasi Absen ?',
		icon: 'warning',
		showCancelButton: true,
		allowEscapeKey: false,
		allowOutsideClick: false,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		cancelButtonText: 'Tidak',
		confirmButtonText: 'Ya, Lanjutkan!'
	}).then((result) => {
		if (result.isConfirmed) {
			$.ajax({
                method: 'POST',
                dataType: 'JSON',
                data: input_data + '&' + master_data,
                url: `${baseUrl}/guru/absen_mapel/simpan`,
                beforeSend(xhr) {
                    swal.fire({
                        title: `Tunggu, sedang menyimpan data...`,
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                        didOpen: () => {
                            swal.showLoading();
                        }
                    })
                },
                success: function (result) {
                    if (result.status == 200) {
                        Swal.fire({
                            icon: 'success',
                            title: `Berhasil simpan data`
                        }).then((r) => {
                            data_absen()
                        });
                    } else {
                        Swal.fire({
                            icon: 'warning',
                            title: `${result.keterangan}`
                        })
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    Swal.fire({
                        icon: 'error',
                        title: `Gagal, error pada ajax [${textStatus} - ${errorThrown}]`
                    })
                },
            });
		}
	})
}