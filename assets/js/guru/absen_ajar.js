let baseUrl = `${window.origin}/presensi`
$(document).ready(function () {
    // tabel_data.api().ajax.reload()
    komponen_data()
    komponen_check()
    button()
})

function komponen_check(){	
    $('#check_data').click(function () {
        $('input[class=list_data]').not(":disabled").prop('checked', this.checked);
    });
}

function komponen_data(){
	if (!$('#tanggal').val()) $('#tanggal').val(moment().format('DD-MM-YYYY'))
    $('#tanggal').datetimepicker({
        format: 'DD-MM-YYYY'
    }).on('dp.change', function (e) {
        $("#tanggal").data("DateTimePicker").hide();
    	ambil_hari()
    	ambil_pelajaran()
    })
    ambil_hari()
    ambil_kelas()
    get_kelas()
	get_pelajaran()
	get_pengajar()
}

function ambil_hari(){
	let tanggal  	= $(`#tanggal`).val()

	$.ajax({
		method: 'POST',
		dataType: 'JSON',
		async: false,
		data: { tanggal },
		url: `${baseUrl}/guru/absen_ajar/ambil_hari`,
		success: function (data) {
			$(`#hari`).val(data.data)
		}
	})
}

function ambil_kelas(){
    $.ajax({
        method: 'GET',
        dataType: 'JSON',
        url: `${baseUrl}/guru/absen_ajar/ambil_kelas`,
        success: function (result) {
            let kelas = result.kelas.map((e) => { return { id: e.id, text: `${e.kelas}` } });
            $('#kelas').select2({
                placeholder: 'Pilih kelas',
                allowClear: true,
                theme: "classic",
                data: kelas
            })
        },
        error: function (jqXHR, textStatus, errorThrown) {
            Swal.fire({
                icon: 'error',
                title: `Gagal, error pada ajax [${textStatus} - ${errorThrown}]`
            })
        },
    });
}

function get_kelas(){
	$('#kelas').change(function () {
		ambil_pelajaran()
	});
}

function ambil_pelajaran(){
	$(`#pelajaran`).val('').trigger('change')
	$(`#pengajar`).val('').trigger('change')

	let kelas  = $(`#kelas`).val()
	let hari   = $(`#hari`).val()

	$('#pelajaran').select2({
		placeholder: 'Pilih pelajaran',
		allowClear: true,
		theme: "classic",
		ajax :{
			method: 'POST',
			dataType: 'JSON',
			data: { kelas, hari },
			url: `${baseUrl}/guru/absen_ajar/ambil_pelajaran`,
			data: function (params) {
				return {
					q: params.term,
					p: kelas,
					o: hari,
				};
			},
			processResults: function (data, params) {
				return {
					results: $.map(data.data, function (e) { return {id: e.id, text: `${e.pelajaran}`}}),
					pagination: {

					}
				};
			},
			cache: false
		}
	})
}

function get_pelajaran(){
	$('#pelajaran').change(function () {
		ambil_pengajar()
    });
}

function ambil_pengajar(){
	$(`#pengajar`).val('').trigger('change')
	
	let kelas  = $(`#kelas`).val()
	let pelajaran  = $(`#pelajaran`).val()

	$('#pengajar').select2({
		placeholder: 'Pilih pengajar',
		allowClear: true,
		theme: "classic",
		ajax :{
			method: 'POST',
			dataType: 'JSON',
			data: { kelas, pelajaran },
			url: `${baseUrl}/guru/absen_ajar/ambil_pengajar`,
			data: function (params) {
				return {
					q: params.term,
					p: pelajaran,
					r: kelas,
				};
			},
			processResults: function (data, params) {
				return {
					results: $.map(data.data, function (e) { return {id: e.id, text: `${e.nama}`}}),
					pagination: {

					}
				};
			},
			cache: false
		}
	})
}

function get_pengajar(){
	$('#pengajar').change(function () {
		data_absen()
		// hit_data()
    });
}

function data_absen(){
	let tanggal  	= $(`#tanggal`).val()
	let kelas  		= $(`#kelas`).val()
	let pelajaran  	= $(`#pelajaran`).val()
	let pengajar  	= $(`#pengajar`).val()

	if(kelas!='' && pelajaran!='' && pengajar!=''){
		$.ajax({
			method: 'POST',
			async:false,
			dataType: 'JSON',
			data: { tanggal, kelas, pelajaran, pengajar },
			url: `${baseUrl}/guru/absen_ajar/ambil_data`,
			success: function (data) {
				$(`#id_jadwal`).val(data.jadwal.id)

				var data_presensi = '';
				var i;
				no = 1;
				jum_data = 0
				if(data.data != 'zonk'){
					jum_data = data.data.length
					for (i = 0; i < data.data.length; i++) {
						id_siswa 		= data.data[i].id_siswa
						nisn 			= data.data[i].nisn
						nama 			= data.data[i].nama
						jenis_kelamin 	= data.data[i].jenis_kelamin
						jam_mulai 		= data.data[i].jam_mulai
						jam_akhir 		= data.data[i].jam_akhir
						jumlah_jam 		= data.data[i].jumlah_jam
						status 			= data.data[i].status
						status2 		= data.data[i].status2

						data_presensi += `
							<tr>
								<td width="5%">
									<input type="text" class="form-control bg-white" name="inp_no[]" id="inp_no${i}" value="${no}" readonly>
									<input type="hidden" class="form-control bg-white" name="inp_id_siswa[]" id="inp_id_siswa${i}" value="${data.data[i].id_siswa}" readonly>
									<input type="hidden" class="form-control bg-white" name="inp_username[]" id="inp_username${i}" value="${data.data[i].username}" readonly>
								</td>
								<td width="5%" class="text-center bg-white"><input class="list_data" type="checkbox" onchange="check_this(${i},${id_siswa})" id="list_data${i}" name="list_data[]" value="${id_siswa}"></td>
								<td width="15%">
									<input type="text" class="form-control bg-white" name="inp_nisn[]" id="inp_nisn${i}" value="${nisn}" readonly>
									<input type="hidden" class="form-control bg-white" name="inp_jam_mulai[]" id="inp_jam_mulai${i}" value="${jam_mulai}" readonly>
									<input type="hidden" class="form-control bg-white" name="inp_jam_akhir[]" id="inp_jam_akhir${i}" value="${jam_akhir}" readonly>
									<input type="hidden" class="form-control bg-white" name="inp_jumlah_jam[]" id="inp_jumlah_jam${i}" value="${jumlah_jam}" readonly>
								</td>
								<td width="45%">
									<input type="text" class="form-control bg-white" name="inp_nama[]" id="inp_nama${i}" value="${nama}" readonly>
								</td>
								<td width="15%">
									<input type="text" class="form-control bg-white" name="inp_jenis_kelamin[]" id="inp_jenis_kelamin${i}" value="${jenis_kelamin}" readonly>
								</td>
								<td width="15%" class="body-billing td-bill">
									<select class="form-control search-select-hadir" name="inp_status[]" id="inp_status${i}" onchange="jum_kehadiran(${jum_data})">
						                <option value="${status}">${status2}</option>
						            </select>
								</td>
							</tr>
						`;
						no++
					}
					$('#data_absen').html(data_presensi)
				}else{
					data_presensi += `
						<tr>
							<td colspan="5">
								<input type="text" class="form-control text-center bg-white" value="Tidak ada presensi kelas" readonly>
							</td>
						</tr>
					`;
					$('#data_absen').html(data_presensi)
				}
				select2()
				jum_kehadiran(jum_data)
				cek_status()
			}
		})
	}else{
		$('#data_absen').html('')
		$(`#tot_siswa`).text('0')
		$(`#tot_hadir`).text('0')
		$(`#tot_x`).text('0')

		$(`#id_jadwal`).val('')

		$('#txt_verif_a').addClass('d-none');
		$('#txt_verif_n').addClass('d-none');

		$(`#jam_mulai`).val('')
		$(`#jam_akhir`).val('')
	}
}

function check_all(){
	var i;
	for (i = 0; i < jum_data; i++) {
		let inp_status = $(`#inp_status${i}`).val()

		if(inp_status == ''){
			$(`#inp_status${i}`).append(new Option('Hadir', 'H', true, true)).trigger('change');
		}
	}
}

function check_this(i, id_siswa){
	let inp_status = $(`#inp_status${i}`).val()

	if(inp_status == ''){
		$(`#inp_status${i}`).append(new Option('Hadir', 'H', true, true)).trigger('change');
	}
}

function jum_kehadiran(jum_data){
	hadir=0
	x=0
	for (i = 0; i < jum_data; i++) {
		let hadirnya = $(`#inp_status${i}`).val()

		if(hadirnya == 'H'){
			hadir = hadir+1
		}else{
			x = x+1
		}
	}
	$(`#tot_siswa`).text(jum_data)
	$(`#tot_hadir`).text(hadir)
	$(`#tot_x`).text(x)
}

function cek_status(){
	let tanggal  	= $(`#tanggal`).val()
	let kelas  		= $(`#kelas`).val()
	let pelajaran  	= $(`#pelajaran`).val()
	let pengajar  	= $(`#pengajar`).val()

	if(kelas!='' && pelajaran!='' && pengajar!=''){
		$.ajax({
			method: 'POST',
			async:false,
			dataType: 'JSON',
			data: { tanggal, kelas, pelajaran, pengajar },
			url: `${baseUrl}/guru/absen_ajar/cek_status`,
			success: function (data) {
				if(data.data == 'kosong'){
					$('#txt_verif_a').addClass('d-none');
					$('#txt_verif_n').removeClass('d-none');
				}else{
					$('#txt_verif_a').removeClass('d-none');
					$('#txt_verif_n').addClass('d-none');	
				}

				$(`#jam_mulai`).val(data.jadwal.jam_mulai)
				$(`#jam_akhir`).val(data.jadwal.jam_akhir)
			}
		})
	}
}

function select2(){
	$.ajax({
		method: 'POST',
		dataType: 'JSON',
		url: `${baseUrl}/guru/absen_ajar/select2`,
		success: function (data) {
			let select = data.select.map((e) => { return { id: e.select_id, text: e.select_nama } });

			$('.search-select-hadir').select2({
				placeholder: 'Silahkan pilih',
				allowClear: true,
				theme: "classic",
				data: select
			})
		},
		error: function (jqXHR, textStatus, errorThrown) {
			Swal.fire({
				icon: 'error',
				title: `Gagal, error pada ajax [${textStatus} - ${errorThrown}]`
			})
		},
	});
}

function button(){
	$('#verifikasi').click((e) => {
        e.preventDefault()
        
        let master_data = $("#master_data").serialize();
        let input_data 	= $("#input_data").serialize();

        $.ajax({
			method: 'POST',
			dataType: 'JSON',
			data: master_data+ '&' + input_data,
			url: `${baseUrl}/guru/absen_ajar/cek_akses`,
			success: function (data) {
				if(data.data == 'sama'){
			        Swal.fire({
			        	title: 'Simpan data Presensi ?',
			        	icon: 'warning',
			        	showCancelButton: true,
			        	allowEscapeKey: false,
			        	allowOutsideClick: false,
			        	confirmButtonColor: '#3085d6',
			        	cancelButtonColor: '#d33',
			        	cancelButtonText: 'Tidak',
			        	confirmButtonText: 'Ya, Lanjutkan!'
			        }).then((result) => {
			        	if (result.isConfirmed) {
			        		$.ajax({
			        			method: 'POST',
			        			dataType: 'JSON',
			        			data: master_data+ '&' + input_data,
			        			url: `${baseUrl}/guru/absen_ajar/simpan`,
			        			beforeSend(xhr) {
			        				swal.fire({
			        					title: `Tunggu, sedang menyimpan data...`,
			        					allowEscapeKey: false,
			        					allowOutsideClick: false,
			        					didOpen: () => {
			        						swal.showLoading();
			        					}
			        				})
			        			},
			        			success: function (result) {
			        				if (result.status == 200) {
			        					Swal.fire({
			        						icon: 'success',
			        						title: `Berhasil simpan data`
			        					}).then((r) => {
			        						data_absen()
			        						$("#check_data").prop("checked", false);
			        					});
			        				} else {
			        					Swal.fire({
			        						icon: 'error',
			        						title: `${result.keterangan}`
			        					})
			        				}
			        			},
			        			error: function (jqXHR, textStatus, errorThrown) {
			        				Swal.fire({
			        					icon: 'error',
			        					title: `Gagal, error pada ajax [${textStatus} - ${errorThrown}]`
			        				})
			        			},
			        		});
			        	}
			        })
			    }else{
			    	Swal.fire({
			    		icon: 'warning',
			    		title: `Maaf, bukan kelas anda !!`
			    	})
			    }
			}
		})
    });
}