let baseUrl = `${window.origin}/presensi`
$(document).ready(function () {
    tabel.api().ajax.reload()
})

let tabel = $('#tabel').dataTable({
    "processing": true,
    "serverSide": true,

    "ajax": {
        "url": `${baseUrl}/pns/absen_history/datatables`,
        "type": "POST",
        "data": function (data) {

        }
    },
    "bPaginate": true,
    "bLengthChange": false,
    "bFilter": false,
    "bInfo": true,
    "stateSave": true,
    "bAutoWidth": false,
    "ordering": false,
    "pageLength": 12,
    "pagingType": "simple_numbers",
    "scrollX": true,
    "scrollY": "65vh",
    "columnDefs": [
        { className: "text-left", "targets": [1, 2] }
    ],
});

$('#tabel tbody').on('click', 'tr', function () {
    let dt = tabel.api().row(this).data()
    let id = dt[3];
    show_modal(id)
})

function show_modal(id){
    $('#modal_data').modal('show')
    $.ajax({
        method: 'POST',
        data: { id },
        dataType: 'JSON',
        url: `${baseUrl}/pns/absen_history/get_data`,
        success: function (data) {
            $('#nip').text(data.data.nip)
            $('#nama').text(data.data.nama)
            $('#golongan').text(data.data.golongan)
            $('#jabatan').text(data.data.jabatan)
            $('#waktu').text(data.data.waktu)
            $('#latitude').text(data.data.latitude)
            $('#longitude').text(data.data.longitude)
            let d = new Date();
            $('#foto').prop('src', `${baseUrl}/file/pns/${data.data.image}?v=${d.getTime()}`)
        }
    });
}