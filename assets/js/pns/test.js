let baseUrl = `${window.origin}/presensi`
$(document).ready(function () {
	getMyLocation()
	// myMap()
	klik_absen()
})

//////////////////////////////////////////////////////////
function myMap() {
	var lat = '-7.550730';
	var lng = '110.621807';

	var mapCanvas = document.getElementById("map");
	var mapOptions = {
	    center: new google.maps.LatLng(lat, lng),//mengatur posisi awal pada map, silakan sesuaikan masing-masing
	    zoom: 15 //mengatur zoom saat pertama map di load
	}
	var map = new google.maps.Map(mapCanvas, mapOptions);
	var latLng = new google.maps.LatLng(lat, lng);
	var marker = new google.maps.Marker({
		position: latLng,
		map: map,
		title: 'Current location'
	});
	marker.setMap(map);
}
//////////////////////////////////////////////////////////

function displayCurrentLocation(position){
	var lat = position.coords.latitude;
	var lng = position.coords.longitude;
	//diluar jangkauan
	// var lat = '-7.550806';
	// var lng = '110.621637';
	//dalamjangkauan
	// var lat = '-7.550749';
	// var lng = '110.621749';
	$('#lokasi_saya').text(`${lat}, ${lng}`)
	$('#lat_saya').val(`${lat}`)
	$('#long_saya').val(`${lng}`)

	var mapCanvas = document.getElementById("map");
	var mapOptions = {
	    center: new google.maps.LatLng(lat, lng),//mengatur posisi awal pada map, silakan sesuaikan masing-masing
	    zoom: 17 //mengatur zoom saat pertama map di load
	}
	var map = new google.maps.Map(mapCanvas, mapOptions);
	var latLng = new google.maps.LatLng(lat, lng);
	var marker = new google.maps.Marker({
		position: latLng,
		map: map,
		title: 'Current location'
	});
	marker.setMap(map);
}

function getMyLocation(){
	console.log('Entering getMyLocation');
	if(navigator.geolocation){
		navigator.geolocation.getCurrentPosition(displayCurrentLocation, displayError, {
			maximumAge: 3000, 
			timeout: 5000, 
			enableHighAccuracy: true 
		});
	}
	console.log('Exiting getMyLocation');
}

function displayError(error){
	console.log('Error in getting location');
	console.log(error);
}

/* ================================================== */

function klik_absen(){
	$('#start_absen').click((e) => {
        e.preventDefault()
        initialize()
    });
}

function initialize() {
	let lat_absen 	= $('#lat_absen').val()
	let long_absen 	= $('#long_absen').val()
	let lat_saya 	= $('#lat_saya').val()
	let long_saya 	= $('#long_saya').val()

	var center = new google.maps.LatLng(lat_absen, long_absen); //rsi
    var radius_circle = 10; // 30km
    var markerPositions = [
    	{lat: lat_saya, lng: long_saya} //rumah
    ];

    var markers=[];
    var mapOptions = {
    	center: center,
    	zoom: 15,
    	mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById('map'), mapOptions);
    var circle = drawCircle(mapOptions.center, radius_circle);


    for (var i=0; i<markerPositions.length; i++) {
    	markers.push(
    		new google.maps.Marker({
    			position: new google.maps.LatLng(markerPositions[i].lat, markerPositions[i].lng),
    			map: map,
    			draggable: false
    		})
    	);
    }

    for (var i=0; i<markerPositions.length; i++) {
    	var distance = calculateDistance(
    		markers[i].getPosition().lat(),
    		markers[i].getPosition().lng(),
    		circle.getCenter().lat(),
    		circle.getCenter().lng(),
    		"K"
    		);
        if (distance * 1000 < radius_circle) {  // radius is in meter; distance in km
        	Swal.fire({
        		icon: 'warning',
        		title: `Absen Berhasil`,
        		showConfirmButton: false,
        		timer: 1000
        	})
        }else {
          	Swal.fire({
        		icon: 'warning',
        		title: `Lokasi anda diluar jangkauan`,
        		showConfirmButton: false,
        		timer: 1000
        	})
        }
    }

    function drawCircle(center, radius) {
      return new google.maps.Circle({
        strokeColor: '#0000FF',
        strokeOpacity: 0.7,
        strokeWeight: 1,
        fillColor: '#0000FF',
        fillOpacity: 0.15,
        draggable: false,
        map: map,
        center: center,
        radius: radius
      });
    }

    function calculateDistance(lat1, lon1, lat2, lon2, unit) {
      var radlat1 = Math.PI * lat1/180;
      var radlat2 = Math.PI * lat2/180;
      var radlon1 = Math.PI * lon1/180;
      var radlon2 = Math.PI * lon2/180;
      var theta = lon1-lon2;
      var radtheta = Math.PI * theta/180;
      var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
      dist = Math.acos(dist);
      dist = dist * 180/Math.PI;
      dist = dist * 60 * 1.1515;
      if (unit=="K") { dist = dist * 1.609344; }
      if (unit=="N") { dist = dist * 0.8684; }
      return dist;
    }
}