var Main = function () {
    var runSelect2 = function () {
        $(".search-select").select2({
            placeholder: "Silahkan pilih",
            allowClear: true,
            theme: "classic"
        });
        $(".search-select-multiple").select2({
            placeholder: "Silahkan pilih",
        });
    };

    var runJam = function () {
        $('.jam').datetimepicker({
            format: 'HH:mm',
        });
    }

    
    return {
        init: function () {
            runSelect2();
            runJam();
        }
    };
}();