<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="<?=base_url('/assets/logo/shekel.png')?>" type="image/ico" />

	<title><?=$title;?></title>

	<link href="<?=base_url('/assets/backend/vendors/bootstrap/dist/css/bootstrap.min.css')?>" rel="stylesheet">
	<link href="<?=base_url('/assets/backend/vendors/font-awesome/css/font-awesome.min.css')?>" rel="stylesheet">
	<link href="<?=base_url('/assets/backend/plugins/datetimepicker/bootstrap-datetimepicker.min.css')?>" rel="stylesheet">
	<link href="<?=base_url('/assets/backend/plugins/datatables/css/dataTables.bootstrap.min.css')?>" rel="stylesheet">
	<link href="<?=base_url('/assets/backend/plugins/printjs/print.min.css')?>" rel="stylesheet">
	<link href="<?=base_url('/assets/backend/plugins/select2/select2.min.css')?>" rel="stylesheet">
	<link href="<?=base_url('/assets/backend/build/css/custom.css')?>" rel="stylesheet">
	<link href="<?=base_url('/assets/backend/build/custom.css')?>" rel="stylesheet">
	<?=@$file_css ? '<link rel="stylesheet" href="' . base_url('/assets/css/') . $file_css . '?v=' . time() . '">' : ''?>
</head>

<?php 
	$username 	= $this->session->userdata('username');
	$hak_akses 	= $this->session->userdata('hak_akses');

	if($hak_akses == 'SISWA'){
		$cek = $this->db->query("SELECT * FROM siswa WHERE username='$username'")->row();
	}else{
		$cek = $this->db->query("SELECT * FROM user WHERE username='$username'")->row();
	}
	if($cek->layar == 'true'){
		$body = 'nav-sm';
	}else{
		$body = 'nav-md';
	}
?>

<body class="<?=$body;?>">
	<div class="container body">
		<div class="main_container">
			<div class="col-md-3 left_col">
				<div class="left_col scroll-view">
					<?php $this->load->view('auth/menu');?>
				</div>
			</div>

			<!-- top navigation -->
			<div class="top_nav">
				<div class="nav_menu">
					<nav>
						<div class="nav toggle">
							<a id="menu_toggle"><i class="fa fa-bars"></i></a>
						</div>

						<div class="judul-formulir">
							<span><?=$title;?></span>
						</div>

						<!-- <ul class="nav navbar-nav navbar-right">
							<li class="">
								<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
									<img src="<?=base_url();?>/file/default_img.png" alt=""><?= $this->session->userdata('nama');?>
									<span class=" fa fa-angle-down"></span>
								</a>
								<ul class="dropdown-menu dropdown-usermenu pull-right">
									<li><a href="<?=base_url('auth/login/logout')?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
								</ul>
							</li>
						</ul> -->
					</nav>
				</div>
			</div>
			<!-- /top navigation -->

			<!-- page content -->
			<div class="right_col" role="main">
				<?php $this->load->view($page); ?>
			</div>
			<!-- /page content -->

			<!-- footer content -->
			<!-- <footer>
				<div class="pull-right">
					Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
				</div>
				<div class="clearfix"></div>
			</footer> -->
			<!-- /footer content -->
		</div>
	</div>

	<script src="<?=base_url('/assets/backend/vendors/jquery/dist/jquery.min.js')?>"></script>
	<script src="<?=base_url('/assets/backend/vendors/bootstrap/dist/js/bootstrap.min.js')?>"></script>
	<script src="<?=base_url('/assets/backend/plugins/momentjs/moment.min.js')?>"></script>
	<script src="<?=base_url('/assets/backend/plugins/datetimepicker/bootstrap-datetimepicker.min.js')?>"></script>
	<script src="<?=base_url('assets/backend/plugins/datatables/js/jquery.dataTables.min.js')?>"></script>
	<script src="<?=base_url('assets/backend/plugins/datatables/js/datatables.min.js')?>"></script>
	<script src="<?=base_url('assets/backend/plugins/datatables/js/dataTables.bootstrap.min.js')?>"></script>
	<script src="<?=base_url('/assets/backend/plugins/printjs/print.min.js')?>"></script>
	<script src="<?=base_url('/assets/backend/plugins/select2/select2.min.js')?>"></script>
	<script src="<?=base_url('/assets/backend/plugins/sweetalert/sweetalert.min.js')?>"></script>
	<script src="<?=base_url('/assets/backend/plugins/jquery-maskmoney/jquery.maskMoney.js')?>"></script>
	<script src="<?=base_url('/assets/backend/vendors/moment/min/moment.min.js')?>"></script>
	<script src="<?=base_url('/assets/backend/build/js/main.js')?>"></script>
	<script src="<?=base_url('/assets/backend/build/js/custom.min.js')?>"></script>
	<script src="<?=base_url('/assets/js/') . $file_js.'.js'?>"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			Main.init();
		});
	</script>
</body>
</html>
