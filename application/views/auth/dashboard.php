<div class="row tile_count">
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="fa fa-user"></i> Total Guru PNS</span>
        <div class="count blue"><?=$tot_pns;?></div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="fa fa-user"></i> Total Guru</span>
        <div class="count dongker"><?=$tot_guru;?></div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="fa fa-user"></i> Total Tendik</span>
        <div class="count yellow"><?=$tot_tendik;?></div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="fa fa-user"></i> Total Operator</span>
        <div class="count green"><?=$tot_operator;?></div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="fa fa-user"></i> Total Siswa</span>
        <div class="count red"><?=$tot_siswa;?></div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="fa fa-user"></i> Total Kelas</span>
        <div class="count nila"><?=$tot_kelas;?></div>
    </div>
</div>

<div class="col-md-4 col-sm-6 col-xs-12">
    <div class="x_panel">
        <div class="x_title" style="background: white;">
            <h4>Data Siswa <small>Perkelas</small></h4>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="wrap-formulir" style="height: 250px;">
                <table class="" style="width:100%;">
                    <tr>
                        <th>
                            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                <p class="">Kelas</p>
                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                <p class="">Jumlah Siswa</p>
                            </div>
                        </th>
                    </tr>
                </table>
                <table class="tile_info">
                    <?php foreach ($kelas as $kls) { ?>
                    <tr>
                        <td style="width: 200px;">
                            <p><i class="fa fa-square blue"></i><?=$kls->kelas;?> </p>
                        </td>
                        <td>
                            <b>
                                <?= $this->db->query("SELECT * FROM siswa WHERE kelas='$kls->id'")->num_rows();?>
                            </b>
                            Siswa
                        </td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="col-md-4 col-sm-4 col-xs-12">
    <div class="x_panel">
        <div class="x_title" style="background: white;">
        <h4>10 Siswa alfa bulan ini</h4>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="dashboard-widget-content">
                <div class="wrap-formulir" style="height: 240px;">
                    <table class="countries_list">
                        <tbody>
                            <?php foreach ($absen_siswa as $ab) { ?>
                            <tr>
                                <td><?=$ab['nama'];?></td>
                                <td class="fs15 fw700 text-right"><?=$ab['jumlah'];?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- <div class="col-md-3 col-sm-3 col-xs-12 bg-white">
    <div class="x_title">
        <h2>Top Campaign Performance</h2>
        <div class="clearfix"></div>
    </div>

    <div class="col-md-12 col-sm-12 col-xs-6">
        <div>
            <p>Facebook Campaign</p>
            <div class="">
                <div class="progress progress_sm" style="width: 76%;">
                    <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="80"></div>
                </div>
            </div>
        </div>
        <div>
            <p>Twitter Campaign</p>
            <div class="">
                <div class="progress progress_sm" style="width: 76%;">
                    <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="60"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-6">
        <div>
            <p>Conventional Media</p>
            <div class="">
                <div class="progress progress_sm" style="width: 76%;">
                    <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="40"></div>
                </div>
            </div>
        </div>
        <div>
            <p>Bill boards</p>
            <div class="">
                <div class="progress progress_sm" style="width: 76%;">
                    <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="50"></div>
                </div>
            </div>
        </div>
    </div>
</div> -->