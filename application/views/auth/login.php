<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?=base_url('/assets/logo/shekel.png')?>" type="image/ico" />

    <title><?=$title;?> </title>

    <link href="<?=base_url('/assets/backend/vendors/bootstrap/dist/css/bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?=base_url('/assets/backend/vendors/font-awesome/css/font-awesome.min.css')?>" rel="stylesheet">
    <link href="<?=base_url('/assets/backend/vendors/nprogress/nprogress.css')?>" rel="stylesheet">
    <link href="<?=base_url('/assets/backend/vendors/animate.css/animate.min.css')?>" rel="stylesheet">

    <link href="<?=base_url('/assets/backend/build/css/custom.min.css')?>" rel="stylesheet">
</head>

<body class="login" style="background-image: url('<?=base_url('/assets/img/background.png')?>'); background-repeat: repeat-y; ">
    <div>
        <a class="hiddenanchor" id="signup"></a>
        <a class="hiddenanchor" id="signin" style="border: 1px solid #000000;"></a>

        <div class="login_wrapper">
            <div class="animate form login_form p-3"    style="padding:0.8em;border:1.4em solid #227F7D;border-radius: 8px; background-color: #fff;">
                <?=$this->session->flashdata('pesan')?>
                <input type="hidden" id="pesan" value="<?$this->session->flashdata('pesan')?>">
                <section class="login_content">
                    <img src="<?=base_url('/assets/img/nio_smknel.png')?>" style="width: 100px;" class="img-circle">
                    <form method="post" class="form-login">
                        <h1 style="color:#48779e"><b>ABSENSI<br><small style="color:#48779e"><b> SMKN 1 Enam Lingkung</b></small></b></h1>
                        <div>
                            <input type="text" class="form-control" name="username" id="username" placeholder="Username" autocomplete="off">
                        </div>
                        <div>
                            <input type="password" class="form-control" name="password" id="password" placeholder="Password" autocomplete="off">
                        </div>
                        <div>
                            <button type="submit" name="login" class="btn btn-success btn-block sm-lg"><b>MASUK</b></button>
                        </div>

                        <div class="clearfix"></div>

                        <div class="separator">
                          <h6></h6>
                          <p>Copyright © 2022 SMK N.1 Enam Lingkung <br> <small class="text-info">power by <i class="fa fa-google-wallet"></i> CABDIN Padang Pariaman</small></p>
                      </div>
                    </form>
                </section>
            </div>
        </div>
    </div>

    <script src="<?=base_url('/assets/backend/vendors/jquery/dist/jquery.min.js')?>"></script>
    <script type="text/javascript">
        let pesan = $('#pesan').val()
        if(pesan){
            $('#username').focus()
        }
        $('#username').keydown((e) => {
            if (e.key === 'Enter' || e.keyCode === 13) {
                $("#password").focus()
                return false;
            }
        })

    </script>
</body>
</html>