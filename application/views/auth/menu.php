<div class="navbar nav_title" style="border: 0;">
	<a href="<?= base_url();?>auth/dashboard" class="site_title">
		<i class="fa fa-paw"></i>
		<span><?=$teks;?></span>
	</a>
</div>

<div class="clearfix"></div>

<!-- menu profile quick info -->
<?php 
$username = $this->session->userdata('username');
?>
<input type="hidden" id="username" value="<?=$username;?>" readonly="">

<div class="profile clearfix">
	<div class="profile_pic">
		<img style="width: 65px;" src="<?=base_url();?>/file/default_img.png" height="65" alt="..." class="img-circle profile_img">
	</div>
	<div class="profile_info">
		<span>Selamat datang,</span>
		<h2>
			<?php $user = $this->session->userdata('nama');
			$arr = explode(' ',trim($user));
			if(!empty($arr[1])){
				$dua = substr($arr[1], 0,12);
				echo $arr[0].' '.$dua;
			}else{
				echo $user;
			}
			?>
		</h2>
	</div>
</div>
<!-- /menu profile quick info -->

<br />

<?php $session = $this->session->userdata();?>
<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu ters">
	<!-- <div class="wrap-formulir"> -->
	<div class="menu_section">

		<?php $MENU_PNS = ['ADMIN', 'PNS', 'TENDIK'];?>
		<h3 class="<?=in_array($session['hak_akses'], $MENU_PNS) ? '' : 'd-none'?>">Absen PNS & Tendik</h3>
		<ul class="nav side-menu">
			<li class="<?=in_array($session['hak_akses'], $MENU_PNS) ? '' : 'd-none'?>">
				<a href="<?= base_url();?>pns/absen"><i class="fa fa-desktop"></i> Absen PNS & Tendik </a>
			</li>
			<li class="<?=in_array($session['hak_akses'], $MENU_PNS) ? '' : 'd-none'?>">
				<a href="<?= base_url();?>pns/absen_history"><i class="fa fa-desktop"></i> Riwayat Absen </a>
			</li>
		</ul>

		<?php $MENU_GURU = ['ADMIN', 'PNS', 'TENDIK', 'GURU'];?>
		<h3 class="<?=in_array($session['hak_akses'], $MENU_GURU) ? '' : 'd-none'?>">Absen Mengajar</h3>
		<!-- <ul class="nav side-menu">
			<li class="<?=in_array($session['hak_akses'], $MENU_GURU) ? '' : 'd-none'?>">
				<a href="<?= base_url();?>guru/absen"><i class="fa fa-desktop"></i> Absen Mengajar</a>
			</li>
		</ul> -->
		<ul class="nav side-menu">
			<li class="<?=in_array($session['hak_akses'], $MENU_GURU) ? '' : 'd-none'?>"><a><i class="fa fa-desktop"></i> Absen Mengajar <span class="fa fa-chevron-down"></span></a>
				<ul class="nav child_menu">
					<li><a href="<?= base_url();?>guru/absen_ajar">Absen Pelajaran</a></li>
					<!-- <li><a href="<?= base_url();?>guru/rekap_absen">Rekap Absen</a></li> -->
				</ul>
			</li>
		</ul>

		<!-- <?php $MENU_SISWA = ['ADMIN', 'SISWA'];?>
		<h3 class="<?=in_array($session['hak_akses'], $MENU_SISWA) ? '' : 'd-none'?>">Absen Siswa</h3>
		<ul class="nav side-menu">
			<li class="<?=in_array($session['hak_akses'], $MENU_SISWA) ? '' : 'd-none'?>">
				<a href="<?= base_url();?>siswa/absen_siswa"><i class="fa fa-desktop"></i> Absen Siswa</a>
			</li>
			<li class="<?=in_array($session['hak_akses'], $MENU_SISWA) ? '' : 'd-none'?>">
				<a href="<?= base_url();?>siswa/history"><i class="fa fa-desktop"></i> Riwayat Absen</a>
			</li>
		</ul> -->

		<?php $MENU_OPERATOR = ['ADMIN', 'OPERATOR'];?>
		<h3 class="<?=in_array($session['hak_akses'], $MENU_OPERATOR) ? '' : 'd-none'?>">Menu Operator</h3>
		<ul class="nav side-menu">
			<li class="<?=in_array($session['hak_akses'], $MENU_OPERATOR) ? '' : 'd-none'?>"><a><i class="fa fa-desktop"></i> Master Data <span class="fa fa-chevron-down"></span></a>
				<ul class="nav child_menu">
					<li><a href="<?= base_url();?>operator/master_user">Master User</a></li>
					<li><a href="<?= base_url();?>operator/master_siswa">Master Siswa</a></li>
					<li><a href="<?= base_url();?>operator/master_pelajaran">Master Pelajaran</a></li>
					<li><a href="<?= base_url();?>operator/master_jabatan">Master Jabatan</a></li>
					<li><a href="<?= base_url();?>operator/master_kelas">Master Kelas</a></li>
				</ul>
			</li>
		</ul>
		<ul class="nav side-menu">
			<li class="<?=in_array($session['hak_akses'], $MENU_OPERATOR) ? '' : 'd-none'?>"><a><i class="fa fa-desktop"></i> Data Absen <span class="fa fa-chevron-down"></span></a>
				<ul class="nav child_menu">
					<li><a href="<?= base_url();?>operator/presensi_pns">Presensi  Guru</a></li>
					<li><a href="<?= base_url();?>operator/presensi_guru">Presensi  Pengajar</a></li>
					<li><a href="<?= base_url();?>operator/presensi_siswa">Presensi  Siswa</a></li>
				</ul>
			</li>
		</ul>
		<ul class="nav side-menu">
			<li class="<?=in_array($session['hak_akses'], $MENU_OPERATOR) ? '' : 'd-none'?>"><a><i class="fa fa-desktop"></i> Jadwal Pelajaran <span class="fa fa-chevron-down"></span></a>
				<ul class="nav child_menu">
					<li><a href="<?= base_url();?>operator/set_jadwal">Setting Jadwal</a></li>
				</ul>
			</li>
		</ul>
		<ul class="nav side-menu">
			<li class="<?=in_array($session['hak_akses'], $MENU_OPERATOR) ? '' : 'd-none'?>"><a><i class="fa fa-desktop"></i> Lokasi <span class="fa fa-chevron-down"></span></a>
				<ul class="nav child_menu">
					<li><a href="<?= base_url();?>operator/set_lokasi_absen">Setting Lokasi Dinas</a></li>
					<li><a href="<?= base_url();?>operator/set_lokasi">Setting Lokasi Sekolah</a></li>
				</ul>
			</li>
		</ul>
		<ul class="nav side-menu">
			<li class="<?=in_array($session['hak_akses'], $MENU_OPERATOR) ? '' : 'd-none'?>"><a><i class="fa fa-desktop"></i> Profil Sekolah <span class="fa fa-chevron-down"></span></a>
				<ul class="nav child_menu">
					<li><a href="<?= base_url();?>operator/set_profil">Setting Profil</a></li>
				</ul>
			</li>
		</ul>
	</div>
	<!-- </div> -->
</div>
<!-- /sidebar menu -->

<!-- /menu footer buttons -->
<div class="sidebar-footer hidden-small">
	<!-- <a data-toggle="tooltip" data-placement="top" title="Settings">
		<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
	</a>
	<a data-toggle="tooltip" data-placement="top" title="FullScreen">
		<span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
	</a>
	<a data-toggle="tooltip" data-placement="top" title="Lock">
		<span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
	</a> -->
	<a data-toggle="tooltip" data-placement="top" title="Logout" href="<?=base_url('auth/login/logout')?>" style="width: 100%;">
		<span class="glyphicon glyphicon-log-out" aria-hidden="true"></span>
	</a>
</div>
<!-- /menu footer buttons -->