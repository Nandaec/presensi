<!-- map -->
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/css/materialize.min.css"> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script> -->
<style type="text/css">
  #map{
    width: 365px;
    height: 500px;
}
@media (min-width: 755px) {
    #map{
        width: 100%;
        height: 500px;
    }
}
</style>
<!-- map -->

<div id="map"></div>

<!-- <table class="data table table-striped no-margin">
    <tbody>
        <tr>
            <td width="25%">Lokasi Absen</td>
            <td width="1%">:</td>
            <td width="50%"><span id="lokasi_absen"><?= $lat; ?>, <?= $long; ?></span></td>
        </tr>
        <tr>
            <td width="25%">Lokasi Anda</td>
            <td width="1%">:</td>
            <td width="50%"><span id="lokasi_saya"></span></td>
        </tr>
    </tbody>
</table> -->
<br>
<div class="text-center">
    Titik lokasi harus berada pada lokasi yang ditandai diatas, jika diluar wilayah maka absen tidak dapat dilakukan
    <br>
    <button class="btn btn-success text-center" id="start_absen">Mulai Absen</button>
    <br>
    <input type="hidden" id="lat_absen" value="<?= $lat; ?>">
    <input type="hidden" id="long_absen" value="<?= $long; ?>">
    <input type="hidden" id="lat_saya">
    <input type="hidden" id="long_saya">
</div>

<!-- <script src="https://maps.googleapis.com/maps/api/js?callback=myMap"></script> -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?"></script>

<div class="modal fade" id="modal_absen" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-modal-header">
                <h4 class="modal-title" style="display: inline-block"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: white !important"><h4 class="m0 mt3" >X</h4></button>
            </div>
            <div class="modal-body">
                <div class="row mt20 mb20">
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="hidden" name="cetak_no_transaksi" id="cetak_no_transaksi">
                            <input type="hidden" name="cetak_no_rm" id="cetak_no_rm">
                            <input type="hidden" name="cetak_kunjung_id" id="cetak_kunjung_id">
                            <button class="btn btn-danger btn-block mt5" id="btn_cetak_nota_pasien" data-kunjung_id="">Cetak Nota Pasien</button>
                            <button class="btn btn-danger btn-block mt5" id="btn_cetak_nota" data-kunjung_id="">Cetak Nota</button>
                            <button class="btn btn-danger btn-block mt5" id="btn_cetak_kwitansi" data-kunjung_id="">Cetak Kwitansi</button>
                            <button class="btn btn-danger btn-block mt5" id="btn_cetak_klaim" data-kunjung_id="">Cetak Klaim</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>