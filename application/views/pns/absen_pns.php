<!-- map -->
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/css/materialize.min.css"> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script> -->
<style type="text/css">
  #map{
    width: 365px;
    height: 500px;
}
@media (min-width: 755px) {
    #map{
        width: 100%;
        height: 500px;
    }
}
</style>
<!-- map -->

<div id="map"></div>

<!-- <table class="data table table-striped no-margin">
    <tbody>
        <tr>
            <td width="25%">Lokasi Absen</td>
            <td width="1%">:</td>
            <td width="50%"><span id="lokasi_absen"><?= $lat; ?>, <?= $long; ?></span></td>
        </tr>
        <tr>
            <td width="25%">Lokasi Anda</td>
            <td width="1%">:</td>
            <td width="50%"><span id="lokasi_saya"></span></td>
        </tr>
    </tbody>
</table> -->
<br>
<div class="text-center">
    Titik lokasi harus berada pada lokasi yang ditandai diatas, jika diluar wilayah maka absen tidak dapat dilakukan
    <br>
    <button class="btn btn-success text-center" id="start_absen">Mulai Absen</button>
    <br>
    <form id="input_lokasi" class="form-horizontal">
        <input type="hidden" id="lat_radius" name="lat_radius">
        <input type="hidden" id="nama_tempat" name="nama_tempat">
        <input type="hidden" id="lat_absen" name="lat_absen">
        <input type="hidden" id="long_absen" name="long_absen">
        <!-- <input type="hidden" id="lat_absen" value="<?= $lat; ?>">
        <input type="hidden" id="long_absen" value="<?= $long; ?>"> -->
        <input type="hidden" id="lat_saya" name="lat_saya">
        <input type="hidden" id="long_saya" name="long_saya">
    </form>
</div>

<!-- <script src="https://maps.googleapis.com/maps/api/js?callback=myMap"></script> -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
<div class="modal fade" id="modal_absen" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header bg-modal-header">
                <h4 class="modal-title" style="display: inline-block"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: white !important"><h4 class="m0 mt3" >X</h4></button>
            </div>
            <div class="modal-body">
                <!-- <form method="POST" action="storeImage.php"> -->
                <form id="input_absen" class="form-horizontal">
                    <!-- <input type="file" accept="image/*" capture="camera"> -->
                    <!-- <table class="data table table-striped no-margin">
                        <tbody>
                            <tr>
                                <td width="25%">Nama</td>
                                <td width="1%">:</td>
                                <td width="50%">Nanda E Chandra</span></td>
                            </tr>
                            <tr>
                                <td>Waktu</td>
                                <td>:</td>
                                <td>2022-09-21 14:00:00</td>
                            </tr>
                            <tr>
                                <td>Latitude</td>
                                <td>:</td>
                                <td>-7.674266</span></td>
                            </tr>
                            <tr>
                                <td>Longtitude</td>
                                <td>:</td>
                                <td>110.615246</span></td>
                            </tr>
                        </tbody>
                    </table> -->
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-6 text-center">
                            <div id="my_camera"></div>
                            <br/>
                            <input type="hidden" name="image" id="image" class="image-tag">
                            <input type=button value="Ambil Foto" onClick="take_snapshot()">
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 text-center">
                            <div id="results"></div>
                        </div>
                    </div>
                </form>
                <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                    <br/>
                    <button class="btn btn-success" id="simpan">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script language="JavaScript">
    Webcam.set({
        width: 150,
        height: 190,
        image_format: 'jpeg',
        jpeg_quality: 90
    });
  
    Webcam.attach( '#my_camera' );
  
    function take_snapshot() {
        Webcam.snap( function(data_uri) {
            $(".image-tag").val(data_uri);
            document.getElementById('results').innerHTML = '<img src="'+data_uri+'"/>';
        } );
    }
</script>