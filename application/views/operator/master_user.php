<div class="form-group col-sm-8 col-xs-12">
	<button class="btn btn-sm btn-success" id="tambah"> Tambah Data User</button>
</div>
<div class="form-group col-sm-4 col-xs-12">
		<input autocomplete="off" type="text" name="cari" id="cari" class="form-control" placeholder="Pencarian">
</div>

<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
		<table class="table table-bordered table-hover" id="tabel_user">
			<thead>
				<tr>
					<th width="3%">No</th>
					<th width="5%">Username</th>
					<th width="14%">Nama Guru</th>
					<th width="13%">NIP</th>
					<th width="11%">Pangkat Golongan</th>
					<th width="13%">NUPTK</th>
					<th width="13%">Pengajar</th>
					<th width="8%">Jabatan</th>
					<th width="8%">No WA</th>
					<th width="5%">Status</th>
					<th width="7%">Aksi</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
		</div>
	</div>
</div>

<div class="modal fade bs-example-modal-lg" id="modal_data" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title" id="myModalLabel"><span id="teks"></span></h4>
			</div>
			<div class="modal-body">
				<div class="x_content">
					<form id="input_data" method="post" enctype="multipart/form-data" class="form-horizontal">						
						<input type="hidden" class="form-control" name="id" id="id" readonly="">
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12">&nbsp;</div>
							<div class='col-md-3 col-sm-3 col-xs-12 pb10'>
								Foto
								<div class="form-group">
									<img src="<?=base_url('/file/default_img.png')?>" id="images" class="img-thumbnail" style="max-width:200px; max-height:150px" />
								</div>
								<label class="control-label small-label"></label>
								<input type="file" id="foto" name="foto" class="file-input">
							</div>
							<div class='col-md-3 col-sm-3 col-xs-12 pb10'>
								Tanda Tangan
								<div class="form-group">
									<img src="<?=base_url('/file/default_img.png')?>" id="ttd_placeholder" class="img-thumbnail" style="max-width:200px; max-height:150px" />
								</div>
								<label class="control-label small-label"></label>
								<input type="file" id="ttd" name="ttd" class="file-input">
							</div>
							<div class='col-md-6 col-sm-6 col-xs-12 pb10'>
								Username
								<div class="form-group">
									<input type="text" id="username21" name="username" class="form-control col-md-7 col-sm-7 col-xs-12" autocomplete="off">
								</div>
							</div>
							<div class='col-md-6 col-sm-6 col-xs-12 pb10'>
								Password
								<div class="form-group">
									<input type="text" id="password" name="password" class="form-control col-md-7 col-sm-7 col-xs-12" autocomplete="off">
								</div>
							</div>
							<div class='col-md-6 col-sm-6 col-xs-12 pb10'>
								NIP
								<div class="form-group">
									<input type="text" id="nip" name="nip" class="form-control col-md-7 col-sm-7 col-xs-12" autocomplete="off">
								</div>
							</div>
							<div class='col-md-6 col-sm-6 col-xs-12 pb10'>
								Nama Lengkap
								<div class="form-group">
									<input type="text" id="nama" name="nama" class="form-control col-md-7 col-sm-7 col-xs-12" autocomplete="off">
								</div>
							</div>
							<div class='col-md-6 col-sm-6 col-xs-12 pb10'>
								Pangkat Golongan
								<div class="form-group">
									<input type="text" id="golongan" name="golongan" class="form-control col-md-7 col-sm-7 col-xs-12" autocomplete="off">
								</div>
							</div>
							<div class='col-md-6 col-sm-6 col-xs-12 pb10'>
								NUPTK
								<div class="form-group">
									<input type="text" id="nuptk" name="nuptk" class="form-control col-md-7 col-sm-7 col-xs-12" autocomplete="off">
								</div>
							</div>
							<div class='col-md-6 col-sm-6 col-xs-12 pb10'>
								Jabatan
								<div class="form-group">
									<select name="jabatan" id="jabatan" class="form-control search-select" style="width: 100%">
										<option value="">&nbsp;</option>
									</select>
								</div>
							</div>
							<div class='col-md-6 col-sm-6 col-xs-12 pb10'>
								Mengajar
								<div class="form-group">
									<select name="pelajaran" id="pelajaran" class="form-control search-select" style="width: 100%">
										<option value="">&nbsp;</option>
									</select>
								</div>
							</div>
							<div class='col-md-6 col-sm-6 col-xs-12 pb10'>
								No WA
								<div class="form-group">
									<input type="text" id="no_telp" name="no_telp" class="form-control col-md-7 col-sm-7 col-xs-12" autocomplete="off">
								</div>
							</div>
							<div class='col-md-6 col-sm-6 col-xs-12 pb10'>
								Akses
								<div class="form-group">
									<select name="akses" id="akses" class="form-control search-select" style="width: 100%">
										<option value="">&nbsp;</option>
										<option value="PNS">PNS</option>
										<option value="GURU">GURU</option>
										<option value="TENDIK">TENDIK</option>
										<option value="OPERATOR">OPERATOR</option>
									</select>
								</div>
							</div>
							<div class='col-md-6 col-sm-6 col-xs-12 pb10'>
								Akses
								<div class="form-group">
									<select name="sts_absen" id="sts_absen" class="form-control search-select" style="width: 100%">
										<option value="">&nbsp;</option>
										<option value="aktif">Aktif</option>
										<option value="cuti">Cuti</option>
										<option value="ijin">Ijin</option>
										<option value="sakit">Sakit</option>
									</select>
								</div>
							</div>
						</div>
					</form>
					<div class="btn-group pt5 pb10 mt10" style="width: 100%; display: flex; align-items:center; justify-content:center; background-color: #d4d4d4;">
						<button class="btn btn-sm btn-danger" id="simpan">Simpan</button>
						<button class="btn btn-sm btn-danger" id="batal">Batal</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>