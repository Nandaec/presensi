<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="form-group col-sm-2 col-xs-12 pl0">
				<input type="text" id="cari_tanggal" name="cari_tanggal" class="form-control text-center" autocomplete="off">
			</div>
			<div class="form-group col-sm-2 col-xs-12">
				<button type="button" id="cetak" class="btn btn-primary btn-sm text-right">
					Cetak Laporan
				</button>
			</div>
			<div class="form-group col-sm-4 col-xs-12"> </div>
			<div class="form-group col-sm-4 col-xs-12">
				<input autocomplete="off" type="text" name="cari" id="cari" class="form-control" placeholder="Pencarian">
			</div>
			<table class="table table-bordered table-hover" id="tabel_data">
				<thead>
					<tr>
						<th width="%">No</th>
						<th width="%">NIP</th>
						<th width="%">Nama</th>
						<th width="%">Foto</th>
						<th width="%">Latitude</th>
						<th width="%">Longitude</th>
						<th width="%">Radius</th>
						<th width="%">Tempat</th>
						<th width="%">Waktu</th>
						<th width="%">Status</th>
						<th width="%">Aksi</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
</div>

<div class="modal fade bs-example-modal-lg" id="modal_data" data-backdrop="static" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">Detail Absen</h4>
			</div>
			<div class="modal-body">
				<div class="x_content">
					<div class="col-xs-7">
						<h5><strong> Absen Berangkat </strong></h5>
						<table class="data table no-margin">
							<tbody>
								<tr>
									<td width="25%">NIP</td>
									<td width="1%">:</td>
									<td width="50%"><span id="nip_berangkat"></span></td>
								</tr>
								<tr>
									<td>Nama</td>
									<td>:</td>
									<td><span id="nama_berangkat"></span></td>
								</tr>
								<tr>
									<td>Golongan</td>
									<td>:</td>
									<td><span id="golongan_berangkat"></span></td>
								</tr>
								<tr>
									<td>Jabatan</td>
									<td>:</td>
									<td><span id="jabatan_berangkat"></span></td>
								</tr>
								<tr>
									<td>Waktu</td>
									<td>:</td>
									<td><span id="waktu_berangkat"></span></td>
								</tr>
								<tr>
									<td>Latitude</td>
									<td>:</td>
									<td><span id="latitude_berangkat"></span></td>
								</tr>
								<tr>
									<td>Longitude</td>
									<td>:</td>
									<td><span id="longitude_berangkat"></span></td>
								</tr>
								<tr>
									<td>Foto</td>
									<td>:</td>
									<td>
										<img id="foto_berangkat" src="<?=base_url('/file/default_img.png')?>" alt="" class="img-responsive">
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="col-xs-5">
						<h5><strong> Absen Pulang </strong></h5>
						<table class="data table no-margin">
							<tbody>
								<tr>
									<td width="25%">NIP</td>
									<td width="1%">:</td>
									<td width="50%"><span id="nip_pulang"></span></td>
								</tr>
								<tr>
									<td>Nama</td>
									<td>:</td>
									<td><span id="nama_pulang"></span></td>
								</tr>
								<tr>
									<td>Golongan</td>
									<td>:</td>
									<td><span id="golongan_pulang"></span></td>
								</tr>
								<tr>
									<td>Jabatan</td>
									<td>:</td>
									<td><span id="jabatan_pulang"></span></td>
								</tr>
								<tr>
									<td>Waktu</td>
									<td>:</td>
									<td><span id="waktu_pulang"></span></td>
								</tr>
								<tr>
									<td>Latitude</td>
									<td>:</td>
									<td><span id="latitude_pulang"></span></td>
								</tr>
								<tr>
									<td>Longitude</td>
									<td>:</td>
									<td><span id="longitude_pulang"></span></td>
								</tr>
								<tr>
									<td>Foto</td>
									<td>:</td>
									<td>
										<img id="foto_pulang" src="<?=base_url('/file/default_img.png')?>" alt="" class="img-responsive">
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>