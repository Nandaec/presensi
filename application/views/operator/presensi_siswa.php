<!-- <?php
	$mulai_jad = '08:50:00';
	$akhir_jad = '09:30:00';

	$mulai_role = '08:40:00';
	$akhir_role = '09:20:00';

	if($mulai_jad >= $mulai_role && $akhir_jad <= $akhir_role){
		echo "1";
	}else{
		echo "0";
	}
?> -->
<div class="row">
	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="x_title">
			<div class="form_name">Form Jadwal Pelajaran</div>
			<div class="clearfix"></div>
		</div>
		<div class="x_panel">
			<form id="input_data" class="form-horizontal">
				<div class="form-group">
					<label class="control-label small-label col-md-4 col-sm-4 col-xs-4">Tanggal :</label>
					<div class="col-md-8 col-sm-8 col-xs-8 text-center">
						<input type="text" id="cari_tanggal" name="cari_tanggal" class="form-control text-center" autocomplete="off">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label small-label col-md-4 col-sm-4 col-xs-4">Hari :</label>
					<div class="col-md-8 col-sm-8 col-xs-8 text-center">
						<input type="text" name="cari_hari" id="cari_hari" class="form-control text-center" readonly="">
					</div>
				</div>
				<!-- <div class="form-group">
					<label class="control-label small-label col-md-4 col-sm-4 col-xs-4">Hari :</label>
					<div class="col-md-8 col-sm-8 col-xs-8 text-center">
						<select name="cari_hari" id="cari_hari" class="form-control search-select" style="width: 100%"> 
							<option value="">&nbsp;</option> 
							<option value="Senin">Senin</option>
							<option value="Selasa">Selasa</option>
							<option value="Rabu">Rabu</option>
							<option value="Kamis">Kamis</option>
							<option value="Jumat">Jumat</option>
							<option value="Sabtu">Sabtu</option>
						</select> 
					</div>
				</div> -->
				<div class="form-group">
					<label class="control-label small-label col-md-4 col-sm-4 col-xs-4">Kelas :</label>
					<div class="col-md-8 col-sm-8 col-xs-8 text-center">
						<select name="cari_kelas" id="cari_kelas" class="form-control search-select" style="width: 100%"> 
							<option value="">&nbsp;</option> 
						</select> 
					</div>
				</div>
				<div class="form-group">
					<label class="control-label small-label col-md-4 col-sm-4 col-xs-4">Pelajaran :</label>
					<div class="col-md-8 col-sm-8 col-xs-8 text-center">
						<select name="cari_pelajaran" id="cari_pelajaran" class="form-control search-select" style="width: 100%"> 
							<option value="">&nbsp;</option> 
						</select> 
					</div>
				</div>
				<div class="form-group">
					<label class="control-label small-label col-md-4 col-sm-4 col-xs-4">Pengajar :</label>
					<div class="col-md-8 col-sm-8 col-xs-8 text-center">
						<select name="cari_pengajar" id="cari_pengajar" class="form-control search-select" style="width: 100%"> 
							<option value="">&nbsp;</option> 
						</select> 
					</div>
				</div>
				<!-- <br>
				<div class="form-group">
					<label class="control-label small-label col-md-4 col-sm-4 col-xs-4"></label>
					<div class="col-md-8 col-sm-8 col-xs-8 text-center">
						<button class="btn btn-sm btn-primary" id="cetak" style="width: 100%;">Cetak Laporan Harian</button>
					</div>
				</div> -->
			</form>
		</div>
	</div>
	<div class="col-md-8 col-sm-8 col-xs-12">
		<div class="x_title">
			<div class="form_name">Data Absen</div>
		</div>
		<div class="x_panel">
			<div class="form-group col-sm-2 col-xs-12">
				<button type="button" id="cetak" class="btn btn-primary btn-sm text-right">
					Cetak Laporan
				</button>
			</div>
			<table class="table table-bordered table-hover" id="tabel_data">
				<thead>
					<tr>
						<th width="%">No</th>
						<th width="%">Kelas</th>
						<th width="%">Mata Pelajaran</th>
						<th width="%">Pengajar</th>
						<th width="%">Hari</th>
						<th width="%">Jam</th>
						<th width="%">Status</th>
						<th width="%">Aksi</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
</div>

<div class="modal fade bs-example-modal-lg" id="modal_data" tabindex="-1" data-backdrop="static" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg" style="width: 90%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">
					<span>Data absensi kelas</span>
					<span id="txt_kelas" style="color: yellow"></span>
					<span>mata pelajaran</span>
					<span id="txt_pelajaran" style="color: yellow"></span>
					<input type="hidden" class="form-control" id="get_tanggal" readonly="">
					<input type="hidden" class="form-control" id="get_kelas" readonly="">
					<input type="hidden" class="form-control" id="get_pelajaran" readonly="">
					<input type="hidden" class="form-control" id="get_pengajar" readonly="">
					<input type="hidden" class="form-control" id="get_hari" readonly="">
				</h4>
			</div>
			<div class="modal-body">
				<div class="row pt10 pb10">
					<div class="col-md-2 col-sm-2 col-xs-12 pt10">
						<span class="btn btn-success d-none" id="text_tanda1">Sudah direkap</span>
						<span class="btn btn-warning d-none" id="text_tanda2">Belum direkap</span>
					</div>
					<div class="col-md-9 col-sm-8 col-xs-12 text-center">
						<h3>Laporan Harian Absensi Siswa</h3>
					</div>
					<div class="col-md-1 col-sm-1 col-xs-12 pt10">
						<!-- <button type="button" id="cetak" class="btn btn-primary text-right">
							Cetak
						</button> -->
					</div>
				</div>
				<div class="wrap-formulir">
					<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<th width="%">No</th>
								<th width="%">NISN</th>
								<th width="%">Nama Siswa</th>
								<th width="%">Jenis Kelamin</th>
								<th width="%">No WA</th>
								<th width="%">Status</th>
								<th width="%">Aksi</th>
							</tr>
						</thead>
						<tbody id="data_kelas"></tbody>
					</table>
				</div>
				<div class="btn-group pt5 pb5 mt20" id="button" style="width: 100%; display: flex; align-items:center; justify-content:center; background-color: #d4d4d4">
					<button class="btn btn-danger" id="tandai">Verifikasi</button>
				</div>
			</div>
		</div>
	</div>
</div>