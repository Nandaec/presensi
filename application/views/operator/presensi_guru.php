<div class="row">
	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="x_title">
			<div class="form_name">Form Jadwal Pelajaran</div>
			<div class="clearfix"></div>
		</div>
		<div class="x_panel">
			<form id="input_data" class="form-horizontal">
				<div class="form-group">
					<label class="control-label small-label col-md-4 col-sm-4 col-xs-4">Tanggal :</label>
					<div class="col-md-8 col-sm-8 col-xs-8 text-center">
						<input type="text" id="tanggal" name="tanggal" class="form-control text-center" autocomplete="off">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label small-label col-md-4 col-sm-4 col-xs-4">Hari :</label>
					<div class="col-md-8 col-sm-8 col-xs-8 text-center">
						<input type="text" name="hari" id="hari" class="form-control text-center" readonly="">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label small-label col-md-4 col-sm-4 col-xs-4">Kelas :</label>
					<div class="col-md-8 col-sm-8 col-xs-8 text-center">
						<select name="kelas" id="kelas" class="form-control search-select" style="width: 100%"> 
							<option value="">&nbsp;</option> 
						</select> 
					</div>
				</div>
				<div class="form-group">
					<label class="control-label small-label col-md-4 col-sm-4 col-xs-4">Pengajar :</label>
					<div class="col-md-8 col-sm-8 col-xs-8 text-center">
						<select name="pengajar" id="pengajar" class="form-control search-select" style="width: 100%"> 
							<option value="">&nbsp;</option> 
						</select> 
					</div>
				</div>
				<div class="form-group">
					<label class="control-label small-label col-md-4 col-sm-4 col-xs-4">Jam :</label>
					<div class="col-md-8 col-sm-8 col-xs-8 text-center">
						<select name="jam" id="jam" class="form-control search-select" style="width: 100%">
							<option value="">&nbsp;</option> 
							<option value="1">Jam ke 1</option> 
							<option value="2">Jam ke 2</option> 
							<option value="3">Jam ke 3</option> 
							<option value="4">Jam ke 4</option> 
							<option value="5">Jam ke 5</option> 
							<option value="6">Jam ke 6</option> 
							<option value="7">Jam ke 7</option> 
							<option value="8">Jam ke 8</option> 
							<option value="9">Jam ke 9</option> 
							<option value="10">Jam ke 10</option> 
							<option value="11">Jam ke 11</option> 
						</select> 
					</div>
				</div>
			</form>

			<div class="btn-group pt5 pb10 mt10" style="width: 100%; display: flex; align-items:center; justify-content:center; background-color: #d4d4d4;">
				<button class="btn btn-sm btn-danger" id="simpan">Simpan</button>
				<button class="btn btn-sm btn-danger" id="batal">Batal</button>
			</div>
		</div>
	</div>
	<div class="col-md-8 col-sm-8 col-xs-12">
		<div class="x_title">
			<div class="form_name">Data Absen</div>
		</div>
		<div class="x_panel">
			<div class="form-group col-sm-2 col-xs-12">
				<button type="button" id="cetak" class="btn btn-primary btn-sm text-right">
					Cetak Laporan
				</button>
			</div>
			<table class="table table-bordered table-hover" id="tabel_data">
				<thead>
					<tr>
						<th class="text-center" width="%">No</th>
						<th class="text-left" width="%">Nama</th>
						<th class="text-center" width="%">1</th>
						<th class="text-center" width="%">2</th>
						<th class="text-center" width="%">3</th>
						<th class="text-center" width="%">4</th>
						<th class="text-center" width="%">5</th>
						<th class="text-center" width="%">6</th>
						<th class="text-center" width="%">7</th>
						<th class="text-center" width="%">8</th>
						<th class="text-center" width="%">9</th>
						<th class="text-center" width="%">10</th>
						<th class="text-center" width="%">11</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
</div>

<div class="modal fade" id="modal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg" role="document" style="width:30vw; margin-top:10vh">
		<div class="modal-content">
			<div class="modal-header bg-modal-header">
			<h4 class="modal-title" style="display: inline-block" id="title_modal">Data Absen</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: white !important"><h4 class="m0 mt3" >X</h4></button>
			</div>
			<div class="modal-body" style="max-height: 84.5vh; overflow-x:hidden; overflow-y:scroll;">
				<form id="form_absen">
					<input type="hidden" name="e_tanggal" id="e_tanggal">
					<input type="hidden" name="e_pengajar" id="e_pengajar">
					<input type="hidden" name="e_kelas" id="e_kelas">
					<input type="hidden" name="e_jam" id="e_jam">
					<input type="hidden" name="e_hadir" id="e_hadir">
					<div class="form-group mt20">
						<div class="col-md-12 col-sm-12">
							<select name="e_no_urut" id="e_no_urut" class="form-control search-select" style="width: 100%">
								<option value="">&nbsp;</option>
								<option value="x">Tidak Ada Jam</option> 
								<option value="1">Jam ke 1</option> 
								<option value="2">Jam ke 2</option> 
								<option value="3">Jam ke 3</option> 
								<option value="4">Jam ke 4</option> 
								<option value="5">Jam ke 5</option> 
								<option value="6">Jam ke 6</option> 
								<option value="7">Jam ke 7</option> 
								<option value="8">Jam ke 8</option> 
								<option value="9">Jam ke 9</option> 
								<option value="10">Jam ke 10</option> 
								<option value="11">Jam ke 11</option> 
							</select>
						</div>
					</div>
				</form>

			</div>
		</div>
	</div>
</div>