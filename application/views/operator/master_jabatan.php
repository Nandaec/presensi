<div class="form-group col-sm-4 col-xs-12">
	<button class="btn btn-sm btn-success" id="tambah"> Tambah Data Jabatan</button>
</div>
<div class="form-group col-sm-4 col-xs-12">
		<input autocomplete="off" type="text" name="cari" id="cari" class="form-control" placeholder="Pencarian">
</div>

<div class="row">
	<div class="col-md-8 col-sm-8 col-xs-12">
		<div class="x_panel">
		<table class="table table-bordered table-hover" id="tabel_data">
			<thead>
				<tr>
					<th width="10%">No</th>
					<th width="70%">Jabatan</th>
					<th width="20%">Aksi</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
		</div>
	</div>
</div>

<div class="modal fade bs-example-modal-lg" id="modal_data" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title" id="myModalLabel"><span id="teks"></span></h4>
			</div>
			<div class="modal-body">
				<div class="x_content">
					<form id="input_data" class="form-horizontal">						
						<input type="hidden" class="form-control" name="id" id="id" readonly="">
						<div class="row">
							<div class="col-md-12">&nbsp;</div>
							<div class='col-md-12 col-sm-12 col-xs-12 pb10'>
								Nama Jabatan
								<div class="form-group">
									<input type="text" id="jabatan" name="jabatan" class="form-control col-md-7 col-sm-7 col-xs-12" autocomplete="off">
								</div>
							</div>
						</div>
					</form>
					<div class="btn-group pt5 pb10 mt10" style="width: 100%; display: flex; align-items:center; justify-content:center; background-color: #d4d4d4;">
						<button class="btn btn-sm btn-danger" id="simpan">Simpan</button>
						<button class="btn btn-sm btn-danger" id="batal">Batal</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>