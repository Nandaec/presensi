<div class="row">
	<form id="test" method="post" enctype="multipart/form-data" class="form-horizontal">
	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="x_title">
			<div class="form_name">Input Data Sekolah</div>
			<div class="clearfix"></div>
		</div>
		<div class="x_panel">
			<!-- <form id="input_profil" method="post" enctype="multipart/form-data" class="form-horizontal"> -->
				<div class="form-group">
					<label class="control-label small-label col-md-3 col-sm-3 col-xs-3">Nama Sekolah :</label>
					<div class="col-md-8 col-sm-8 col-xs-8 text-center">
						<input type="text" class="form-control" id="sekolah" name="sekolah" autocomplete="off">
						<input type="hidden" class="form-control" id="id" name="id" readonly="" autocomplete="off">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label small-label col-md-3 col-sm-3 col-xs-3">Kepala Sekolah :</label>
					<div class="col-md-8 col-sm-8 col-xs-8 text-center">
						<select name="kepala" id="kepala" class="form-control search-select" style="width: 100%"> 
							<option value="">&nbsp;</option> 
						</select> 
					</div>
				</div>
				<div class="form-group">
					<label class="control-label small-label col-md-3 col-sm-3 col-xs-3">Wakil Kepala Sekolah :</label>
					<div class="col-md-8 col-sm-8 col-xs-8 text-center">
						<select name="wakil" id="wakil" class="form-control search-select" style="width: 100%"> 
							<option value="">&nbsp;</option> 
						</select> 
					</div>
				</div>
				<div class="form-group">
					<label class="control-label small-label col-md-3 col-sm-3 col-xs-3">Wakil Kesiswaan :</label>
					<div class="col-md-8 col-sm-8 col-xs-8 text-center">
						<select name="kesiswaan" id="kesiswaan" class="form-control search-select" style="width: 100%"> 
							<option value="">&nbsp;</option> 
						</select> 
					</div>
				</div>
				<div class="form-group">
					<label class="control-label small-label col-md-3 col-sm-3 col-xs-3">Alamat Sekolah :</label>
					<div class="col-md-8 col-sm-8 col-xs-8 text-center">
						<input type="text" class="form-control" id="alamat" name="alamat" autocomplete="off">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label small-label col-md-3 col-sm-3 col-xs-3">Kode Pos :</label>
					<div class="col-md-8 col-sm-8 col-xs-8 text-center">
						<input type="text" class="form-control" id="kode_pos" name="kode_pos" autocomplete="off">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label small-label col-md-3 col-sm-3 col-xs-3">No Telp. :</label>
					<div class="col-md-8 col-sm-8 col-xs-8 text-center">
						<input type="text" class="form-control" id="telepon" name="telepon" autocomplete="off">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label small-label col-md-3 col-sm-3 col-xs-3">NPSN :</label>
					<div class="col-md-8 col-sm-8 col-xs-8 text-center">
						<input type="text" class="form-control" id="npsn" name="npsn" autocomplete="off">
					</div>
				</div>
			<!-- </form> -->
		</div>

		<div class="x_title">
			<div class="form_name">Tahun Kurikulium</div>
			<div class="clearfix"></div>
		</div>
		<div class="x_panel">
			<!-- <form id="input_kurikulum" method="post" enctype="multipart/form-data" class="form-horizontal"> -->
				<div class="form-group">
					<label class="control-label small-label col-md-3 col-sm-3 col-xs-3">Tahun Ajaran :</label>
					<div class="col-md-8 col-sm-8 col-xs-8 text-center">
						<input type="text" class="form-control" id="thn_ajaran" name="thn_ajaran" autocomplete="off">
					</div>
				</div>
			<!-- </form> -->
		</div>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="x_title">
			<div class="form_name">Input Logo</div>
			<div class="clearfix"></div>
		</div>
		<div class="x_panel">
			<!-- <form id="input_logo" method="post" enctype="multipart/form-data" class="form-horizontal"> -->
				<div class='col-md-6 col-sm-6 col-xs-12 pb10'>
					Logi Provinsi
					<div class="form-group">
						<img src="<?=base_url('/file/default_img.png')?>" id="logo_prov" class="img-thumbnail" style="max-width:200px; max-height:150px" />
					</div>
					<label class="control-label small-label"></label>
					<input type="file" id="image_prov" name="image_prov" class="file-input">
				</div>
				<div class='col-md-6 col-sm-6 col-xs-12 pb10'>
					Logi Sekolah
					<div class="form-group">
						<img src="<?=base_url('/file/default_img.png')?>" id="logo_sekolah" class="img-thumbnail" style="max-width:200px; max-height:150px" />
					</div>
					<label class="control-label small-label"></label>
					<input type="file" id="image_sekolah" name="image_sekolah" class="file-input">
				</div>
			<!-- </form> -->
		</div>
	</div>
	</form>
	<div class="btn-group pt5 pb10 mt10" id="button" style="width: 100%; display: flex; align-items:center; justify-content:center; background-color: #d4d4d4;">
		<button class="btn btn-sm btn-danger" id="simpan">Simpan</button>
	</div>
</div>