<style type="text/css">
  #map{
    width: 365px;
    height: 500px;
}
@media (min-width: 755px) {
    #map{
        width: 100%;
        height: 500px;
    }
}
</style>

<div class="row">
    <div class="col-md-4 col-sm-4 col-xs-12">
        <div class="x_title">
            <div class="form_name">Input Lokasi</div>
            <div class="clearfix"></div>
        </div>
        <div class="x_panel">
            <form id="input_data" class="form-horizontal">
                <input type="hidden" class="form-control" name="id" id="id" readonly="">
                <div class="form-group">
                    <label class="control-label small-label col-md-4 col-sm-4 col-xs-4">Latitude :</label>
                    <div class="col-md-8 col-sm-8 col-xs-8 text-center">
                        <input type="hidden" id="id_lokasi" name="id_lokasi" class="form-control" autocomplete="off">
                        <input type="text" id="latitude" name="latitude" class="form-control" autocomplete="off">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label small-label col-md-4 col-sm-4 col-xs-4">Longitude :</label>
                    <div class="col-md-8 col-sm-8 col-xs-8 text-center">
                        <input type="text" id="longitude" name="longitude" class="form-control" autocomplete="off">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label small-label col-md-4 col-sm-4 col-xs-4">Radius :</label>
                    <div class="col-md-8 col-sm-8 col-xs-8 text-center">
                        <input type="text" id="radius" name="radius" class="form-control" onkeypress="return onlyNumberKey(event)" autocomplete="off">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label small-label col-md-4 col-sm-4 col-xs-4">Nama Tempat :</label>
                    <div class="col-md-8 col-sm-8 col-xs-8 text-center">
                        <input type="text" id="nama_tempat" name="nama_tempat" class="form-control" autocomplete="off">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label small-label col-md-4 col-sm-4 col-xs-4">Keterangan :</label>
                    <div class="col-md-8 col-sm-8 col-xs-8 text-center">
                        <textarea type="text" class="form-control col-md-7 col-sm-7 col-xs-12" name="keterangan" id="keterangan" style="resize: none;"></textarea>
                    </div>
                </div>
            </form>
            <div class="btn-group pt5 pb10 mt10" style="width: 100%; display: flex; align-items:center; justify-content:center; background-color: #d4d4d4;">
                <button class="btn btn-sm btn-danger" id="simpan">Simpan</button>
            </div>
        </div>
    </div>
    <div class="col-md-8 col-sm-8 col-xs-12">
        <div class="x_title">
            <div class="form_name">Data Lokasi</div>
        </div>
        <!-- <div class="x_panel"> -->
            <div id="map"></div>
        <!-- </div> -->
    </div>
</div>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?"></script>