<?php
$this->load->helper(['tanggal_indo']);

// $this->load->library('PDF_FORMAT_SATU');
// $obj_pdf = new PDF_FORMAT_SATU('P', 'mm', 'A4', true, 'UTF-8', false);
$obj_pdf = new TCPDF('p','mm','A4',true,'UTF-8',false);
$obj_pdf->SetCreator(PDF_CREATOR);
$title = "Cetak Laporan";
$obj_pdf->SetTitle($title);
$obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title);
$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$obj_pdf->SetDefaultMonospacedFont('helvetica');
$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
// $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$obj_pdf->SetMargins(10, 10, -1, true);
$obj_pdf->SetAutoPageBreak(true, 30);
$obj_pdf->SetFont('helvetica', '', 9);
$obj_pdf->setFontSubsetting(false);
$obj_pdf->SetPrintHeader(false);
$obj_pdf->AddPage();
ob_start();
?>

<style type="text/css">
	.font14 { font-size: 14px; }
	.font11 { font-size: 11px; }
	.font10 { font-size: 10px; }
	.font9 { font-size: 9px; }
	.font8 { font-size: 8px; }
	.center { text-align: center; }
	.right { text-align: right; }
	.left { text-align: left; }
	.bold { font-weight: bold; }
	.bt { border-top: 1px solid black; }
	.bb { border-bottom: 1px solid black; }
	.bl { border-left: 1px solid black; }
	.br { border-right: 1px solid black; }
	.abu { background-color: #d4d4d4; }
	.green { color: #09942e; }
	.red { color: #fc0341; }
	.orange { color: #fc9803; }
</style>

<?php
$image_prov 	= $this->db->query("SELECT * FROM profil")->row('image_prov');
$image_sekolah 	= $this->db->query("SELECT * FROM profil")->row('image_sekolah');
?>

<table>
	<tbody>
		<tr>
			<td width="15%">
				<img class="logo" style="height: 20mm;width: 20mm;" src="<?php echo base_url();?>file/<?=$image_prov;?>">
			</td>
			<td width="70%">
				<font class="center bold font10">
					PEMERINTAH PROVINSI SUMATERA BARAT<br>
					DINAS PENDIDIKAN<br>
					<?=$profil->sekolah;?>
				</font>
				<br>
				<font class="center font8">
					<?=$profil->alamat;?> Kode POS <?=$profil->kode_pos;?> Telp. <?=$profil->telepon;?> NPSN : <?=$profil->npsn;?>
				</font>
			</td>
			<td width="15%">
				<img class="logo" style="height: 20mm;width: 20mm;" src="<?php echo base_url();?>file/<?=$image_sekolah;?>">
			</td>
		</tr>
	</tbody>
</table>

<div class="center bold bt">
	JADWAL PELAJARAN SESUAI HARI MASUK<br>
	<?= strtoupper($hari);?> / <?=strtoupper(tanggalan($tanggal));?> <br>
	SEMESTER <?=$profil->thn_ajaran;?>
</div>

<div></div>

<table cellpadding="1">
	<tbody>
		<tr class="abu">
			<td width="5%" class="center bold bt bb bl" rowspan="2">No</td>
			<td width="24%" class="center bold bt bb bl" rowspan="2">NAMA</td>
			<td width="64%" class="center bold bt bb bl br" colspan="11">JAM KE</td>
			<td width="6%" class="center bold bt bb bl br" rowspan="2">Total Jam</td>
		</tr>
		<tr class="abu">
			<td class="center bold bt bb bl">1</td>
			<td class="center bold bt bb bl">2</td>
			<td class="center bold bt bb bl">3</td>
			<td class="center bold bt bb bl">4</td>
			<td class="center bold bt bb bl">5</td>
			<td class="center bold bt bb bl">6</td>
			<td class="center bold bt bb bl">7</td>
			<td class="center bold bt bb bl">8</td>
			<td class="center bold bt bb bl">9</td>
			<td class="center bold bt bb bl">10</td>
			<td class="center bold bt bb bl">11</td>
		</tr>
	</tbody>
	<tbody>
		<?php $no=1; foreach ($data as $key) {
		$jadwal = $this->db->query("SELECT SUM(jumlah_jam) as jumlah_jam FROM jadwal WHERE pengajar='$key->pengajar' AND hari='$hari' AND deleted='0'")->row();
		?>
		<tr>
			<td class="center bt bb bl"><?=$no;?></td>
			<td class="bt bb bl"><?=$key->nama;?></td>
			<td class="font8 center bt bb bl">
				<?php
				$dat = $key->satu;
				if(!empty($dat)){
					$am = $this->db->query("SELECT * FROM absen_mengajar WHERE tanggal='$tanggal' AND id_user='$key->pengajar' AND deleted='0'")->row();
					$kls = $this->db->query("SELECT * FROM master_kelas WHERE id='$am->kelas'")->row();
					echo $kls->kelas;
				}
				?>
			</td>
			<td class="font8 center bt bb bl">
				<?php
				$dat = $key->dua;
				if(!empty($dat)){
					$am = $this->db->query("SELECT * FROM absen_mengajar WHERE tanggal='$tanggal' AND id_user='$key->pengajar' AND deleted='0'")->row();
					$kls = $this->db->query("SELECT * FROM master_kelas WHERE id='$am->kelas'")->row();
					echo $kls->kelas;
				}
				?>
			</td>
			<td class="font8 center bt bb bl">
				<?php
				$dat = $key->tiga;
				if(!empty($dat)){
					$am = $this->db->query("SELECT * FROM absen_mengajar WHERE tanggal='$tanggal' AND id_user='$key->pengajar' AND deleted='0'")->row();
					$kls = $this->db->query("SELECT * FROM master_kelas WHERE id='$am->kelas'")->row();
					echo $kls->kelas;
				}
				?>
			</td>
			<td class="font8 center bt bb bl">
				<?php
				$dat = $key->empat;
				if(!empty($dat)){
					$am = $this->db->query("SELECT * FROM absen_mengajar WHERE tanggal='$tanggal' AND id_user='$key->pengajar' AND deleted='0'")->row();
					$kls = $this->db->query("SELECT * FROM master_kelas WHERE id='$am->kelas'")->row();
					echo $kls->kelas;
				}
				?>
			</td>
			<td class="font8 center bt bb bl">
				<?php
				$dat = $key->lima;
				if(!empty($dat)){
					$am = $this->db->query("SELECT * FROM absen_mengajar WHERE tanggal='$tanggal' AND id_user='$key->pengajar' AND deleted='0'")->row();
					$kls = $this->db->query("SELECT * FROM master_kelas WHERE id='$am->kelas'")->row();
					echo $kls->kelas;
				}
				?>
			</td>
			<td class="font8 center bt bb bl">
				<?php
				$dat = $key->enam;
				if(!empty($dat)){
					$am = $this->db->query("SELECT * FROM absen_mengajar WHERE tanggal='$tanggal' AND id_user='$key->pengajar' AND deleted='0'")->row();
					$kls = $this->db->query("SELECT * FROM master_kelas WHERE id='$am->kelas'")->row();
					echo $kls->kelas;
				}
				?>
			</td>
			<td class="font8 center bt bb bl">
				<?php
				$dat = $key->tujuh;
				if(!empty($dat)){
					$am = $this->db->query("SELECT * FROM absen_mengajar WHERE tanggal='$tanggal' AND id_user='$key->pengajar' AND deleted='0'")->row();
					$kls = $this->db->query("SELECT * FROM master_kelas WHERE id='$am->kelas'")->row();
					echo $kls->kelas;
				}
				?>
			</td>
			<td class="font8 center bt bb bl">
				<?php
				$dat = $key->delapan;
				if(!empty($dat)){
					$am = $this->db->query("SELECT * FROM absen_mengajar WHERE tanggal='$tanggal' AND id_user='$key->pengajar' AND deleted='0'")->row();
					$kls = $this->db->query("SELECT * FROM master_kelas WHERE id='$am->kelas'")->row();
					echo $kls->kelas;
				}
				?>
			</td>
			<td class="font8 center bt bb bl">
				<?php
				$dat = $key->sembilan;
				if(!empty($dat)){
					$am = $this->db->query("SELECT * FROM absen_mengajar WHERE tanggal='$tanggal' AND id_user='$key->pengajar' AND deleted='0'")->row();
					$kls = $this->db->query("SELECT * FROM master_kelas WHERE id='$am->kelas'")->row();
					echo $kls->kelas;
				}
				?>
			</td>
			<td class="font8 center bt bb bl">
				<?php
				$dat = $key->sepuluh;
				if(!empty($dat)){
					$am = $this->db->query("SELECT * FROM absen_mengajar WHERE tanggal='$tanggal' AND id_user='$key->pengajar' AND deleted='0'")->row();
					$kls = $this->db->query("SELECT * FROM master_kelas WHERE id='$am->kelas'")->row();
					echo $kls->kelas;
				}
				?>
			</td>
			<td class="font8 center bt bb bl">
				<?php
				$dat = $key->sebelas;
				if(!empty($dat)){
					$am = $this->db->query("SELECT * FROM absen_mengajar WHERE tanggal='$tanggal' AND id_user='$key->pengajar' AND deleted='0'")->row();
					$kls = $this->db->query("SELECT * FROM master_kelas WHERE id='$am->kelas'")->row();
					echo $kls->kelas;
				}
				?>
			</td>
			<td class="font8 center bt bb bl br">
				<?php
				echo $jadwal->jumlah_jam;
				?> Jam
			</td>
		</tr>
		<?php $no++; } ?>
	</tbody>
</table>

<div></div>

<?php
 	$kepala = $this->db->query("SELECT * FROM user WHERE id='$profil->kepala'")->row();
 	$wakil = $this->db->query("SELECT * FROM user WHERE id='$profil->wakil'")->row();
?>

<table style="page-break-inside:avoid;">
	<tbody>
		<tr>
			<td width="5%"></td>
			<td width="50%">Mengetahui,</td>
			<td width="50%">
				Parit Malintang,
				<?php 
				$date = date('Y-m-d');
				echo tanggalan($date);
				?>
			</td>
		</tr>
		<tr>
			<td></td>
			<td>Kepala Sekolah</td>
			<td>Wakil Kurikulum</td>
		</tr>
		<tr>
			<td></td>
			<td>
				<?php
				$ttd = $kepala->ttd;
				if(empty($ttd)){
					$tanda_tangan = 'file/ttd/ttd_default.jpg';
				}else{
					$tanda_tangan = 'file/ttd/'.$ttd;
				}
				?>
				<img src="<?php base_url();?><?=$tanda_tangan;?>" style="height: 65px;">
			</td>
			<td>
				<?php
				$ttd = $wakil->ttd;
				if(empty($ttd)){
					$tanda_tangan = 'file/ttd/ttd_default.jpg';
				}else{
					$tanda_tangan = 'file/ttd/'.$ttd;
				}
				?>
				<img src="<?php base_url();?><?=$tanda_tangan;?>" style="height: 65px;">
			</td>
		</tr>
		<tr>
			<td></td>
			<td class="bold"><?= $kepala->nama;?></td>
			<td class="bold"><?= $wakil->nama;?></td>
		</tr>
		<tr>
			<td></td>
			<td class="bold"><?= $kepala->nip;?></td>
			<td class="bold"><?= $wakil->nip;?></td>
		</tr>
	</tbody>
</table>

<?php
$content = ob_get_contents();
ob_end_clean();
$obj_pdf->writeHTML($content, true, false, true, false, '');
$obj_pdf->Output('output.pdf', 'I');
// $obj_pdf->Output(FCPATH . '/file/dokumen/kasir_nota.pdf', 'F');
?>