<?php
$this->load->helper(['tanggal_indo']);

// $this->load->library('PDF_FORMAT_SATU');
// $obj_pdf = new PDF_FORMAT_SATU('P', 'mm', 'A4', true, 'UTF-8', false);
$obj_pdf = new TCPDF('p','mm','A4',true,'UTF-8',false);
$obj_pdf->SetCreator(PDF_CREATOR);
$title = "Cetak Laporan";
$obj_pdf->SetTitle($title);
$obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title);
$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$obj_pdf->SetDefaultMonospacedFont('helvetica');
$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
// $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$obj_pdf->SetMargins(10, 10, -1, true);
$obj_pdf->SetAutoPageBreak(true, 30);
$obj_pdf->SetFont('helvetica', '', 9);
$obj_pdf->setFontSubsetting(false);
$obj_pdf->SetPrintHeader(false);
$obj_pdf->AddPage();
ob_start();
?>

<style type="text/css">
	.font14 { font-size: 14px; }
	.font11 { font-size: 11px; }
	.font10 { font-size: 10px; }
	.font9 { font-size: 9px; }
	.font8 { font-size: 8px; }
	.center { text-align: center; }
	.right { text-align: right; }
	.left { text-align: left; }
	.bold { font-weight: bold; }
	.bt { border-top: 1px solid black; }
	.bb { border-bottom: 1px solid black; }
	.bl { border-left: 1px solid black; }
	.br { border-right: 1px solid black; }
	.abu { background-color: #d4d4d4; }
	.green { color: #09942e; }
	.red { color: #fc0341; }
	.orange { color: #fc9803; }
</style>

<?php
$wali = $this->db->query("SELECT * FROM user WHERE id='$kelas->wali'")->row();
$image_prov 	= $this->db->query("SELECT * FROM profil")->row('image_prov');
$image_sekolah 	= $this->db->query("SELECT * FROM profil")->row('image_sekolah');
?>

<table>
	<tbody>
		<tr>
			<td width="15%">
				<img class="logo" style="height: 20mm;width: 20mm;" src="<?php echo base_url();?>file/<?=$image_prov;?>">
			</td>
			<td width="70%">
				<font class="center bold font10">
					PEMERINTAH PROVINSI SUMATERA BARAT<br>
					DINAS PENDIDIKAN<br>
					<?=$profil->sekolah;?>
				</font>
				<br>
				<font class="center font8">
					<?=$profil->alamat;?> Kode POS <?=$profil->kode_pos;?> Telp. <?=$profil->telepon;?> NPSN : <?=$profil->npsn;?>
				</font>
			</td>
			<td width="15%">
				<img class="logo" style="height: 20mm;width: 20mm;" src="<?php echo base_url();?>file/<?=$image_sekolah;?>">
			</td>
		</tr>
	</tbody>
</table>

<div class="center bold bt">
	ABSEN HARIAN SEMESTER <?=$profil->thn_ajaran;?><br>
	KELAS: <?=$kelas->kelas;?>
</div>

<table>
	<tbody>
		<tr>
			<td width="16%">Hari</td>
			<td width="2%">:</td>
			<td class="bold" width="20%"><?php echo strtoupper($hari);?></td>
		</tr>
		<tr>
			<td width="16%">Tanggal</td>
			<td width="2%">:</td>
			<td class="bold" width="20%"><?php echo strtoupper(tanggalan($tanggal));?></td>
		</tr>
		<?php $nom=1; foreach ($guru as $gr) { ?>
			<?php if($nom <= 1){ ?>
				<tr>
					<td width="16%">Guru Mengajar</td>
					<td width="2%">:</td>
					<td width="50%"><?=$nom;?>. <?=$gr->nama;?></td>
				</tr>
			<?php } else { ?>
				<tr>
					<td width="16%"></td>
					<td width="2%"></td>
					<td width="50%"><?=$nom;?>. <?=$gr->nama;?></td>
				</tr>
			<?php } ?>
		<?php $nom++; } ?>
	</tbody>
</table>

<div></div>

<table cellpadding="1">
	<tbody>
		<tr class="abu">
			<td width="5%" class="center bold bt bb bl" rowspan="2">No</td>
			<td width="20%" class="center bold bt bb bl" rowspan="2">NAMA</td>
			<td width="7%" class="center bold bt bb bl" rowspan="2">NISN</td>
			<td width="4%" class="center bold bt bb bl" rowspan="2">L/P</td>
			<!-- <td width="5%" class="center bold bt bb bl" rowspan="2">PAGI</td> -->
			<td width="58%" class="center bold bt bb bl br" colspan="11">JAM KE</td>
			<td width="6%" class="center bold bt bb bl br" rowspan="2">KET</td>
		</tr>
		<tr class="abu">
			<td class="center bold bt bb bl">1</td>
			<td class="center bold bt bb bl">2</td>
			<td class="center bold bt bb bl">3</td>
			<td class="center bold bt bb bl">4</td>
			<td class="center bold bt bb bl">5</td>
			<td class="center bold bt bb bl">6</td>
			<td class="center bold bt bb bl">7</td>
			<td class="center bold bt bb bl">8</td>
			<td class="center bold bt bb bl">9</td>
			<td class="center bold bt bb bl">10</td>
			<td class="center bold bt bb bl">11</td>
		</tr>
	</tbody>
	<tbody>
		<?php $no=1; foreach ($data as $key) { ?>
		<tr>
			<td class="center bt bb bl"><?=$no;?></td>
			<td class="bt bb bl"><?=$key['nama'];?></td>
			<td class="center bt bb bl"><?=$key['nisn'];?></td>
			<td class="center bt bb bl"><?=$key['jenis_kelamin'];?></td>
			<!-- <td class="bt bb bl"></td> -->
			<td class="bold center bt bb bl">
				<?php if($key['satu'] == 'H'){ ?>
					<font class="green"><?=$key['satu'];?></font>
				<?php }else if($key['satu'] == 'A'){ ?>
					<font class="red"><?=$key['satu'];?></font>
				<?php }else if($key['satu'] == 'S'){ ?>
					<font class="orange"><?=$key['satu'];?></font>
				<?php }else{ ?>

				<?php } ?>
			</td>
			<td class="bold center bt bb bl">
				<?php if($key['dua'] == 'H'){ ?>
					<font class="green"><?=$key['dua'];?></font>
				<?php }else if($key['dua'] == 'A'){ ?>
					<font class="red"><?=$key['dua'];?></font>
				<?php }else if($key['dua'] == 'S'){ ?>
					<font class="orange"><?=$key['dua'];?></font>
				<?php }else{ ?>

				<?php } ?>
			</td>
			<td class="bold center bt bb bl">
				<?php if($key['tiga'] == 'H'){ ?>
					<font class="green"><?=$key['tiga'];?></font>
				<?php }else if($key['tiga'] == 'A'){ ?>
					<font class="red"><?=$key['tiga'];?></font>
				<?php }else if($key['tiga'] == 'S'){ ?>
					<font class="orange"><?=$key['tiga'];?></font>
				<?php }else{ ?>

				<?php } ?>
			</td>
			<td class="bold center bt bb bl">
				<?php if($key['empat'] == 'H'){ ?>
					<font class="green"><?=$key['empat'];?></font>
				<?php }else if($key['empat'] == 'A'){ ?>
					<font class="red"><?=$key['empat'];?></font>
				<?php }else if($key['empat'] == 'S'){ ?>
					<font class="orange"><?=$key['empat'];?></font>
				<?php }else{ ?>

				<?php } ?>
			</td>
			<td class="bold center bt bb bl">
				<?php if($key['lima'] == 'H'){ ?>
					<font class="green"><?=$key['lima'];?></font>
				<?php }else if($key['lima'] == 'A'){ ?>
					<font class="red"><?=$key['lima'];?></font>
				<?php }else if($key['lima'] == 'S'){ ?>
					<font class="orange"><?=$key['lima'];?></font>
				<?php }else{ ?>

				<?php } ?>
			</td>
			<td class="bold center bt bb bl">
				<?php if($key['enam'] == 'H'){ ?>
					<font class="green"><?=$key['enam'];?></font>
				<?php }else if($key['enam'] == 'A'){ ?>
					<font class="red"><?=$key['enam'];?></font>
				<?php }else if($key['enam'] == 'S'){ ?>
					<font class="orange"><?=$key['enam'];?></font>
				<?php }else{ ?>

				<?php } ?>
			</td>
			<td class="bold center bt bb bl">
				<?php if($key['tujuh'] == 'H'){ ?>
					<font class="green"><?=$key['tujuh'];?></font>
				<?php }else if($key['tujuh'] == 'A'){ ?>
					<font class="red"><?=$key['tujuh'];?></font>
				<?php }else if($key['tujuh'] == 'S'){ ?>
					<font class="orange"><?=$key['tujuh'];?></font>
				<?php }else{ ?>

				<?php } ?>
			</td>
			<td class="bold center bt bb bl">
				<?php if($key['delapan'] == 'H'){ ?>
					<font class="green"><?=$key['delapan'];?></font>
				<?php }else if($key['delapan'] == 'A'){ ?>
					<font class="red"><?=$key['delapan'];?></font>
				<?php }else if($key['delapan'] == 'S'){ ?>
					<font class="orange"><?=$key['delapan'];?></font>
				<?php }else{ ?>

				<?php } ?>
			</td>
			<td class="bold center bt bb bl">
				<?php if($key['sembilan'] == 'H'){ ?>
					<font class="green"><?=$key['sembilan'];?></font>
				<?php }else if($key['sembilan'] == 'A'){ ?>
					<font class="red"><?=$key['sembilan'];?></font>
				<?php }else if($key['sembilan'] == 'S'){ ?>
					<font class="orange"><?=$key['sembilan'];?></font>
				<?php }else{ ?>

				<?php } ?>
			</td>
			<td class="bold center bt bb bl">
				<?php if($key['sepuluh'] == 'H'){ ?>
					<font class="green"><?=$key['sepuluh'];?></font>
				<?php }else if($key['sepuluh'] == 'A'){ ?>
					<font class="red"><?=$key['sepuluh'];?></font>
				<?php }else if($key['sepuluh'] == 'S'){ ?>
					<font class="orange"><?=$key['sepuluh'];?></font>
				<?php }else{ ?>

				<?php } ?>
			</td>
			<td class="bold center bt bb bl">
				<?php if($key['sebelas'] == 'H'){ ?>
					<font class="green"><?=$key['sebelas'] ;?></font>
				<?php }else if($key['sebelas'] == 'A'){ ?>
					<font class="red"><?=$key['sebelas'] ;?></font>
				<?php }else if($key['sebelas'] == 'S'){ ?>
					<font class="orange"><?=$key['sebelas'] ;?></font>
				<?php }else{ ?>

				<?php } ?>
			</td>
			<td class="center bt bb bl br font8"><?=strtoupper($key['keterangan']) ;?></td>
		</tr>
		<?php $no++; } ?>
	</tbody>
</table>

<div></div>

<table>
	<tr>
		<td width="5%"></td>
		<td width="4%">KL</td>
		<td width="2%">:</td>
		<td width="20%">Kultum</td>

		<td width="4%">X</td>
		<td width="2%">:</td>
		<td width="20%">Tidak Hadir</td>

		<td width="4%">J</td>
		<td width="2%">:</td>
		<td width="20%">19</td>

		<td width="4%">S</td>
		<td width="2%">:</td>
		<td width="20%">Sakit</td>
	</tr>
	<tr>
		<td></td>
		<td>UP</td>
		<td>:</td>
		<td>Upacara</td>

		<td>L</td>
		<td>:</td>
		<td>9</td>

		<td>H</td>
		<td>:</td>
		<td>Hadir</td>
	</tr>
	<tr>
		<td></td>
		<td>AP</td>
		<td>:</td>
		<td>Apel</td>

		<td>P</td>
		<td>:</td>
		<td>10</td>

		<td>A</td>
		<td>:</td>
		<td>Alfa</td>
	</tr>
</table>

<div></div>

<table style="page-break-inside:avoid;">
	<tbody>
		<tr>
			<td width="5%"></td>
			<td width="50%">Mengetahui,</td>
			<td width="50%">
				Parit Malintang,
				<?php 
				$date = date('Y-m-d');
				echo tanggalan($date);
				?>
			</td>
		</tr>
		<tr>
			<td></td>
			<td>Kepala Sekolah</td>
			<td>Wali Kelas</td>
		</tr>
		<tr>
			<td></td>
			<td>
				<?php
				$ttd = $mengetahui->ttd;
				if(empty($ttd)){
					$tanda_tangan = 'file/ttd/ttd_default.jpg';
				}else{
					$tanda_tangan = 'file/ttd/'.$ttd;
				}
				?>
				<img src="<?php base_url();?><?=$tanda_tangan;?>" style="height: 65px;">
			</td>
			<td>
				<?php
				$ttd = $wali->ttd;
				if(empty($ttd)){
					$tanda_tangan = 'file/ttd/ttd_default.jpg';
				}else{
					$tanda_tangan = 'file/ttd/'.$ttd;
				}
				?>
				<img src="<?php base_url();?><?=$tanda_tangan;?>" style="height: 65px;">
			</td>
		</tr>
		<tr>
			<td></td>
			<td class="bold"><?= $mengetahui->nama;?></td>
			<td class="bold"><?= $wali->nama;?></td>
		</tr>
		<tr>
			<td></td>
			<td class="bold"><?= $mengetahui->nip;?></td>
			<td class="bold"><?= $wali->nip;?></td>
		</tr>
	</tbody>
</table>

<?php
$content = ob_get_contents();
ob_end_clean();
$obj_pdf->writeHTML($content, true, false, true, false, '');
$obj_pdf->Output('output.pdf', 'I');
// $obj_pdf->Output(FCPATH . '/file/dokumen/kasir_nota.pdf', 'F');
?>
