<div class="form-group col-sm-5 col-xs-12">
	<button class="btn btn-sm btn-success" id="tambah"> Tambah Data Siswa</button>
	<button class="btn btn-sm btn-success" id="import"> Import</button>
</div>
<div class="form-group col-sm-3 col-xs-12">
	<select name="cari_kelas" id="cari_kelas" class="form-control search-select" style="width: 100%">
		<option value="">&nbsp;</option>
	</select>
</div>
<div class="form-group col-sm-4 col-xs-12">
	<input autocomplete="off" type="text" name="cari" id="cari" class="form-control" placeholder="Pencarian">
</div>

<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<table class="table table-bordered table-hover" id="tabel_user">
				<thead>
					<tr>
						<th class="text-center" width="5%">No</th>
						<th class="text-center" width="8%">Username</th>
						<th class="text-center" width="9%">NISN</th>
						<th class="text-center" width="20%">Nama</th>
						<th class="text-center" width="10%">Jenis Kelamin</th>
						<th class="text-center" width="18%">Alamat</th>
						<th class="text-center" width="11%">No telp</th>
						<th class="text-center" width="10%">Kelas</th>
						<th class="text-center" width="7%">Aksi</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
</div>

<div class="modal fade bs-example-modal-lg" id="modal_data" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title" id="myModalLabel"><span id="teks"></span></h4>
			</div>
			<div class="modal-body">
				<div class="x_content">
					<form id="input_data" class="form-horizontal">						
						<input type="hidden" class="form-control" name="id" id="id" readonly="">
						<div class="row">
							<div class="col-md-12">&nbsp;</div>
							<div class='col-md-6 col-sm-6 col-xs-12 pb10'>
								Username
								<div class="form-group">
									<input type="text" id="username21" name="username" class="form-control col-md-7 col-sm-7 col-xs-12" autocomplete="off">
								</div>
							</div>
							<div class='col-md-6 col-sm-6 col-xs-12 pb10'>
								Password
								<div class="form-group">
									<input type="text" id="password" name="password" class="form-control col-md-7 col-sm-7 col-xs-12" autocomplete="off">
								</div>
							</div>
							<div class='col-md-6 col-sm-6 col-xs-12 pb10'>
								NISN
								<div class="form-group">
									<input type="text" id="nisn" name="nisn" class="form-control col-md-7 col-sm-7 col-xs-12" autocomplete="off">
								</div>
							</div>
							<div class='col-md-6 col-sm-6 col-xs-12 pb10'>
								Nama Lengkap
								<div class="form-group">
									<input type="text" id="nama" name="nama" class="form-control col-md-7 col-sm-7 col-xs-12" autocomplete="off">
								</div>
							</div>
							<div class='col-md-6 col-sm-6 col-xs-12 pb10'>
								Jenis Kelamin
								<div class="form-group">
									<select name="jenis_kelamin" id="jenis_kelamin" class="form-control search-select" style="width: 100%">
										<option value="">&nbsp;</option>
										<option value="L">L</option>
										<option value="P">P</option>
									</select>
								</div>
							</div>
							<div class='col-md-6 col-sm-6 col-xs-12 pb10'>
								Kelas
								<div class="form-group">
									<select name="kelas" id="kelas" class="form-control search-select" style="width: 100%">
										<option value="">&nbsp;</option>
									</select>
								</div>
							</div>
							<div class='col-md-6 col-sm-6 col-xs-12 pb10'>
								No Whatsapp
								<div class="form-group">
									<input type="text" id="no_telp" name="no_telp" class="form-control col-md-7 col-sm-7 col-xs-12" autocomplete="off">
								</div>
							</div>
							<div class='col-md-6 col-sm-6 col-xs-12 pb10'>
								Alamat
								<div class="form-group">
									<textarea type="text" class="form-control col-md-7 col-sm-7 col-xs-12" name="alamat" id="alamat" style="resize: none;"></textarea>
								</div>
							</div>
						</div>
					</form>
					<div class="btn-group pt5 pb10 mt10" style="width: 100%; display: flex; align-items:center; justify-content:center; background-color: #d4d4d4;">
						<button class="btn btn-sm btn-danger" id="simpan">Simpan</button>
						<button class="btn btn-sm btn-danger" id="batal">Batal</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal_import" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
  	<div class="modal-dialog modal-lg" role="document" style="width:95vw; ">
		<div class="modal-content">
			<div class="modal-header bg-modal-header">
				<h4 class="modal-title" style="display: inline-block" id="modal_title_detail">import data</h4>
                <button type="button" class="close" id="btn_modal_import_close" style="color: white !important"><h4 class="m0 mt3" >X</h4></button>
            </div>
            <div class="modal-body">
            	<form method="post" id="formulir_import" enctype="multipart/form-data" class="form-horizontal mt20">
            		<div class="form-group">
            			<label class="col-sm-2 control-label small-label pl0 pr0 text-right" >File :</label>
            			<div class="col-sm-10">
            				<input type="file" name="import_file" id="import_file">
            			</div>
            		</div>
            		<div class="form-group">
                        <label class="col-sm-2 control-label small-label pl0 pr0 text-right" >Kelas :</label>
                        <div class="col-sm-10">
                        <select name="import_kelas" id="import_kelas" class="form-control search-select" style="width: 25%">
                        		<option value="">&nbsp;</option>
                        	</select>
                        </div>
                    </div>
            	</form>
            	<div class="form-group mt10">
            		<label class="col-sm-2 control-label small-label pl0 pr0 text-right" ></label>
            		<div class="col-sm-10">
            			<button id="btn_import_excel_import" class="btn btn-sm btn-danger">Import excel</button>
            		</div>
            	</div>
            	<br>
            	<br>
            	<!-- <div class="table-responsive mt20"> -->
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th colspan="7" class="text-center">DATA EXCEL</th>
                                <th colspan="2" class="text-center"><button id="btn_import_excel_update" class="btn btn-sm btn-danger">Update data siswa</button></th>
                            </tr>
                            <tr>
                            	<th class="text-center" width="5%">No</th>
                            	<th class="text-center" width="8%">Username</th>
                            	<th class="text-center" width="7%">Password</th>
                            	<th class="text-center" width="9%">NISN</th>
                            	<th class="text-center" width="20%">Nama</th>
                            	<th class="text-center" width="10%">Jenis Kelamin</th>
                            	<th class="text-center" width="18%">Alamat</th>
                            	<th class="text-center" width="11%">No telp</th>
                            	<th class="text-center" width="10%">Kelas</th>
                            </tr>
                        </thead>
                        <tbody id="tbl_import_excel">
                        </tbody>
                    </table>
                <!-- </div> -->
            </div>
        </div>
	</div>
</div>