<div class="row">
	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="x_title">
			<div class="form_name">Input Jadwal</div>
			<div class="clearfix"></div>
		</div>
		<div class="x_panel">
			<form id="input_data" class="form-horizontal">
				<input type="hidden" class="form-control" name="id" id="id" readonly="">
				<div class="form-group">
					<label class="control-label small-label col-md-4 col-sm-4 col-xs-4">Kelas :</label>
					<div class="col-md-8 col-sm-8 col-xs-8 text-center">
						<select name="kelas" id="kelas" class="form-control search-select" style="width: 100%">
							<option value="">&nbsp;</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label small-label col-md-4 col-sm-4 col-xs-4">Pelajaran :</label>
					<div class="col-md-8 col-sm-8 col-xs-8 text-center">
						<select name="pelajaran" id="pelajaran" class="form-control search-select" style="width: 100%">
							<option value="">&nbsp;</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label small-label col-md-4 col-sm-4 col-xs-4">Pengajar :</label>
					<div class="col-md-8 col-sm-8 col-xs-8 text-center">
						<select name="pengajar" id="pengajar" class="form-control search-select" style="width: 100%">
							<option value="">&nbsp;</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label small-label col-md-4 col-sm-4 col-xs-4">Hari :</label>
					<div class="col-md-8 col-sm-8 col-xs-8 text-center">
						<select name="hari" id="hari" class="form-control search-select" style="width: 100%">
							<option value="">&nbsp;</option>
							<option value="Senin">Senin</option>
							<option value="Selasa">Selasa</option>
							<option value="Rabu">Rabu</option>
							<option value="Kamis">Kamis</option>
							<option value="Jumat">Jumat</option>
							<option value="Sabtu">Sabtu</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label small-label col-md-4 col-sm-4 col-xs-4">Jumlah jam :</label>
					<div class="col-md-8 col-sm-8 col-xs-8 text-center">
						<input type="text" class="form-control text-center" id="jumlah_jam" name="jumlah_jam" onkeypress="return onlyNumberKey(event)" autocomplete="off">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label small-label col-md-4 col-sm-4 col-xs-4">Jam :</label>
					<div class="col-md-8 col-sm-8 col-xs-8 text-center">
						<div class="col-sm-12 col-md-5 col-xs-5 pr0 pl0">
							<input type="text" name="jam_mulai" id="jam_mulai" class="form-control jam text-center" autocomplete="off" placeholder="Jam Mulai">
						</div>
						<div class="col-sm-12 col-md-2 col-xs-2 pr0 pl0">
							<input type="text" value="-" class="form-control text-center" readonly>
						</div>
						<div class="col-sm-12 col-md-5 col-xs-5 pr0 pl0">
							<input type="text" name="jam_akhir" id="jam_akhir" class="form-control jam text-center" autocomplete="off" placeholder="Jam Akhir">
						</div>
					</div>
				</div>
			</form>
			<div class="btn-group pt5 pb10 mt10" style="width: 100%; display: flex; align-items:center; justify-content:center; background-color: #d4d4d4;">
				<button class="btn btn-sm btn-danger" id="simpan">Simpan</button>
				<button class="btn btn-sm btn-danger" id="batal">Batal</button>
			</div>
		</div>
	</div>

	<div class="col-md-8 col-sm-8 col-xs-12">
		<div class="x_title">
			<div class="form_name">Data Jadwal</div>
		</div>
		<div class="x_panel">
			<div class="form-group col-sm-3 col-xs-12 pl0 pr0">
				<select name="cari_hari" id="cari_hari" class="form-control search-select" style="width: 100%"> 
					<option value="">&nbsp;</option> 
					<option value="Senin">Senin</option>
					<option value="Selasa">Selasa</option>
					<option value="Rabu">Rabu</option>
					<option value="Kamis">Kamis</option>
					<option value="Jumat">Jumat</option>
					<option value="Sabtu">Sabtu</option>
				</select> 
			</div>
			<div class="form-group col-sm-3 col-xs-12 pl0 pr0">
				<select name="cari_kelas" id="cari_kelas" class="form-control search-select" style="width: 100%"> 
					<option value="">&nbsp;</option> 
				</select> 
			</div>
			<div class="form-group col-sm-3 col-xs-12 pl0 pr0">
				<select name="cari_pelajaran" id="cari_pelajaran" class="form-control search-select" style="width: 100%"> 
					<option value="">&nbsp;</option> 
				</select> 
			</div>
			<div class="form-group col-sm-3 col-xs-12 pl0 pr0">
				<select name="cari_pengajar" id="cari_pengajar" class="form-control search-select" style="width: 100%"> 
					<option value="">&nbsp;</option> 
				</select> 
			</div>
			<table class="table table-bordered table-hover" id="tabel_data">
				<thead>
					<tr>
						<th width="5%">No</th>
						<th width="7%">Kelas</th>
						<th width="25%">Mata Pelajaran</th>
						<th width="24%">Pengajar</th>
						<th width="7%">Hari</th>
						<th width="11%">Waktu</th>
						<th width="10%">Jam</th>
						<th width="11%">Aksi</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
</div>