<div class="row">
	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="x_title">
			<div class="form_name">Form Jadwal Pelajaran</div>
			<div class="clearfix"></div>
		</div>
		<div class="x_panel">
			<form id="input_data" class="form-horizontal">
				<div class="form-group">
					<label class="control-label small-label col-md-4 col-sm-4 col-xs-4">Tanggal :</label>
					<div class="col-md-8 col-sm-8 col-xs-8 text-center">
						<input type="text" name="tanggal" id="tanggal" class="form-control text-center" autocomplete="off">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label small-label col-md-4 col-sm-4 col-xs-4">Kelas :</label>
					<div class="col-md-8 col-sm-8 col-xs-8 text-center">
						<select name="kelas" id="kelas" class="form-control search-select" style="width: 100%">
							<option value="">&nbsp;</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label small-label col-md-4 col-sm-4 col-xs-4">Pelajaran :</label>
					<div class="col-md-8 col-sm-8 col-xs-8 text-center">
						<select name="pelajaran" id="pelajaran" class="form-control search-select" style="width: 100%">
							<option value="">&nbsp;</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label small-label col-md-4 col-sm-4 col-xs-4">Pengajar :</label>
					<div class="col-md-8 col-sm-8 col-xs-8 text-center">
						<select name="pengajar" id="pengajar" class="form-control search-select" style="width: 100%">
							<option value="">&nbsp;</option>
						</select>
					</div>
				</div>
			</form>
		</div>
		<div class="x_title">
			<div class="form_name">Data Kehadiran</div>
			<div class="clearfix"></div>
		</div>
		<div class="x_panel">
			<div class="row">
				<div class="row tile_count">
					<div class="col-md-4 col-sm-4 col-xs-4 tile_stats_count">
						<span class="count_top">Total Siswa</span>
						<div class="count text-center green"><font id="tot_siswa">0</font></div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-4 tile_stats_count">
						<span class="count_top">Hadir</span>
						<div class="count text-center blue"><font id="tot_hadir">0</font></div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-4 tile_stats_count">
						<span class="count_top">Tidak Hadir</span>
						<div class="count text-center black"><font id="tot_x">0</font></div>
					</div>
				</div>
			</div>
		</div>
		<div class="x_title">
			<div class="form_name">Data Presensi Kelas</div>
			<div class="clearfix"></div>
		</div>
		<div class="x_panel">
			<form id="info_mapel" class="form-horizontal">
				<div class="form-group">
					<label class="control-label small-label col-md-4 col-sm-4 col-xs-4">Kelas :</label>
					<div class="col-md-8 col-sm-8 col-xs-8 text-center">
						<input type="text" name="txt_kelas" id="txt_kelas" class="form-control bg-none" readonly="">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label small-label col-md-4 col-sm-4 col-xs-4">Pelajaran :</label>
					<div class="col-md-8 col-sm-8 col-xs-8 text-center">
						<input type="text" name="txt_pelajaran" id="txt_pelajaran" class="form-control bg-none" readonly="">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label small-label col-md-4 col-sm-4 col-xs-4">Jam Pelajaran :</label>
					<div class="col-md-3 col-sm-3 col-xs-3 pr0 text-center">
						<input type="text" name="txt_jam_mulai" id="txt_jam_mulai" class="form-control bg-none" readonly="">
					</div>
					<div class="col-sm-2 col-md-2 col-xs-2 pr0 pl0">
						<input type="text" value="-" class="form-control text-center bg-none" readonly>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-3 pl0 text-center">
						<input type="text" name="txt_jam_akhir" id="txt_jam_akhir" class="form-control bg-none" readonly="">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label small-label col-md-4 col-sm-4 col-xs-4">Verifikasi :</label>
					<div class="col-md-8 col-sm-8 col-xs-8 text-center">
						<input type="text" name="txt_verifikasi" id="txt_verifikasi" class="form-control bg-none" readonly="">
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="col-md-8 col-sm-8 col-xs-12">
		<div class="x_title">
			<div class="form_name">Data Jadwal</div>
		</div>
		<!-- <div class="table-responsive"> -->
		<div class="wrap-formulir">
			<form id="master_data" class="form-horizontal">
				<table class="log_table">
					<thead class="log-thead">
						<tr>
							<th width="10%">No</th>
							<th width="10%">NISN</th>
							<th width="35%">Nama Siswa</th>
							<th width="20%">Hadir</th>
							<th width="25%">Waktu Absen</th>
						</tr>
					</thead>
					<tbody class="log-tbody" id="data_absen"></tbody>
				</table>
			</form>
		</div>
		<div class="btn-group pt5 pb5 mt20" id="button" style="width: 100%; display: flex; align-items:center; justify-content:center; background-color: #d4d4d4">
			<button class="btn btn-danger" id="verifikasi">Verifikasi</button>
		</div>
		<!-- </div> -->
	</div>
</div>