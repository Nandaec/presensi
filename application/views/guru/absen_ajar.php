<div class="row">
	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="x_title">
			<div class="form_name">Form Jadwal Pelajaran</div>
			<div class="clearfix"></div>
		</div>
		<div class="x_panel">
			<form id="input_data" class="form-horizontal">
				<div class="form-group">
					<label class="control-label small-label col-md-4 col-sm-4 col-xs-4">Tanggal :</label>
					<div class="col-md-8 col-sm-8 col-xs-8 text-center">
						<input type="text" name="tanggal" id="tanggal" class="form-control text-center" autocomplete="off">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label small-label col-md-4 col-sm-4 col-xs-4">Hari :</label>
					<div class="col-md-8 col-sm-8 col-xs-8 text-center">
						<input type="text" name="hari" id="hari" class="form-control text-center" readonly="">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label small-label col-md-4 col-sm-4 col-xs-4">Kelas :</label>
					<div class="col-md-8 col-sm-8 col-xs-8 text-center">
						<select name="kelas" id="kelas" class="form-control search-select" style="width: 100%">
							<option value="">&nbsp;</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label small-label col-md-4 col-sm-4 col-xs-4">Pelajaran :</label>
					<div class="col-md-8 col-sm-8 col-xs-8 text-center">
						<select name="pelajaran" id="pelajaran" class="form-control search-select" style="width: 100%">
							<option value="">&nbsp;</option>
						</select>
						<input type="hidden" class="form-control" name="id_jadwal" id="id_jadwal" readonly="">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label small-label col-md-4 col-sm-4 col-xs-4">Pengajar :</label>
					<div class="col-md-8 col-sm-8 col-xs-8 text-center">
						<select name="pengajar" id="pengajar" class="form-control search-select" style="width: 100%">
							<option value="">&nbsp;</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label small-label col-md-4 col-sm-4 col-xs-4">Jam :</label>
					<div class="col-md-8 col-sm-8 col-xs-8 text-center">
						<div class="col-sm-12 col-md-5 col-xs-5 pr0 pl0">
							<input type="text" name="jam_mulai" id="jam_mulai" class="form-control jam text-center" autocomplete="off" placeholder="Jam Mulai" readonly="">
						</div>
						<div class="col-sm-12 col-md-2 col-xs-2 pr0 pl0">
							<input type="text" value="-" class="form-control text-center" readonly>
						</div>
						<div class="col-sm-12 col-md-5 col-xs-5 pr0 pl0">
							<input type="text" name="jam_akhir" id="jam_akhir" class="form-control jam text-center" autocomplete="off" placeholder="Jam Akhir" readonly="">
						</div>
					</div>
				</div>
			</form>
		</div>
		<div class="x_title">
			<div class="form_name">Data Kehadiran</div>
			<div class="clearfix"></div>
		</div>
		<div class="x_panel">
			<div class="row">
				<div class="row tile_count">
					<div class="col-md-4 col-sm-4 col-xs-4 tile_stats_count">
						<span class="count_top">Total Siswa</span>
						<div class="count text-center green"><font id="tot_siswa">0</font></div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-4 tile_stats_count">
						<span class="count_top">Hadir</span>
						<div class="count text-center blue"><font id="tot_hadir">0</font></div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-4 tile_stats_count">
						<span class="count_top">Tidak Hadir</span>
						<div class="count text-center black"><font id="tot_x">0</font></div>
					</div>
				</div>
			</div>
		</div>
		<div class="x_title">
			<div class="form_name">Status Presensi Kelas</div>
			<div class="clearfix"></div>
		</div>
		<div class="x_panel">
			<div class="text-center">
				<span class="btn btn-success d-none" id="txt_verif_a">Sudah Terverifikasi</span>
				<span class="btn btn-warning d-none" id="txt_verif_n">Belum di Verifikasi</span>
			</div>
		</div>
	</div>
	<div class="col-md-8 col-sm-8 col-xs-12">
		<div class="x_title">
			<div class="form_name">Data Absen</div>
		</div>
		<!-- <div class="table-responsive"> -->
		<div class="wrap-formulir">
			<form id="master_data" class="form-horizontal">
				<table class="log_table">
					<thead class="log-thead">
						<tr>
							<th width="5%">No</th>
							<th widtd="5%" class="text-center"> 
								<input type="checkbox" class="custom-control-input bg-white" id="check_data" onchange="check_all()">
							</th>
							<th width="15%">NISN</th>
							<th width="45%">Nama Siswa</th>
							<th width="15%">Jenis Kelamin</th>
							<th width="15%">Kehadiran</th>
						</tr>
					</thead>
					<tbody class="log-tbody" id="data_absen"></tbody>
				</table>
			</form>
		</div>
		<div class="btn-group pt5 pb5 mt20" id="button" style="width: 100%; display: flex; align-items:center; justify-content:center; background-color: #d4d4d4">
			<button class="btn btn-danger" id="verifikasi">Verifikasi</button>
		</div>
		<!-- </div> -->
	</div>
</div>