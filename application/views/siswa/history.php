<table class="table table-striped table-hover" id="tabel" style="vertical-align: bottom !important;">
    <thead>
        <tr>
            <th width="5%">No</th>
            <th width="25%">Tanggal</th>
            <th width="40%">Pelajaran</th>
            <th width="30%">Lokasi</th>
        </tr>
    </thead>
    <tbody></tbody>
</table>

<div class="modal fade bs-example-modal-lg" id="modal_data" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">Detail Absen</h4>
			</div>
			<div class="modal-body">
				<div class="x_content">
					<table class="data table table-striped no-margin">
						<tbody>
							<tr>
								<td width="25%">NISN</td>
								<td width="1%">:</td>
								<td width="50%"><span id="nisn"></span></td>
							</tr>
							<tr>
								<td>Nama</td>
								<td>:</td>
								<td><span id="nama"></span></td>
							</tr>
							<tr>
								<td>Kelas</td>
								<td>:</td>
								<td><span id="kelas"></span></td>
							</tr>
							<tr>
								<td>Waktu</td>
								<td>:</td>
								<td><span id="waktu"></span></td>
							</tr>
							<tr>
								<td>Latitude</td>
								<td>:</td>
								<td><span id="latitude"></span></td>
							</tr>
							<tr>
								<td>Longitude</td>
								<td>:</td>
								<td><span id="longitude"></span></td>
							</tr>
						</tbody>
					</table>
					<div class="col-xs-3"></div>
					<div class="col-xs-6">
						<img id="foto" src="<?=base_url('/file/default_img.png')?>" alt="" class="img-responsive">
					</div>
					<div class="col-xs-3"></div>
				</div>
			</div>
		</div>
	</div>
</div>