<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Akses
{

    public function cek_login()
    {
        $_this = &get_instance();
        $sess = $_this->session->userdata;

        $OPERATOR       = ['ADMIN', 'OPERATOR'];
        $AKSES_PNS      = ['ADMIN', 'PNS', 'TENDIK'];
        $AKSES_GURU     = ['ADMIN', 'PNS', 'GURU', 'TENDIK'];
        $AKSES_SISWA    = ['ADMIN', 'SISWA'];

        if ($_this->uri->segment(1) == 'auth' && $_this->uri->segment(2) == 'login') {
            if (isset($sess['hak_akses'])) {
                redirect(base_url('auth/dashboard'));
            }
        } 
        else if ($_this->uri->segment(1) == 'auth' && $_this->uri->segment(2) == 'dashboard') {
            if (!isset($sess['hak_akses'])) {
                redirect(base_url('auth/login'));
            }
        }

        else if ($_this->uri->segment(1) == 'operator') {
            if (!isset($sess['hak_akses'])) {
                $_this->session->unset_userdata('nama');
                $_this->session->unset_userdata('username');
                $_this->session->unset_userdata('hak_akses');
                $_this->session->unset_userdata('logged_in');

                $_this->session->set_flashdata('pesan', '<div class="alert alert-danger" role="alert"> Silahkan login dulu.</div>');
                redirect(base_url('auth/login'));
            } else if (in_array($sess['hak_akses'], $OPERATOR)) {
            } else {
                $_this->session->set_flashdata('no_access', "<script>Swal.fire({icon: 'error',title: `Maaf bukan hak akses anda`})</script>");
                redirect(base_url('auth/dashboard'));
            }
        }

        else if ($_this->uri->segment(1) == 'pns') {
            if (!isset($sess['hak_akses'])) {
                $_this->session->unset_userdata('nama');
                $_this->session->unset_userdata('username');
                $_this->session->unset_userdata('hak_akses');
                $_this->session->unset_userdata('logged_in');

                $_this->session->set_flashdata('pesan', '<div class="alert alert-danger" role="alert"> Silahkan login dulu.</div>');
                redirect(base_url('auth/login'));
            } else if (in_array($sess['hak_akses'], $AKSES_PNS)) {
            } else {
                $_this->session->set_flashdata('no_access', "<script>Swal.fire({icon: 'error',title: `Maaf bukan hak akses anda`})</script>");
                redirect(base_url('auth/dashboard'));
            }
        }

        else if ($_this->uri->segment(1) == 'guru') {
            if (!isset($sess['hak_akses'])) {
                $_this->session->unset_userdata('nama');
                $_this->session->unset_userdata('username');
                $_this->session->unset_userdata('hak_akses');
                $_this->session->unset_userdata('logged_in');

                $_this->session->set_flashdata('pesan', '<div class="alert alert-danger" role="alert"> Silahkan login dulu.</div>');
                redirect(base_url('auth/login'));
            } else if (in_array($sess['hak_akses'], $AKSES_GURU)) {
            } else {
                $_this->session->set_flashdata('no_access', "<script>Swal.fire({icon: 'error',title: `Maaf bukan hak akses anda`})</script>");
                redirect(base_url('auth/dashboard'));
            }
        }

        else if ($_this->uri->segment(1) == 'siswa') {
            if (!isset($sess['hak_akses'])) {
                $_this->session->unset_userdata('nama');
                $_this->session->unset_userdata('username');
                $_this->session->unset_userdata('hak_akses');
                $_this->session->unset_userdata('logged_in');

                $_this->session->set_flashdata('pesan', '<div class="alert alert-danger" role="alert"> Silahkan login dulu.</div>');
                redirect(base_url('auth/login'));
            } else if (in_array($sess['hak_akses'], $AKSES_SISWA)) {
            } else {
                $_this->session->set_flashdata('no_access', "<script>Swal.fire({icon: 'error',title: `Maaf bukan hak akses anda`})</script>");
                redirect(base_url('auth/dashboard'));
            }
        }

    }
}