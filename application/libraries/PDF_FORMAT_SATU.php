<?php
class PDF_FORMAT_SATU extends TCPDF
{
    // * 
    // FORMAT PDF UNTUK LAB FULL A4 (ADA ALAMAT DI FOOTER BAWAH)
    // *
    
    public $FONT_FOOT = 7;
    public $NO_DOKUMEN;
    public $TGL_TERBIT;
    public $REVISI;
    public $NAMA_RS = "RSU Islam Boyolali";
    public $ALAMAT = "Jl. Raya Boyolali-Klaten, Kemiri, Mojosongo, Boyolali ";
    public $TELP = "Telp. : 0276 3282158";
    public $WEBSITE = "Website : rsuislamboyolali.com";
    

    public function Header()
    {
    }

    public function Footer()
    {
        $this->SetY(-20);

        $this->SetFont('helvetica', 'B',$this->FONT_FOOT);
        $this->Cell(0, 0, $this->NAMA_RS, 0, 1, 'L', 0, '', 0);
        $this->SetFont('helvetica', '',$this->FONT_FOOT);
        $this->Cell(0, 0, $this->ALAMAT, 0, 1, 'L', 0, '', 0);
        $this->Cell(0, 0, $this->TELP, 0, 1, 'L', 0, '', 0);
        $this->Cell(100, 0, $this->WEBSITE, 0, 0, 'L', 0, '', 0);
        // $this->Cell(90, 0, $this->getPage()."/".$this->getAliasNbPages(), 0, 1, 'R', 0, '', 0);

        // $this->Cell(0, 0, "No.Dok : $this->NO_DOKUMEN Revisi : $this->REVISI Tgl. Terbit : $this->TGL_TERBIT", 0, 1, 'R', 0, '', 0);
    }
}
