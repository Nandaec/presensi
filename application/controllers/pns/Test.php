<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library(['akses']);
        $this->akses->cek_login();
    }

    public function index()
    {
        $data['teks']        = 'E-Absen';
        $data['title']       = 'Absen PNS';
        $data['description'] = 'Halaman Absen Guru PNS';

        $this->load->library('googlemaps_ses');
        $config=array();
        $config['center']="-7.674266, 110.615246";
        $config['zoom']=17;
        $config['map_height']="500px";
        $this->googlemaps_ses->initialize($config);
        $marker=array();
        $marker['position']="-7.674266, 110.615246";
        $this->googlemaps_ses->add_marker($marker);
        $data['map']=$this->googlemaps_ses->create_map();

        $data['lat'] = '-7.674266';
        $data['long'] = '110.615246';

        $data['page']        = 'pns/test';
        $data['file_js']     = 'pns/test';
        $this->load->view('auth/index', $data);
    }

    function show_location(){
        $latitude   = $_POST['latitude'];
        $longitude  = $_POST['longitude'];

        if (!empty($latitude) && !empty($longitude)) {

            $gmap = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($latitude).','.$longitude.'&sensor=false';
        // curl
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $gmap);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_PROXYPORT,3128);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,0);
            $response = curl_exec($ch);
            curl_close($ch);
        // end curl
            $data = json_decode($response);

            if ($response) {
                echo json_encode($data->results[0]->formatted_address);
            }else{
                echo json_encode(false);
            }
        }
    }

    public function save()
    {
        $username = $this->input->post('username', true);
        $email = $this->input->post('email', true);
        $password = $this->input->post('password', true);
        $image = $this->input->post('image');
        $image = str_replace('data:image/jpeg;base64,','', $image);
        $image = base64_decode($image);
        $filename = 'image_'.time().'.png';
        file_put_contents(FCPATH.'/file/'.$filename,$image);
        $data = array(
            'username' => $username,
            'email' => $email,
            'password' => password_hash($password, PASSWORD_DEFAULT),
            'image' => $filename,
        );

        $res = $this->main_model->Insert_where('user', $data);
        echo json_encode($res);
    }
}