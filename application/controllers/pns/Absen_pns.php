<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Absen_pns extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library(['akses']);
        $this->akses->cek_login();
    }

    public function index()
    {
        $data['teks']        = 'E-Absen';
        $data['title']       = 'Absen';
        $data['description'] = 'Halaman Absen Guru';

        // $this->load->library('googlemaps_ses');
        // $config=array();
        // $config['center']="-7.674266, 110.615246";
        // $config['zoom']=17;
        // $config['map_height']="500px";
        // $this->googlemaps_ses->initialize($config);
        // $marker=array();
        // $marker['position']="-7.674266, 110.615246";
        // $this->googlemaps_ses->add_marker($marker);
        // $data['map']=$this->googlemaps_ses->create_map();

        // $data['lat'] = '-7.674266';
        // $data['long'] = '110.615246';

        $data['page']        = 'pns/absen_pns';
        $data['file_js']     = 'pns/absen_pns';
        $this->load->view('auth/index', $data);
    }

    function show_location(){
        $latitude   = $_POST['latitude'];
        $longitude  = $_POST['longitude'];

        if (!empty($latitude) && !empty($longitude)) {

            $gmap = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($latitude).','.$longitude.'&sensor=false';
        // curl
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $gmap);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_PROXYPORT,3128);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,0);
            $response = curl_exec($ch);
            curl_close($ch);
        // end curl
            $data = json_decode($response);

            if ($response) {
                echo json_encode($data->results[0]->formatted_address);
            }else{
                echo json_encode(false);
            }
        }
    }

    function get_location(){
        $lokasi = $this->db->query("SELECT * FROM lokasi WHERE kategori='pns' and deleted='0'")->result();
        echo json_encode([
            'lokasi'  => $lokasi
        ]);
    }

    public function save()
    {
        $image = $this->input->post('image');
        $image = str_replace('data:image/jpeg;base64,','', $image);
        $image = base64_decode($image);
        $filename = 'image_'.time().'.png';
        file_put_contents(FCPATH.'/file/pns/'.$filename,$image);
        $data = array(
            'id_pns'    => $this->session->userdata('id'),
            'username'  => $this->session->userdata('username'),
            'latitude'  => $this->input->post('lat_saya'),
            'longitude' => $this->input->post('long_saya'),
            'radius'    => $this->input->post('lat_radius'),
            'nama_tempat' => $this->input->post('nama_tempat'),
            'waktu'     => date_create('now', timezone_open('Asia/Jakarta'))->format('Y-m-d H:i:s'),
            'tanggal'   => date_create('now', timezone_open('Asia/Jakarta'))->format('Y-m-d'),
            'jam'       => date_create('now', timezone_open('Asia/Jakarta'))->format('H:i:s'),
            'image'     => $filename,
        );

        $res = $this->main_model->Insert_where('absen_pns', $data);
        if ($res > 0) {
            echo json_encode([
                'status' => 200,
                'keterangan' => 'Berhasil simpan data',
            ]);
        } else {
            echo json_encode([
                'status' => 500,
                'keterangan' => 'Gagal simpan data',
            ]);
        }
    }
}