<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Absen_history extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library(['akses']);
        $this->akses->cek_login();
    }

    public function index()
    {
        $data['teks']        = 'E-Absen';
        $data['title']       = 'Riwayat Absen';
        $data['description'] = 'Halaman Riwayat Absen Guru';

        $data['page']        = 'pns/absen_history';
        $data['file_js']     = 'pns/absen_history';
        $this->load->view('auth/index', $data);
    }

    public function datatables()
    {
        $username = $this->session->userdata('username');
        $get = $this->main_model->get_data_absen();
        $data = array();
        $no = $_POST['start'];
        foreach ($get as $r) {
            $no++;
            $row = array();
            $folder = '../file/pns/'.$r->image;

            // $row[] = "<img src='$folder' class='avatar'  style='height: 40px;width:auto;' alt='Avatar'>";
            $row[] = $no;
            $row[] = $r->waktu;
            $row[] = $r->latitude.', '.$r->longitude;
            $row[] = $r->id;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->main_model->absen_count_all(),
            "recordsFiltered" => $this->main_model->absen_count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    function get_data(){
        $id = $this->input->post('id');

        $data = $this->db->query("
            SELECT 
                absen_pns.*,
                user.nip,
                user.nama
            FROM 
                absen_pns 
            LEFT JOIN 
                user
            ON 
                absen_pns.id_pns = user.id
            WHERE 
                absen_pns.id='$id'
        ")->row();

        echo json_encode([ 'data' => $data ]);
    }
}