<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Set_profil extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library(['akses']);
        $this->akses->cek_login();
    }

    public function index()
    {
        $data['teks']        = 'E-Absen';
        $data['title']       = 'Setting Data Sekolahan';
        $data['description'] = 'Halaman Setting Data Sekolahan';

        $data['page']        = 'operator/set_profil';
        $data['file_js']     = 'operator/set_profil';
        $this->load->view('auth/index', $data);
    }

    function ambil_guru(){
        $q = $this->input->post('q');

        $guru = $this->db->query("SELECT * FROM user WHERE nama LIKE '%$q%' AND deleted='0'")->result(); 
        echo json_encode([ 'guru' => $guru ]);
    }

    function ambil_data(){
        $data = $this->db->query("SELECT * FROM profil WHERE id='1'")->row();
        $kepala = $this->db->query("SELECT * FROM user WHERE id='$data->kepala'")->row();
        $wakil = $this->db->query("SELECT * FROM user WHERE id='$data->wakil'")->row();
        $kesiswaan = $this->db->query("SELECT * FROM user WHERE id='$data->kesiswaan'")->row();
        echo json_encode([ 'data' => $data, 'kepala' => $kepala, 'wakil' => $wakil, 'kesiswaan' => $kesiswaan ]);
    }

    function simpan(){
        $id = $this->input->post('id');

        if ($_FILES["image_prov"]['name']) {
            $image_prov = $this->_uploadImage1();
        }else{
            $image_prov = $this->db->query("SELECT * FROM profil")->row('image_prov');
        }

        if ($_FILES["image_sekolah"]['name']) {
            $image_sekolah = $this->_uploadImage2();
        }else{
            $image_sekolah = $this->db->query("SELECT * FROM profil")->row('image_sekolah');
        }

        $data = array(
            'sekolah'       => $this->input->post('sekolah'),
            'kepala'        => $this->input->post('kepala'),
            'wakil'         => $this->input->post('wakil'),
            'kesiswaan'     => $this->input->post('kesiswaan'),
            'alamat'        => $this->input->post('alamat'),
            'kode_pos'      => $this->input->post('kode_pos'),
            'telepon'       => $this->input->post('telepon'),
            'npsn'          => $this->input->post('npsn'),
            'thn_ajaran'    => $this->input->post('thn_ajaran'),
            'image_prov'    => $image_prov,
            'image_sekolah' => $image_sekolah
            );
        $dat = ['id' => $id];
        $save = $this->main_model->Update_where('profil', $data, $dat);

        if ($save) {
            echo json_encode([
                'status' => 200,
                'keterangan' => 'Berhasil menyimpan data',
            ]);
        } else {
            echo json_encode([
                'status' => 500,
                'keterangan' => 'Gagal menyimpan data',
            ]);
        }
    }

    private function _uploadImage1()
    {
        $config['upload_path'] = './file/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['file_name'] = 'image_prov';
        $config['overwrite'] = true;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('image_prov')) {
            $gbr = $this->upload->data();
            //Compress Image
            $config['image_library'] = 'gd2';
            $config['source_image'] = './file/' . $gbr['file_name'];
            $config['create_thumb'] = false;
            $config['maintain_ratio'] = false;
            $config['quality'] = '100%';
            $config['width'] = 200;
            $config['height'] = 150;
            $config['new_image'] = './file/' . $gbr['file_name'];
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            return $this->upload->data("file_name");
        }
    }

    private function _uploadImage2()
    {
        $config['upload_path'] = './file/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['file_name'] = 'image_sekolah';
        $config['overwrite'] = true;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('image_sekolah')) {
            $gbr = $this->upload->data();
            //Compress Image
            $config['image_library'] = 'gd2';
            $config['source_image'] = './file/' . $gbr['file_name'];
            $config['create_thumb'] = false;
            $config['maintain_ratio'] = false;
            $config['quality'] = '100%';
            $config['width'] = 200;
            $config['height'] = 150;
            $config['new_image'] = './file/' . $gbr['file_name'];
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            return $this->upload->data("file_name");
        }
    }
}