<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_siswa extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library(['akses']);
        $this->akses->cek_login();
    }

    public function index()
    {
        $data['teks']        = 'E-Absen';
        $data['title']       = 'Data Siswa';
        $data['description'] = 'Halaman Data Siswa';

        $data['page']        = 'operator/master_siswa';
        $data['file_js']     = 'operator/master_siswa';
        $this->load->view('auth/index', $data);
    }

    function datatables()
    {
        $username = $this->session->userdata('username');
        $get = $this->main_model->get_data_siswa();
        $data = array();
        $no = $_POST['start'];
        foreach ($get as $r) {
            $kelas = $this->db->query("SELECT kelas FROM master_kelas WHERE id='$r->kelas'")->row('kelas');
            $no++;
            $row = array();
            $edit    = "edit_data('$r->id')";
            $hapus   = "hapus_data('$r->id')";

            $row[] = $no;
            $row[] = $r->username;
            $row[] = $r->nisn;
            $row[] = $r->nama;
            $row[] = $r->jenis_kelamin;
            $row[] = $r->alamat;
            $row[] = $r->no_telp;
            $row[] = $kelas;
            $row[] = '
                <button class="btn-xs btn-warning" onclick="'.$edit.'" style="margin-bottom:0px; margin-right:0px;"><i class="fa fa-edit"></i ></button>
                <button class="btn-xs btn-danger" onclick="'.$hapus.'" style="margin-bottom:0px; margin-right:0px;"><i class="fa fa-trash-o"></i ></button>
            ';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->main_model->siswa_count_all(),
            "recordsFiltered" => $this->main_model->siswa_count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    function ambil_kelas(){
        $q = $this->input->post('q');

        $kelas = $this->db->query("SELECT * FROM master_kelas WHERE kelas LIKE '%$q%' AND deleted='0'")->result(); 
        echo json_encode([ 'kelas' => $kelas ]);
    }

    function get_data(){
        $id = $this->input->post('id');

        $data = $this->db->query("SELECT * FROM siswa WHERE id='$id'")->row();

        echo json_encode([ 'data' => $data ]);
    }

    function simpan(){
        $id = $this->input->post('id');

        $data = array(
            'username'      => $this->input->post('username'),
            'password'      => $this->input->post('password'),
            'nama'          => $this->input->post('nama'),
            'nisn'          => $this->input->post('nisn'),
            'jenis_kelamin' => $this->input->post('jenis_kelamin'),
            'no_telp'       => $this->input->post('no_telp'),
            'kelas'         => $this->input->post('kelas'),
            'alamat'        => $this->input->post('alamat'),
            'created'       => $this->session->userdata('id'),
            'date_created'  => date_create('now', timezone_open('Asia/Jakarta'))->format('Y-m-d H:i:s')
        );

        if(empty($id)){
            $save = $this->main_model->Insert_where('siswa', $data);
        }else{
            $dat = ['id' => $id];
            $save = $this->main_model->Update_where('siswa', $data, $dat);
        }

        if ($save) {
            echo json_encode([
                'status' => 200,
                'keterangan' => 'Berhasil menyimpan data',
            ]);
        } else {
            echo json_encode([
                'status' => 500,
                'keterangan' => 'Gagal menyimpan data',
            ]);
        }
    }

    function hapus(){             
        $id = $this->input->post('id'); 
        $updt = ['id' => $id ];

        $data = array('deleted' => '1'); 
        $this->main_model->Update_where('siswa', $data, $updt); 

        echo json_encode([ 
            'status' => 200, 
            'keterangan'   => 'Berhasil menghapus data', 
        ]); 
    }

    public function import()
    {
        if ($_FILES["import_file"]['name']) {
            $path = $_FILES['import_file']['tmp_name'];
            $name = $_FILES['import_file']['name'];
            $ext = pathinfo($name, PATHINFO_EXTENSION);
            if($ext == 'xls' || $ext == 'xlsx'){
                $exec = $this->run_import($ext, $path);
                echo json_encode($exec);
            }else{
                echo json_encode([
                    'status' => false,
                    'ket' => 'format file tidak didukung'
                ]);
            }
        }else{
            echo json_encode([
                'status' => false,
                'ket' => 'file tidak terkirim'
            ]);
        }
    }

    private function run_import($ext, $path){
        // format excel 2007 ke bawah
        if($ext == 'xls'){
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        // format excel 2010 ke atas
        } else {
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        }
        
        $spreadsheet = $reader->load($path);
        $data = $spreadsheet->getSheet(0)->toArray();
        
        $isData = false;
        foreach($data as $index => $row){
            
            if($row[0] == 'stop' || $row[3] == 'TOTAL'){
                break;
            }

            if($row[0] == 1){
                $isData = true;
                $this->db->query("TRUNCATE siswa_import");
            }

            if($isData){    
                $data = [
                    "username"      => $row[1],
                    "password"      => $row[2],
                    "nisn"          => $row[3],
                    "nama"          => $row[4],
                    "jenis_kelamin" => $row[5],
                    "alamat"        => $row[6],
                    "no_telp"       => $row[7],
                    "kelas"         => $this->input->post('import_kelas'),
                ];
    
                $this->main_model->Insert_where('siswa_import', $data);
            }
        }
        if($isData){
            return ['status' => true, 'ket' => 'Berhasil'];
        }else{
            return ['status' => false, 'ket' => 'Data tidak terbaca'];
        }
    }

    public function get_excel()
    {
        $data = $this->db->query("select siswa_import.*, master_kelas.kelas from siswa_import left join master_kelas on master_kelas.id = siswa_import.kelas")->result();
        echo json_encode([
            'data' => $data
        ]);
    }

    public function update_excel()
    {
        $data = $this->db->query("SELECT * FROM siswa_import WHERE deleted='0'")->result();
        foreach ($data as $key) {
            $siswa[] = array(
                "username"      => $key->username,
                "password"      => $key->password,
                "nisn"          => $key->nisn,
                "nama"          => $key->nama,
                "jenis_kelamin" => $key->jenis_kelamin,
                "alamat"        => $key->alamat,
                "no_telp"       => $key->no_telp,
                "kelas"         => $key->kelas,
                "akses"         => 'SISWA',
                'created'       => $this->session->userdata('id'),
                'date_created'  => date_create('now', timezone_open('Asia/Jakarta'))->format('Y-m-d H:i:s')
            );
        }
        $save = $this->db->insert_batch('siswa', $siswa);

        if ($save) {
            echo json_encode([
                'status' => 200,
                'keterangan' => 'Berhasil menyimpan data',
            ]);
        } else {
            echo json_encode([
                'status' => 500,
                'keterangan' => 'Gagal menyimpan data',
            ]);
        }
    }

    public function destroy_excel()
    {
        $this->db->query("TRUNCATE siswa_import");
        echo json_encode([
            'status' => true
        ]);
    }
}