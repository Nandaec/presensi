<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_kelas extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library(['akses']);
        $this->akses->cek_login();
    }

    public function index()
    {
        $data['teks']        = 'E-Absen';
        $data['title']       = 'Data Master Kelas';
        $data['description'] = 'Halaman Data Master Kelas';

        $data['page']        = 'operator/master_kelas';
        $data['file_js']     = 'operator/master_kelas';
        $this->load->view('auth/index', $data);
    }

    function datatables()
    {
        $username = $this->session->userdata('username');
        $get = $this->main_model->get_data_kelas();
        $data = array();
        $no = $_POST['start'];
        foreach ($get as $r) {
            $no++;
            $row = array();
            $edit    = "edit_data('$r->id')";
            $hapus   = "hapus_data('$r->kelas')";

            $row[] = $no;
            $row[] = $r->kelas;
            $row[] = $this->db->query("SELECT * FROM user WHERE id='$r->wali'")->row('nama');
            $row[] = '
                <button class="btn-xs btn-warning" onclick="'.$edit.'" style="margin-bottom:0px; margin-right:0px;"><i class="fa fa-edit"></i ></button>
                <button class="btn-xs btn-danger" onclick="'.$hapus.'" style="margin-bottom:0px; margin-right:0px;"><i class="fa fa-trash-o"></i ></button>
            ';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->main_model->kelas_count_all(),
            "recordsFiltered" => $this->main_model->kelas_count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    function ambil_wali(){
        $q = $this->input->post('q');

        $wali = $this->db->query("SELECT * FROM user WHERE nama LIKE '%$q%' AND deleted='0'")->result(); 
        echo json_encode([ 'wali' => $wali ]);
    }

    function get_data(){
        $id = $this->input->post('id');

        $data = $this->db->query("SELECT * FROM master_kelas WHERE id='$id'")->row();

        echo json_encode([ 'data' => $data ]);
    }

    function simpan(){
        $id = $this->input->post('id');

        $data = array(
            'kelas' => $this->input->post('kelas'),
            'wali' => $this->input->post('wali'),
        );

        if(empty($id)){
            $save = $this->main_model->Insert_where('master_kelas', $data);
        }else{
            $dat = ['kelas' => $id];
            $save = $this->main_model->Update_where('master_kelas', $data, $dat);
        }

        if ($save) {
            echo json_encode([
                'status' => 200,
                'keterangan' => 'Berhasil menyimpan data',
            ]);
        } else {
            echo json_encode([
                'status' => 500,
                'keterangan' => 'Gagal menyimpan data',
            ]);
        }
    }

    function hapus(){             
        $id = $this->input->post('id'); 
        $updt = ['kelas' => $id ];

        $data = array('deleted' => '1'); 
        $this->main_model->Update_where('master_kelas', $data, $updt); 

        echo json_encode([ 
            'status' => 200, 
            'keterangan'   => 'Berhasil menghapus data', 
        ]); 
    }
}