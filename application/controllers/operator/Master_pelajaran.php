<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_pelajaran extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library(['akses']);
        $this->akses->cek_login();
    }

    public function index()
    {
        $data['teks']        = 'E-Absen';
        $data['title']       = 'Data Master Pelajaran';
        $data['description'] = 'Halaman Data Master Pelajaran';

        $data['page']        = 'operator/master_pelajaran';
        $data['file_js']     = 'operator/master_pelajaran';
        $this->load->view('auth/index', $data);
    }

    function datatables()
    {
        $username = $this->session->userdata('username');
        $get = $this->main_model->get_data_pelajaran();
        $data = array();
        $no = $_POST['start'];
        foreach ($get as $r) {
            $no++;
            $row = array();
            $edit    = "edit_data('$r->id')";
            $hapus   = "hapus_data('$r->pelajaran')";

            $row[] = $no;
            $row[] = $r->pelajaran;
            $row[] = '
                <button class="btn-xs btn-warning" onclick="'.$edit.'" style="margin-bottom:0px; margin-right:0px;"><i class="fa fa-edit"></i ></button>
                <button class="btn-xs btn-danger" onclick="'.$hapus.'" style="margin-bottom:0px; margin-right:0px;"><i class="fa fa-trash-o"></i ></button>
            ';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->main_model->pelajaran_count_all(),
            "recordsFiltered" => $this->main_model->pelajaran_count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    function get_data(){
        $id = $this->input->post('id');

        $data = $this->db->query("SELECT * FROM master_pelajaran WHERE id='$id'")->row();

        echo json_encode([ 'data' => $data ]);
    }

    function simpan(){
        $id = $this->input->post('id');

        $data = array(
            'pelajaran' => $this->input->post('pelajaran'),
        );

        if(empty($id)){
            $save = $this->main_model->Insert_where('master_pelajaran', $data);
        }else{
            $dat = ['pelajaran' => $id];
            $save = $this->main_model->Update_where('master_pelajaran', $data, $dat);

            $save = $this->main_model->Update_where('user', $data, $dat);
        }

        if ($save) {
            echo json_encode([
                'status' => 200,
                'keterangan' => 'Berhasil menyimpan data',
            ]);
        } else {
            echo json_encode([
                'status' => 500,
                'keterangan' => 'Gagal menyimpan data',
            ]);
        }
    }

    function hapus(){             
        $id = $this->input->post('id'); 
        $updt = ['pelajaran' => $id ];

        $data = array('deleted' => '1'); 
        $this->main_model->Update_where('master_pelajaran', $data, $updt); 

        $data = array( 'pelajaran' => '' );
        $this->main_model->Update_where('user', $data, $updt); 

        echo json_encode([ 
            'status' => 200, 
            'keterangan'   => 'Berhasil menghapus data', 
        ]); 
    }
}