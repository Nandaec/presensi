<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Presensi_siswa extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library(['akses']);
        $this->akses->cek_login();
    }

    public function index()
    {
        $data['teks']        = 'E-Absen';
        $data['title']       = 'Data Presensi Hadir Siswa';
        $data['description'] = 'Halaman Data Presensi Hadir Siswa';

        $data['file_css']    = 'tabel.css';
        $data['page']        = 'operator/presensi_siswa';
        $data['file_js']     = 'operator/presensi_siswa';
        $this->load->view('auth/index', $data);
    }

    function datatables()
    {
        $username   = $this->session->userdata('username');
        $tanggal    = date('Y-m-d', strtotime($this->input->post('tanggal')));
        if($tanggal == '1970-01-01'){
            $tanggal = date('Y-m-d');
        }else{
            $tanggal = $this->input->post('tanggal');
        }

        $get = $this->main_model->get_rekap_siswa();
        $data = array();
        $no = $_POST['start'];
        foreach ($get as $r) {
            $kelas      = $r->id_kelas;
            $pelajaran  = $r->id_pelajaran;
            $pengajar   = $r->pengajar;
            $hari       = $r->hari;
            $abs = $this->db->query("SELECT * FROM absen_siswa WHERE tanggal='$tanggal' AND kelas='$kelas' AND pelajaran='$pelajaran' AND deleted='0'")->row();
            $cek = $this->db->query("SELECT * FROM rekap_absen_siswa WHERE tanggal='$tanggal' AND kelas='$kelas' AND pelajaran='$pelajaran' AND pengajar='$pengajar' AND hari='$hari' AND deleted='0'")->row();
            if(!empty($abs)){
                if(!empty($cek)){
                    $status = '<button type="button" class="btn btn-success btn-xs">Dilihat</button>';
                }else{
                    $status = '<button type="button" class="btn btn-danger btn-xs">Belum</button>';
                }
            }else{
                $status = '<button type="button" class="btn btn-default btn-xs">Kosong</button>';
            }

            $no++;
            $row = array();
            $lihat = "lihat_data('$r->id','$tanggal','$kelas','$pelajaran','$pengajar','$hari','$r->kelas','$r->pelajaran')";

            $mulai = date('H:i', strtotime($r->jam_mulai));
            $akhir = date('H:i', strtotime($r->jam_akhir));

            $row[] = $no;
            $row[] = $r->kelas;
            $row[] = $r->pelajaran;
            $row[] = $r->nama;
            $row[] = $r->hari;
            $row[] = $mulai.'-'.$akhir;
            $row[] = $status;
            $row[] = '
                <button class="btn-xs btn-primary" onclick="'.$lihat.'" style="margin-bottom:0px;"><i class="fa fa-eye"></i ></button>
            ';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->main_model->rekap_siswa_count_all(),
            "recordsFiltered" => $this->main_model->rekap_siswa_count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    function ambil_hari(){
        $tanggal = date('D', strtotime($this->input->post('tanggal')));

        if($tanggal == 'Mon'){
            $hari = 'Senin';
        }else if($tanggal == 'Tue'){
            $hari = 'Selasa';
        }else if($tanggal == 'Wed'){
            $hari = 'Rabu';
        }else if($tanggal == 'Thu'){
            $hari = 'Kamis';
        }else if($tanggal == 'Fri'){
            $hari = 'Jumat';
        }else if($tanggal == 'Sat'){
            $hari = 'Sabtu';
        }else{
            $hari = 'Minggu';
        }

        echo json_encode([ 'data' => $hari ]);
    }

    function ambil_kelas(){
        $q = $this->input->post('q');

        $kelas = $this->db->query("SELECT * FROM master_kelas WHERE kelas LIKE '%$q%' AND deleted='0'")->result(); 
        echo json_encode([ 'kelas' => $kelas ]);
    }

    function ambil_pelajaran(){
        $q = $this->input->post('q');

        $pelajaran = $this->db->query("SELECT * FROM master_pelajaran WHERE pelajaran LIKE '%$q%' AND deleted='0'")->result(); 
        echo json_encode([ 'pelajaran' => $pelajaran ]);
    }

    function ambil_pengajar(){
        $q = $this->input->post('q');

        $pengajar = $this->db->query("SELECT * FROM user WHERE nama LIKE '%$q%' AND deleted='0'")->result(); 
        echo json_encode([ 'pengajar' => $pengajar ]);
    }

    function get_data(){
        $id_jadwal  = $this->input->post('id_jadwal');
        $tanggal    = $this->input->post('tanggal');
        $kelas      = $this->input->post('kelas');
        $pelajaran  = $this->input->post('pelajaran');
        $pengajar   = $this->input->post('pengajar');
        $hari       = $this->input->post('hari');

        $absen = $this->db->query("
            SELECT 
                siswa.nisn,
                siswa.nama,
                siswa.jenis_kelamin,
                siswa.no_telp,
                absen_siswa.status
            FROM 
                siswa
            LEFT JOIN 
                absen_siswa
            ON 
                absen_siswa.id_siswa = siswa.id
            WHERE 
                absen_siswa.tanggal='$tanggal' AND
                absen_siswa.id_jadwal='$id_jadwal' AND
                absen_siswa.deleted='0'
            ORDER BY absen_siswa.id ASC
        ")->result();

        echo json_encode([ 'data' => $absen ]);
    }

    function simpan_tanda(){
        $tanggal    = $this->input->post('tanggal');
        $kelas      = $this->input->post('kelas');
        $pelajaran  = $this->input->post('pelajaran');
        $pengajar   = $this->input->post('pengajar');
        $hari       = $this->input->post('hari');

        $absen = $this->db->query("SELECT * FROM absen_siswa WHERE tanggal='$tanggal' AND kelas='$kelas' AND pelajaran='$pelajaran' AND deleted='0' ORDER BY id ASC")->row();
        $jad = $this->db->query("SELECT * FROM jadwal WHERE kelas='$kelas' AND pelajaran='$pelajaran' AND pengajar='$pengajar' AND hari='$hari' AND deleted='0' ORDER BY id ASC")->row();
        $cek = $this->db->query("SELECT * FROM rekap_absen_siswa WHERE id_jadwal='$jad->id' AND tanggal='$tanggal' AND deleted='0'")->row();

        if(!empty($absen)){
            $data = array(
                'id_jadwal' => $jad->id,
                'tanggal'   => $tanggal,
                'kelas'     => $kelas,
                'pelajaran' => $pelajaran,
                'pengajar'  => $pengajar,
                'hari'      => $hari,
                'jam_mulai' => $jad->jam_mulai,
                'jam_akhir' => $jad->jam_akhir,
                'jam'       => $jad->jam,
                'pengecek'  => $this->session->userdata('id'),
                'date_cek'  => date_create('now', timezone_open('Asia/Jakarta'))->format('Y-m-d H:i:s'),
            );
            if(empty($cek)){
                $save = $this->main_model->Insert_where('rekap_absen_siswa', $data);
            }else{
                $updt = ['id_jadwal' => $jad->id, 'tanggal' => $tanggal];
                $save = $this->main_model->Update_where('rekap_absen_siswa', $data, $updt);
            }

            if ($save > 0) {
                echo json_encode([
                    'status' => 200,
                    'keterangan' => 'Berhasil simpan data',
                ]);
            } else {
                echo json_encode([
                    'status' => 500,
                    'keterangan' => 'Gagal simpan data',
                ]);
            }
        }else{
            echo json_encode([
                'status' => 500,
                'keterangan' => 'Absen kelas tidak ada',
            ]);
        }
    }

    function cek_status_tanda(){
        $tanggal    = $this->input->post('tanggal');
        $kelas      = $this->input->post('kelas');
        $pelajaran  = $this->input->post('pelajaran');
        $pengajar   = $this->input->post('pengajar');
        $hari       = $this->input->post('hari');

        $cek = $this->db->query("SELECT * FROM rekap_absen_siswa WHERE tanggal='$tanggal' AND kelas='$kelas' AND pelajaran='$pelajaran' AND pengajar='$pengajar' AND deleted='0'")->row();
        if(!empty($cek)){
            $data = 'ada';
        }else{
            $data = 'kosong';
        }

        echo json_encode([ 'data' => $data ]);
    }

    function cetak($tanggal, $kelas, $hari){
        $tanggal   = date('Y-m-d', strtotime($tanggal));

        $siswa = $this->db->query("SELECT * FROM siswa WHERE kelas='$kelas' AND deleted='0'")->result();
        $data_absen = array();
        foreach ($siswa as $sis) {
            $absen = $this->db->query("SELECT * FROM absen_siswa WHERE id_siswa='$sis->id' AND tanggal='$tanggal' AND kelas='$kelas' AND deleted='0'")->result();
            foreach ($absen as $key) {
                if($key->status != 'H'){
                    $ra_mangkat = $key->status;
                }else{
                    $mangkat = $key->status;
                }
            }

            if(!empty($ra_mangkat)){
                $status = $this->select_status($ra_mangkat);
            }else{
                $status = $this->select_status($key->status);
            }

            $data_absen[] = array(
                'id_siswa'      => $sis->id,
                'nama'          => $sis->nama,
                'nisn'          => $sis->nisn,
                'jenis_kelamin' => $sis->jenis_kelamin,
                'satu'          => $this->get_role($sis->id, $tanggal, $kelas, $hari, '1'),
                'dua'           => $this->get_role($sis->id, $tanggal, $kelas, $hari, '2'),
                'tiga'          => $this->get_role($sis->id, $tanggal, $kelas, $hari, '3'),
                'empat'         => $this->get_role($sis->id, $tanggal, $kelas, $hari, '4'),
                'lima'          => $this->get_role($sis->id, $tanggal, $kelas, $hari, '5'),
                'enam'          => $this->get_role($sis->id, $tanggal, $kelas, $hari, '6'),
                'tujuh'         => $this->get_role($sis->id, $tanggal, $kelas, $hari, '7'),
                'delapan'       => $this->get_role($sis->id, $tanggal, $kelas, $hari, '8'),
                'sembilan'      => $this->get_role($sis->id, $tanggal, $kelas, $hari, '9'),
                'sepuluh'       => $this->get_role($sis->id, $tanggal, $kelas, $hari, '10'),
                'sebelas'       => $this->get_role($sis->id, $tanggal, $kelas, $hari, '11'),
                'keterangan'    => $status,
            );

            $ra_mangkat = '';
            $mangkat = '';
        }
        $data['data'] = $data_absen;
        $data['hari'] = $hari;
        $data['tanggal'] = $tanggal;
        $data['kelas'] = $this->db->query("SELECT * FROM master_kelas WHERE id='$kelas'")->row();
        $data['mengetahui'] = $this->db->query("SELECT * FROM user WHERE jabatan='Kepala Sekolah'")->row();
        $data['guru'] = $this->db->query("
            SELECT 
                user.nama 
            FROM 
                absen_mengajar 
            LEFT JOIN 
                user 
            ON 
                user.id=absen_mengajar.id_user 
            WHERE 
                absen_mengajar.tanggal='$tanggal' AND
                absen_mengajar.hari='$hari' AND
                absen_mengajar.kelas='$kelas' AND
                absen_mengajar.deleted='0'")->result();
        $data['profil'] = $this->db->query("SELECT * FROM profil WHERE id='1'")->row();
        $this->load->view('operator/cetak/pdf_presensi_siswa',$data);
    }

    function get_role($id_siswa, $tanggal, $kelas, $hari, $nomor){
        if($hari == 'Senin'){
            $hari = 'A';
        }else if($hari == 'Selasa' || $hari == 'Rabu' || $hari == 'Kamis' || $hari == 'Sabtu'){
            $hari = 'B';
        }else{
            $hari = 'C';
        }
        $cek = $this->db->query("SELECT * FROM jadwal_role WHERE hari='$hari' AND no='$nomor'")->row();
        if(!empty($cek->jam_mulai)){ $jam_mulai = $cek->jam_mulai; } else { $jam_mulai = '00:00:00'; }
        if(!empty($cek->jam_akhir)){ $jam_akhir = $cek->jam_akhir; } else { $jam_akhir = '00:00:00'; }

        $abs = $this->db->query("SELECT * FROM absen_siswa2 WHERE id_siswa='$id_siswa' AND tanggal='$tanggal' AND kelas='$kelas' AND jam_mulai='$jam_mulai' AND jam_akhir = '$jam_akhir' AND deleted='0'")->row();
        // $abs = $this->db->query("
        //     SELECT 
        //         * 
        //     FROM 
        //         absen_siswa2 
        //     WHERE 
        //         id_siswa='$id_siswa' AND
        //         tanggal='$tanggal' AND
        //         kelas='$kelas' AND
        //         (jam_mulai >= '$jam_mulai' AND jam_akhir <= '$jam_akhir') OR
        //         (jam_mulai >= '$jam_mulai' AND jam_akhir <= '$jam_mulai' AND jam_akhir >= '$jam_akhir') OR
        //         (jam_mulai <= '$jam_mulai' AND jam_mulai >= '$jam_akhir' AND jam_akhir <= '$jam_akhir') AND
        //         deleted='0'
        //     ")->row();
        if(empty($abs)){
            $data = '';
        }else{
            $data = $abs->status;
        }
        return $data;
    }

    function select_status($kode){
        if($kode == 'KL'){
            $status = 'Kultum';
        }else if($kode == 'UP'){
            $status = 'Upacara';
        }else if($kode == 'AP'){
            $status = 'Apel';
        }else if($kode == 'X'){
            $status = 'Tidak hadir';
        }else if($kode == 'L'){
            $status = '9';
        }else if($kode == 'P'){
            $status = '10';
        }else if($kode == 'J'){
            $status = '19';
        }else if($kode == 'H'){
            $status = 'Hadir';
        }else if($kode == 'A'){
            $status = 'Alfa';
        }else if($kode == 'S'){
            $status = 'Sakit';
        }else{
            $status = '';
        }

        return $status;
    }

    // function cetak($tanggal, $kelas, $hari){
    //     $data['tanggal']    = $tanggal;
    //     $data['kelas']      = $kelas;
    //     $data['hari']       = $hari;

    //     $abs = $this->db->query("
    //         SELECT
    //             absen_siswa.id_siswa,
    //             absen_siswa.status,
    //             siswa.nama,
    //             siswa.nisn,
    //             siswa.jenis_kelamin,
    //             jadwal.hari,
    //             jadwal.jam_mulai,
    //             jadwal.jam_akhir
    //         FROM
    //             absen_siswa
    //         LEFT JOIN
    //             siswa
    //         ON 
    //             absen_siswa.id_siswa = siswa.id
    //         LEFT JOIN
    //             jadwal
    //         ON 
    //             absen_siswa.id_jadwal = jadwal.id
    //         WHERE
    //             absen_siswa.tanggal = '$tanggal' AND
    //             absen_siswa.kelas = '$kelas' AND
    //             absen_siswa.deleted = '0'
    //     ")->result();
    //     $absen = array();
    //     foreach ($abs as $key) {
    //         $jam_mulai  = $key->jam_mulai;
    //         $jam_akhir  = $key->jam_akhir;
    //         $status     = $key->status;

    //         $absen[] = array(
    //             'id_siswa'      => $key->id_siswa,
    //             'nama'          => $key->nama,
    //             'nisn'          => $key->nisn,
    //             'jenis_kelamin' => $key->jenis_kelamin,
    //             'hari'          => $key->hari,
    //             'satu'          => $this->get_role($hari, $jam_mulai, $jam_akhir, $status, '1'),
    //             'dua'           => $this->get_role($hari, $jam_mulai, $jam_akhir, $status, '2'),
    //             'tiga'          => $this->get_role($hari, $jam_mulai, $jam_akhir, $status, '3'),
    //             'empat'         => $this->get_role($hari, $jam_mulai, $jam_akhir, $status, '4'),
    //             'lima'          => $this->get_role($hari, $jam_mulai, $jam_akhir, $status, '5'),
    //             'enam'          => $this->get_role($hari, $jam_mulai, $jam_akhir, $status, '6'),
    //             'tujuh'         => $this->get_role($hari, $jam_mulai, $jam_akhir, $status, '7'),
    //             'delapan'       => $this->get_role($hari, $jam_mulai, $jam_akhir, $status, '8'),
    //             'sembilan'      => $this->get_role($hari, $jam_mulai, $jam_akhir, $status, '9'),
    //             'sepuluh'       => $this->get_role($hari, $jam_mulai, $jam_akhir, $status, '10'),
    //             'sebelas'       => $this->get_role($hari, $jam_mulai, $jam_akhir, $status, '11'),
    //         );
    //     }

    //     $data['data'] = $absen;
    //     $this->load->view('operator/cetak/pdf_presensi_siswa',$data);
    // }
}