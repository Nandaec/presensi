<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Presensi_pns extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library(['akses']);
        $this->akses->cek_login();
    }

    public function index()
    {
        $data['teks']        = 'E-Absen';
        $data['title']       = 'Data Presensi Hadir Guru';
        $data['description'] = 'Halaman Data Presensi Hadir Guru';

        $data['page']        = 'operator/presensi_pns';
        $data['file_js']     = 'operator/presensi_pns';
        $this->load->view('auth/index', $data);
    }

    function datatables()
    {
        $username = $this->session->userdata('username');
        $tanggal    = date('Y-m-d', strtotime($this->input->post('tanggal')));
        if($tanggal == '1970-01-01'){
            $tanggal = date('Y-m-d');
        }else{
            $tanggal = $this->input->post('tanggal');
        }

        $get = $this->main_model->get_absen_pns();
        $data = array();
        $no = $_POST['start'];
        foreach ($get as $r) {
            $abs = $this->db->query("SELECT * FROM absen_pns WHERE id_pns='$r->id' AND tanggal='$tanggal' AND deleted='0'")->row();
            if(!empty($abs)){
                $awal   = $this->db->query("SELECT * FROM absen_pns WHERE id_pns='$r->id' AND tanggal='$tanggal' ORDER BY id ASC")->row('jam');
                $akhir  = $this->db->query("SELECT * FROM absen_pns WHERE id_pns='$r->id' AND tanggal='$tanggal' ORDER BY id DESC")->row('jam');
                if($awal == $akhir){
                    $status = '<button type="button" class="btn btn-warning btn-xs">Hadir</button>';
                }else{
                    $status = '<button type="button" class="btn btn-success btn-xs">Hadir</button>';
                }
                
                $image          = $abs->image;
                $latitude       = $abs->latitude;
                $longitude      = $abs->longitude;
                $radius         = $abs->radius;
                $nama_tempat    = $abs->nama_tempat;
                $waktu          = $abs->waktu;
            }else{
                $image          = '';
                $latitude       = '';
                $longitude      = '';
                $radius         = '';
                $nama_tempat    = '';
                $waktu          = '';
                $status = '<button type="button" class="btn btn-danger btn-xs">Tidak Hadir</button>';
            }

            $no++;
            $row = array();
            $lihat = "lihat_data('$r->id','$tanggal')";

            $row[] = $no;
            $row[] = $r->nip;
            $row[] = $r->nama;            
            $row[] = $image;            
            $row[] = $latitude;            
            $row[] = $longitude;            
            $row[] = $radius;            
            $row[] = $nama_tempat;            
            $row[] = $waktu;     
            $row[] = $status;       
            $row[] = '
                <button class="btn-xs btn-primary" onclick="'.$lihat.'" style="margin-bottom:0px;"><i class="fa fa-eye"></i ></button>
            ';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->main_model->absen_pns_count_all(),
            "recordsFiltered" => $this->main_model->absen_pns_count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    function get_data(){
        $id = $this->input->post('id');
        $tanggal = $this->input->post('tanggal');

        $berangkat = $this->db->query("
            SELECT 
                absen_pns.*,
                user.nip,
                user.nama
            FROM 
                absen_pns 
            LEFT JOIN 
                user
            ON 
                absen_pns.id_pns = user.id
            WHERE 
                absen_pns.id_pns='$id' AND
                absen_pns.tanggal='$tanggal'
            ORDER BY absen_pns.id ASC
        ")->row();

        $pulang = $this->db->query("
            SELECT 
                absen_pns.*,
                user.nip,
                user.nama
            FROM 
                absen_pns 
            LEFT JOIN 
                user
            ON 
                absen_pns.id_pns = user.id
            WHERE 
                absen_pns.id_pns='$id' AND
                absen_pns.tanggal='$tanggal'
            ORDER BY absen_pns.id DESC
        ")->row();

        echo json_encode([ 'berangkat' => $berangkat, 'pulang' => $pulang ]);
    }

    function cetak($tanggal){
        $tanggal   = date('Y-m-d', strtotime($tanggal));

        $pns = $this->db->query("SELECT * FROM user WHERE (akses='PNS' OR akses='GURU' OR akses='TENDIK') AND deleted='0'")->result();
        $data_absen = array();
        foreach ($pns as $sis) {
            $awal = $this->db->query("SELECT * FROM absen_pns WHERE id_pns='$sis->id' AND tanggal='$tanggal' AND deleted='0' ORDER BY id ASC")->row();
            $akhir = $this->db->query("SELECT * FROM absen_pns WHERE id_pns='$sis->id' AND tanggal='$tanggal' AND deleted='0' ORDER BY id DESC")->row();

            if(empty($awal->jam)){
                $jam_mulai = '';
            }else{
                $jam_mulai = date('H:i', strtotime($awal->jam));
            }

            if(empty($akhir->jam)){
                $jam_akhir = '';
            }else{
                $jam_akhir = date('H:i', strtotime($akhir->jam));
                if($jam_akhir == $jam_mulai){
                    $jam_akhir = date('H:i', strtotime('4 hours', strtotime($jam_mulai)));
                }else{
                    $jam_akhir = date('H:i', strtotime($akhir->jam));
                }
            }

            if(!empty($jam_mulai && !empty($jam_akhir))){
                if($jam_akhir == '<font style="color:white;">'.$jam_mulai.'</font>'){
                    $hadir = 'B';
                    $tot_jam = '';
                }else{
                    $hadir = 'H';

                    $waw   = strtotime($jam_mulai);
                    $waa  = strtotime($jam_akhir);

                    $diff    = $waa - $waw;
                    $jam     = floor($diff / (60 * 60));
                    $menit   = $diff - $jam * (60 * 60);
                    $tot_jam = date('H', strtotime($jam.':'.floor($menit/60))).' JAM';
                }
            }else{
                $hadir = 'X';
                $tot_jam = '';
            }

            $data_absen[] = array(
                'nama'          => $sis->nama,
                'nip'           => $sis->nip,
                'jabatan'       => $sis->jabatan,
                'presensi'      => $jam_mulai.' - '.$jam_akhir,
                'info'          => $hadir,
                'keterangan'    => $tot_jam,
            );
        }

        $data['tanggal'] = $tanggal;
        $data['kepsek'] = $this->db->query("SELECT * FROM user WHERE jabatan='Kepala Sekolah' AND deleted='0'")->row();
        $data['wakil'] = $this->db->query("SELECT * FROM user WHERE jabatan='Wakakur' AND deleted='0'")->row();
        $data['data'] = $data_absen;
        $data['profil'] = $this->db->query("SELECT * FROM profil WHERE id='1'")->row();
        $this->load->view('operator/cetak/pdf_presensi_pns',$data);
    }
}