<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Set_jadwal extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library(['akses']);
        $this->akses->cek_login();
    }

    public function index()
    {
        $data['teks']        = 'E-Absen';
        $data['title']       = 'Setting Jadwal Pelajaran';
        $data['description'] = 'Halaman Setting Jadwal Pelajaran';

        $data['page']        = 'operator/set_jadwal';
        $data['file_js']     = 'operator/set_jadwal';
        $this->load->view('auth/index', $data);
    }

    function datatables()
    {
        $username = $this->session->userdata('username');
        $get = $this->main_model->get_jadwal();
        $data = array();
        $no = $_POST['start'];
        foreach ($get as $r) {
            $no++;
            $row = array();
            $edit    = "edit_data('$r->id')";
            $hapus   = "hapus_data('$r->id')";

            $mulai = date('H:i', strtotime($r->jam_mulai));
            $akhir = date('H:i', strtotime($r->jam_akhir));

            $row[] = $no;
            $row[] = $r->kelas;
            $row[] = $r->pelajaran;
            $row[] = $r->nama;
            $row[] = $r->hari;
            $row[] = $mulai.'-'.$akhir;
            $row[] = date('H:i', strtotime($r->jam))." (".$r->jumlah_jam.")";
            $row[] = '
                <button class="btn-xs btn-warning" onclick="'.$edit.'" style="margin-bottom:0px; margin-right:0px;"><i class="fa fa-edit"></i ></button>
                <button class="btn-xs btn-danger" onclick="'.$hapus.'" style="margin-bottom:0px; margin-right:0px;"><i class="fa fa-trash-o"></i ></button>
            ';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->main_model->jadwal_count_all(),
            "recordsFiltered" => $this->main_model->jadwal_count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    function ambil_kelas(){
        $q = $this->input->post('q');

        $kelas = $this->db->query("SELECT * FROM master_kelas WHERE kelas LIKE '%$q%' AND deleted='0'")->result(); 
        echo json_encode([ 'kelas' => $kelas ]);
    }

    function ambil_pelajaran(){
        $q = $this->input->post('q');

        $pelajaran = $this->db->query("SELECT * FROM master_pelajaran WHERE pelajaran LIKE '%$q%' AND deleted='0'")->result(); 
        echo json_encode([ 'pelajaran' => $pelajaran ]);
    }

    function ambil_pengajar(){
        $q = $this->input->post('q');

        $pengajar = $this->db->query("SELECT * FROM user WHERE nama LIKE '%$q%' AND deleted='0'")->result(); 
        echo json_encode([ 'pengajar' => $pengajar ]);
    }

    function get_data(){
        $id = $this->input->post('id');

        $data = $this->db->query("SELECT * FROM jadwal WHERE id='$id'")->row();

        echo json_encode([ 'data' => $data ]);
    }

    function simpan(){
        $id = $this->input->post('id');

        $jam_awal   = strtotime($this->input->post('jam_mulai'));
        $jam_akhir  = strtotime($this->input->post('jam_akhir'));

        $diff    = $jam_akhir - $jam_awal;
        $jam     = floor($diff / (60 * 60));
        $menit   = $diff - $jam * (60 * 60);
        $tot_jam = date('H:i:s', strtotime($jam.':'.floor($menit/60)));

        $data = array(
            'kelas'         => $this->input->post('kelas'),
            'pelajaran'     => $this->input->post('pelajaran'),
            'pengajar'      => $this->input->post('pengajar'),
            'hari'          => $this->input->post('hari'),
            'jam_mulai'     => $this->input->post('jam_mulai'),
            'jam_akhir'     => $this->input->post('jam_akhir'),
            'jam'           => $tot_jam,
            'jumlah_jam'    => $this->input->post('jumlah_jam'),
            'created'       => $this->session->userdata('id'),
            'date_created'  => date_create('now', timezone_open('Asia/Jakarta'))->format('Y-m-d H:i:s')
        );

        if(empty($id)){
            $save = $this->main_model->Insert_where('jadwal', $data);
        }else{
            $dat = ['id' => $id];
            $save = $this->main_model->Update_where('jadwal', $data, $dat);
        }

        if ($save) {
            echo json_encode([
                'status' => 200,
                'keterangan' => 'Berhasil menyimpan data',
            ]);
        } else {
            echo json_encode([
                'status' => 500,
                'keterangan' => 'Gagal menyimpan data',
            ]);
        }
    }

    function hapus(){             
        $id = $this->input->post('id'); 
        $updt = ['id' => $id ];

        $data = array(
            'deleted'       => '1',
            'edited'        => $this->session->userdata('id'),
            'date_edited'   => date_create('now', timezone_open('Asia/Jakarta'))->format('Y-m-d H:i:s')
        ); 
        $this->main_model->Update_where('jadwal', $data, $updt); 

        echo json_encode([ 
            'status' => 200, 
            'keterangan'   => 'Berhasil menghapus data', 
        ]); 
    }
}