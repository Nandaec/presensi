<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_jabatan extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library(['akses']);
        $this->akses->cek_login();
    }

    public function index()
    {
        $data['teks']        = 'E-Absen';
        $data['title']       = 'Data Master Jabatan';
        $data['description'] = 'Halaman Data Master Jabatan';

        $data['page']        = 'operator/master_jabatan';
        $data['file_js']     = 'operator/master_jabatan';
        $this->load->view('auth/index', $data);
    }

    function datatables()
    {
        $username = $this->session->userdata('username');
        $get = $this->main_model->get_data_jabatan();
        $data = array();
        $no = $_POST['start'];
        foreach ($get as $r) {
            $no++;
            $row = array();
            $edit    = "edit_data('$r->id')";
            $hapus   = "hapus_data('$r->jabatan')";

            $row[] = $no;
            $row[] = $r->jabatan;
            $row[] = '
                <button class="btn-xs btn-warning" onclick="'.$edit.'" style="margin-bottom:0px; margin-right:0px;"><i class="fa fa-edit"></i ></button>
                <button class="btn-xs btn-danger" onclick="'.$hapus.'" style="margin-bottom:0px; margin-right:0px;"><i class="fa fa-trash-o"></i ></button>
            ';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->main_model->jabatan_count_all(),
            "recordsFiltered" => $this->main_model->jabatan_count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    function get_data(){
        $id = $this->input->post('id');

        $data = $this->db->query("SELECT * FROM master_jabatan WHERE id='$id'")->row();

        echo json_encode([ 'data' => $data ]);
    }

    function simpan(){
        $id = $this->input->post('id');

        $data = array(
            'jabatan' => $this->input->post('jabatan'),
        );

        if(empty($id)){
            $save = $this->main_model->Insert_where('master_jabatan', $data);
        }else{
            $dat = ['jabatan' => $id];
            $save = $this->main_model->Update_where('master_jabatan', $data, $dat);

            $save = $this->main_model->Update_where('user', $data, $dat);
        }

        if ($save) {
            echo json_encode([
                'status' => 200,
                'keterangan' => 'Berhasil menyimpan data',
            ]);
        } else {
            echo json_encode([
                'status' => 500,
                'keterangan' => 'Gagal menyimpan data',
            ]);
        }
    }

    function hapus(){             
        $id = $this->input->post('id'); 
        $updt = ['jabatan' => $id ];

        $data = array('deleted' => '1'); 
        $this->main_model->Update_where('master_jabatan', $data, $updt); 

        $data = array( 'jabatan' => '' );
        $this->main_model->Update_where('user', $data, $updt); 

        echo json_encode([ 
            'status' => 200, 
            'keterangan'   => 'Berhasil menghapus data', 
        ]); 
    }
}