<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_user extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library(['akses']);
        $this->akses->cek_login();
    }

    public function index()
    {
        $data['teks']        = 'E-Absen';
        $data['title']       = 'Data User';
        $data['description'] = 'Halaman Data User';

        $data['page']        = 'operator/master_user';
        $data['file_js']     = 'operator/master_user';
        $this->load->view('auth/index', $data);
    }

    function datatables()
    {
        $username = $this->session->userdata('username');
        $get = $this->main_model->get_data_user();
        $data = array();
        $no = $_POST['start'];
        foreach ($get as $r) {
            $no++;
            $row = array();
            $edit    = "edit_data('$r->id')";
            $hapus   = "hapus_data('$r->id')";

            if($r->sts_absen == 'aktif'){
                $sts_absen = '<button class="btn btn-success btn-xs">Aktif</button>';
            }else if($r->sts_absen == 'ijin'){
                $sts_absen = '<button class="btn btn-warning btn-xs">Ijin</button>';
            }else if($r->sts_absen == 'cuti'){
                $sts_absen = '<button class="btn btn-warning btn-xs">Cuti</button>';
            }else{
                $sts_absen = '<button class="btn btn-warning btn-xs">Sakit</button>';
            }

            $row[] = $no;
            $row[] = $r->username;
            $row[] = $r->nama;
            $row[] = $r->nip;
            $row[] = $r->golongan;
            $row[] = $r->nuptk;
            $row[] = $r->pelajaran;
            $row[] = $r->jabatan;
            $row[] = $r->no_telp;
            $row[] = $sts_absen;
            $row[] = '
                <button class="btn-xs btn-warning" onclick="'.$edit.'" style="margin-bottom:0px; margin-right:0px;"><i class="fa fa-edit"></i ></button>
                <button class="btn-xs btn-danger" onclick="'.$hapus.'" style="margin-bottom:0px; margin-right:0px;"><i class="fa fa-trash-o"></i ></button>
            ';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->main_model->user_count_all(),
            "recordsFiltered" => $this->main_model->user_count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    function ambil_pelajaran(){
        $q = $this->input->post('q');

        $pelajaran = $this->db->query("SELECT * FROM master_pelajaran WHERE pelajaran LIKE '%$q%' AND deleted='0'")->result(); 
        echo json_encode([ 'pelajaran' => $pelajaran ]);
    }

    function ambil_jabatan(){
        $q = $this->input->post('q');

        $jabatan = $this->db->query("SELECT * FROM master_jabatan WHERE jabatan LIKE '%$q%' AND deleted='0'")->result(); 
        echo json_encode([ 'jabatan' => $jabatan ]);
    }

    function get_data(){
        $id = $this->input->post('id');

        $data = $this->db->query("SELECT * FROM user WHERE id='$id'")->row();

        echo json_encode([ 'data' => $data ]);
    }

    function simpan(){
        $id = $this->input->post('id');

        if ($_FILES["ttd"]['name']) {
            $ttd = $this->_uploadImage($this->input->post('username'));
        }else{
            $ttd = 'ttd_' . strtoupper($this->input->post('username').'.png');
        }

        if ($_FILES["foto"]['name']) {
            $foto = $this->_uploadFoto($this->input->post('username'));
        }else{
            $foto = 'foto_' . strtoupper($this->input->post('username').'.png');
        }

        $data = array(
            'username'      => $this->input->post('username'),
            'password'      => $this->input->post('password'),
            'nip'           => $this->input->post('nip'),
            'nama'          => $this->input->post('nama'),
            'no_telp'       => $this->input->post('no_telp'),
            'nuptk'         => $this->input->post('nuptk'),
            'golongan'      => $this->input->post('golongan'),
            'jabatan'       => $this->input->post('jabatan'),
            'pelajaran'     => $this->input->post('pelajaran'),
            'akses'         => $this->input->post('akses'),
            'sts_absen'     => $this->input->post('sts_absen'),
            'foto'          => $foto,
            'ttd'           => $ttd,
            'created'       => $this->session->userdata('id'),
            'date_created'  => date_create('now', timezone_open('Asia/Jakarta'))->format('Y-m-d H:i:s')
        );

        if(empty($id)){
            $save = $this->main_model->Insert_where('user', $data);
        }else{
            $dat = ['id' => $id];
            $save = $this->main_model->Update_where('user', $data, $dat);
        }

        if ($save) {
            echo json_encode([
                'status' => 200,
                'keterangan' => 'Berhasil menyimpan data',
            ]);
        } else {
            echo json_encode([
                'status' => 500,
                'keterangan' => 'Gagal menyimpan data',
            ]);
        }
    }

    private function _uploadFoto($username)
    {
        $config['upload_path'] = './file/foto/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['file_name'] = 'foto_' . strtoupper($username);
        $config['overwrite'] = true;
        // $config['max_size'] = 1024; // 1MB
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('foto')) {
            $gbr = $this->upload->data();
            //Compress Image
            $config['image_library'] = 'gd2';
            $config['source_image'] = './file/foto/' . $gbr['file_name'];
            $config['create_thumb'] = false;
            $config['maintain_ratio'] = false;
            $config['quality'] = '100%';
            $config['width'] = 200;
            $config['height'] = 200;
            $config['new_image'] = './file/foto/' . $gbr['file_name'];
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            return $this->upload->data("file_name");
        }
    }

    private function _uploadImage($username)
    {
        $config['upload_path'] = './file/ttd/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['file_name'] = 'ttd_' . strtoupper($username);
        $config['overwrite'] = true;
        // $config['max_size'] = 1024; // 1MB
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('ttd')) {
            $gbr = $this->upload->data();
            //Compress Image
            $config['image_library'] = 'gd2';
            $config['source_image'] = './file/ttd/' . $gbr['file_name'];
            $config['create_thumb'] = false;
            $config['maintain_ratio'] = false;
            $config['quality'] = '100%';
            $config['width'] = 200;
            $config['height'] = 150;
            $config['new_image'] = './file/ttd/' . $gbr['file_name'];
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            return $this->upload->data("file_name");
        }
    }

    function hapus(){             
        $id = $this->input->post('id'); 
        $updt = ['id' => $id ];

        $data = array('deleted' => '1'); 
        $this->main_model->Update_where('user', $data, $updt); 

        echo json_encode([ 
            'status' => 200, 
            'keterangan'   => 'Berhasil menghapus data', 
        ]); 
    }
}