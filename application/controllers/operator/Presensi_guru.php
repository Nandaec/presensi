<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Presensi_guru extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library(['akses']);
        $this->akses->cek_login();
    }

    public function index()
    {
        $data['teks']        = 'E-Absen';
        $data['title']       = 'Data Presensi Hadir Pengajar';
        $data['description'] = 'Halaman Data Presensi Hadir Pengajar';

        $data['page']        = 'operator/presensi_guru';
        $data['file_js']     = 'operator/presensi_guru';
        $this->load->view('auth/index', $data);
    }

    function datatables()
    {
        $username   = $this->session->userdata('username');
        $tanggal    = date('Y-m-d', strtotime($this->input->post('tanggal')));
        if($tanggal == '1970-01-01'){
            $tanggal = date('Y-m-d');
        }else{
            $tanggal = $tanggal;
        }

        $get = $this->main_model->get_rekap_guru();
        $data = array();
        $no = $_POST['start'];
        foreach ($get as $r) {
            $no++;
            $row = array();

            $g1 = "g1('$r->tanggal','$r->pengajar','$r->kelas','1','$r->satu')";
            $g2 = "g2('$r->tanggal','$r->pengajar','$r->kelas','2','$r->dua')";
            $g3 = "g3('$r->tanggal','$r->pengajar','$r->kelas','3','$r->tiga')";
            $g4 = "g4('$r->tanggal','$r->pengajar','$r->kelas','4','$r->empat')";
            $g5 = "g5('$r->tanggal','$r->pengajar','$r->kelas','5','$r->lima')";
            $g6 = "g6('$r->tanggal','$r->pengajar','$r->kelas','6','$r->enam')";
            $g7 = "g7('$r->tanggal','$r->pengajar','$r->kelas','7','$r->tujuh')";
            $g8 = "g8('$r->tanggal','$r->pengajar','$r->kelas','8','$r->delapan')";
            $g9 = "g9('$r->tanggal','$r->pengajar','$r->kelas','9','$r->sembilan')";
            $g10 = "g10('$r->tanggal','$r->pengajar','$r->kelas','10','$r->sepuluh')";
            $g11 = "g11('$r->tanggal','$r->pengajar','$r->kelas','11','$r->sebelas')";

            $row[] = $no;
            $row[] = $r->nama;
            $row[] = '
                <span class="btn btn-xs text-center ikon-hapus" onclick="'.$g1.'" style="margin-bottom:0px; margin-right:0px; margin-left:0px; margin-top:0px;">'.$r->satu.'</span>';
            $row[] = '
                <span class="btn btn-xs text-center ikon-hapus" onclick="'.$g2.'" style="margin-bottom:0px; margin-right:0px; margin-left:0px; margin-top:0px;">'.$r->dua.'</span>';
            $row[] = '
                <span class="btn btn-xs text-center ikon-hapus" onclick="'.$g3.'" style="margin-bottom:0px; margin-right:0px; margin-left:0px; margin-top:0px;">'.$r->tiga.'</span>';
            $row[] = '
                <span class="btn btn-xs text-center ikon-hapus" onclick="'.$g4.'" style="margin-bottom:0px; margin-right:0px; margin-left:0px; margin-top:0px;">'.$r->empat.'</span>';
            $row[] = '
                <span class="btn btn-xs text-center ikon-hapus" onclick="'.$g5.'" style="margin-bottom:0px; margin-right:0px; margin-left:0px; margin-top:0px;">'.$r->lima.'</span>';
            $row[] = '
                <span class="btn btn-xs text-center ikon-hapus" onclick="'.$g6.'" style="margin-bottom:0px; margin-right:0px; margin-left:0px; margin-top:0px;">'.$r->enam.'</span>';
            $row[] = '
                <span class="btn btn-xs text-center ikon-hapus" onclick="'.$g7.'" style="margin-bottom:0px; margin-right:0px; margin-left:0px; margin-top:0px;">'.$r->tujuh.'</span>';
            $row[] = '
                <span class="btn btn-xs text-center ikon-hapus" onclick="'.$g8.'" style="margin-bottom:0px; margin-right:0px; margin-left:0px; margin-top:0px;">'.$r->delapan.'</span>';
            $row[] = '
                <span class="btn btn-xs text-center ikon-hapus" onclick="'.$g9.'" style="margin-bottom:0px; margin-right:0px; margin-left:0px; margin-top:0px;">'.$r->sembilan.'</span>';
            $row[] = '
                <span class="btn btn-xs text-center ikon-hapus" onclick="'.$g10.'" style="margin-bottom:0px; margin-right:0px; margin-left:0px; margin-top:0px;">'.$r->sepuluh.'</span>';
            $row[] = '
                <span class="btn btn-xs text-center ikon-hapus" onclick="'.$g11.'" style="margin-bottom:0px; margin-right:0px; margin-left:0px; margin-top:0px;">'.$r->sebelas.'</span>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->main_model->rekap_guru_count_all(),
            "recordsFiltered" => $this->main_model->rekap_guru_count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    function ambil_hari(){
        $tanggal = date('D', strtotime($this->input->post('tanggal')));

        if($tanggal == 'Mon'){
            $hari = 'Senin';
        }else if($tanggal == 'Tue'){
            $hari = 'Selasa';
        }else if($tanggal == 'Wed'){
            $hari = 'Rabu';
        }else if($tanggal == 'Thu'){
            $hari = 'Kamis';
        }else if($tanggal == 'Fri'){
            $hari = 'Jumat';
        }else if($tanggal == 'Sat'){
            $hari = 'Sabtu';
        }else{
            $hari = 'Minggu';
        }

        echo json_encode([ 'data' => $hari ]);
    }

    function ambil_kelas(){
        $q = $this->input->post('q');

        $kelas = $this->db->query("SELECT * FROM master_kelas WHERE kelas LIKE '%$q%' AND deleted='0'")->result(); 
        echo json_encode([ 'kelas' => $kelas ]);
    }

    function ambil_pengajar(){
        $q = $this->input->post('q');

        $pengajar = $this->db->query("SELECT * FROM user WHERE nama LIKE '%$q%' AND deleted='0'")->result(); 
        echo json_encode([ 'pengajar' => $pengajar ]);
    }

    function simpan(){
        $no         = $this->input->post('jam');
        $pengajar   = $this->input->post('pengajar');
        $hari       = $this->input->post('hari');

        $jad = $this->db->query("SELECT * FROM jadwal WHERE pengajar='$pengajar' AND hari='$hari' AND deleted='0'")->row();
        if(!empty($jad)){
            $cek = $this->db->query("SELECT * FROM absen_mengajar2 WHERE pengajar='$pengajar' AND deleted='0'")->row();
            if($no == '1'){
                $data['satu'] = 'H';
            }else if($no == '2'){
                $data['dua'] = 'H';
            }else if($no == '3'){
                $data['tiga'] = 'H';
            }else if($no == '4'){
                $data['empat'] = 'H';
            }else if($no == '5'){
                $data['lima'] = 'H';
            }else if($no == '6'){
                $data['enam'] = 'H';
            }else if($no == '7'){
                $data['tujuh'] = 'H';
            }else if($no == '8'){
                $data['delapan'] = 'H';
            }else if($no == '9'){
                $data['sembilan'] = 'H';
            }else if($no == '10'){
                $data['sepuluh'] = 'H';
            }else if($no == '11'){
                $data['sebelas'] = 'H';
            }else{

            }

            $data['tanggal']      = date('Y-m-d', strtotime($this->input->post('tanggal')));
            $data['hari']         = $this->input->post('hari');
            $data['kelas']        = $this->input->post('kelas');
            $data['pengajar']     = $pengajar;
            $data['created']      = $this->session->userdata('id');
            $data['date_created'] = date_create('now', timezone_open('Asia/Jakarta'))->format('Y-m-d H:i:s');

            if(empty($cek)){
                $save = $this->main_model->Insert_where('absen_mengajar2', $data);
            }else{
                $dat = ['pengajar' => $pengajar];
                $save = $this->main_model->Update_where('absen_mengajar2', $data, $dat);
            }

            if ($save) {
                echo json_encode([
                    'status' => 200,
                    'keterangan' => 'Berhasil menyimpan data',
                ]);
            } else {
                echo json_encode([
                    'status' => 500,
                    'keterangan' => 'Gagal menyimpan data',
                ]);
            }
        }else{
            echo json_encode([
                'status' => 500,
                'keterangan' => 'Tidak ada jadwal hari ini',
                ]);
        }
    }

    function update(){
        $tanggal    = date('Y-m-d', strtotime($this->input->post('e_tanggal')));
        $pengajar   = $this->input->post('e_pengajar');
        $kelas      = $this->input->post('e_kelas');
        $jam        = $this->input->post('e_jam');
        $no         = $this->input->post('e_no_urut');

        if($no == '1'){
            $data['satu'] = 'H';
        }else if($no == '2'){
            $data['dua'] = 'H';
        }else if($no == '3'){
            $data['tiga'] = 'H';
        }else if($no == '4'){
            $data['empat'] = 'H';
        }else if($no == '5'){
            $data['lima'] = 'H';
        }else if($no == '6'){
            $data['enam'] = 'H';
        }else if($no == '7'){
            $data['tujuh'] = 'H';
        }else if($no == '8'){
            $data['delapan'] = 'H';
        }else if($no == '9'){
            $data['sembilan'] = 'H';
        }else if($no == '10'){
            $data['sepuluh'] = 'H';
        }else if($no == '11'){
            $data['sebelas'] = 'H';
        }else{

        }

        if($jam != $no){
            if($jam == '1'){
                $update['satu'] = '';
            }else if($jam == '2'){
                $update['dua'] = '';
            }else if($jam == '3'){
                $update['tiga'] = '';
            }else if($jam == '4'){
                $update['empat'] = '';
            }else if($jam == '5'){
                $update['lima'] = '';
            }else if($jam == '6'){
                $update['enam'] = '';
            }else if($jam == '7'){
                $update['tujuh'] = '';
            }else if($jam == '8'){
                $update['delapan'] = '';
            }else if($jam == '9'){
                $update['sembilan'] = '';
            }else if($jam == '10'){
                $update['sepuluh'] = '';
            }else if($jam == '11'){
                $update['sebelas'] = '';
            }else{

            }

            $dat = ['tanggal' => $tanggal, 'pengajar' => $pengajar];
            $save = $this->main_model->Update_where('absen_mengajar2', $update, $dat);
        }

        $dat = ['tanggal' => $tanggal, 'pengajar' => $pengajar];
        $save = $this->main_model->Update_where('absen_mengajar2', $data, $dat);

        if ($save) {
            echo json_encode([
                'status' => 200,
                'keterangan' => 'Berhasil menyimpan data',
            ]);
        } else {
            echo json_encode([
                'status' => 500,
                'keterangan' => 'Gagal menyimpan data',
            ]);
        }
    }

    function cetak($tanggal, $hari){
        $tanggal   = date('Y-m-d', strtotime($tanggal));

        $data['hari'] = $hari;
        $data['tanggal'] = $tanggal;
        $data['data'] = $this->db->query("
            SELECT 
                absen_mengajar2.*, 
                user.nama
            FROM 
                absen_mengajar2 
            LEFT JOIN 
                user 
            ON 
                user.id = absen_mengajar2.pengajar
            WHERE 
                absen_mengajar2.tanggal='$tanggal' AND 
                absen_mengajar2.deleted='0'
            ORDER BY id ASC
            ")->result();

        $data['profil'] = $this->db->query("SELECT * FROM profil WHERE id='1'")->row();
        $this->load->view('operator/cetak/pdf_presensi_guru',$data);
    }
}