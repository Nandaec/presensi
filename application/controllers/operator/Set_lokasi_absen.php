<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Set_lokasi_absen extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library(['akses']);
        $this->akses->cek_login();
    }

    public function index()
    {
        $data['teks']        = 'E-Absen';
        $data['title']       = 'Setting Titik Lokasi Absen';
        $data['description'] = 'Halaman Setting Titik Lokasi Absen';

        $data['page']        = 'operator/set_lokasi_absen';
        $data['file_js']     = 'operator/set_lokasi_absen';
        $this->load->view('auth/index', $data);
    }

    function get_location(){
        $lokasi = $this->db->query("SELECT * FROM lokasi WHERE kategori='pns' and deleted='0' ORDER BY id DESC")->result();
        echo json_encode([
            'lokasi'  => $lokasi
        ]);
    }

    public function save()
    {
        $id = $this->input->post('id_lokasi');
        $data = array(
            'latitude'      => $this->input->post('latitude'),
            'longitude'     => $this->input->post('longitude'),
            'radius'        => $this->input->post('radius'),
            'nama_tempat'   => $this->input->post('nama_tempat'),
            'keterangan'   => $this->input->post('keterangan'),
        );
        $dat = ['id' => $id];
        $save = $this->main_model->Update_where('lokasi', $data, $dat);

        if ($save > 0) {
            echo json_encode([
                'status' => 200,
                'keterangan' => 'Berhasil simpan data',
            ]);
        } else {
            echo json_encode([
                'status' => 500,
                'keterangan' => 'Gagal simpan data',
            ]);
        }
    }
}