<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Absen_mapel extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library(['akses']);
        $this->akses->cek_login();
    }

    public function index()
    {
        $data['teks']        = 'E-Absen';
        $data['title']       = 'Data Presensi Kehadiran Siswa';
        $data['description'] = 'Halaman Data Presensi Kehadiran Siswa';

        $data['file_css']    = 'tabel.css';
        $data['page']        = 'guru/absen_mapel';
        $data['file_js']     = 'guru/absen_mapel';
        $this->load->view('auth/index', $data);
    }

    function ambil_kelas(){
        $q = $this->input->post('q');

        $kelas = $this->db->query("SELECT * FROM master_kelas WHERE kelas LIKE '%$q%' AND deleted='0'")->result(); 
        echo json_encode([ 'kelas' => $kelas ]);
    }

    function ambil_pelajaran(){
        $q      = $this->input->post('q');
        $kelas  = $this->input->post('p');

        $data = $this->db->query("
            SELECT 
                master_pelajaran.id, 
                master_pelajaran.pelajaran 
            FROM 
                jadwal
            LEFT JOIN 
                master_pelajaran
            ON 
                jadwal.pelajaran = master_pelajaran.id
            WHERE
                master_pelajaran.pelajaran LIKE '%$q%' AND
                jadwal.kelas='$kelas' AND 
                jadwal.deleted='0'
            ORDER BY id DESC
        ")->result(); 
        echo json_encode([ 'data' => $data ]);
    }

    function ambil_pengajar(){
        $q      = $this->input->post('q');
        $pelajaran  = $this->input->post('p');
        $kelas  = $this->input->post('r');

        $data = $this->db->query("
            SELECT 
                user.id, 
                user.nama 
            FROM 
                jadwal
            LEFT JOIN 
                user
            ON 
                jadwal.pengajar = user.id
            WHERE
                user.nama LIKE '%$q%' AND
                jadwal.kelas='$kelas' AND 
                jadwal.pelajaran='$pelajaran' AND 
                jadwal.deleted='0'
            ORDER BY id DESC
        ")->result(); 
        echo json_encode([ 'data' => $data ]);
    }

    function ambil_data(){
        $tanggal    = date('Y-m-d', strtotime($this->input->post('tanggal')));
        $kelas      = $this->input->post('kelas');
        $pelajaran  = $this->input->post('pelajaran');
        $pengajar   = $this->input->post('pengajar');

        $siswa = $this->db->query("SELECT * FROM siswa WHERE kelas = '$kelas' AND deleted='0'")->result();

        $data = array();
        foreach ($siswa as $key) {
            $jad = $this->db->query("SELECT * FROM absen_siswa WHERE tanggal='$tanggal' AND kelas='$kelas' AND pelajaran='$pelajaran' AND deleted='0' ORDER BY id ASC")->row();
            $cek = $this->db->query("SELECT * FROM absen_siswa WHERE id_siswa='$key->id' AND tanggal='$tanggal' AND kelas='$kelas' AND pelajaran='$pelajaran' AND deleted='0' ORDER BY id ASC")->row();
            if(!empty($jad)){
                if(!empty($cek)){
                    $id     = $cek->id;

                    if($cek->status == NULL){
                        $hadir1 = 'H';
                        $hadir2 = 'Hadir';

                        $absen = array('status' => $hadir1);
                        $updt = ['id' => $id ];
                        $this->main_model->Update_where('absen_siswa', $absen, $updt);
                    }
                    if($cek->status != NULL){
                        $hadir1 = $cek->status;
                        $hadir2 = $this->select_status($hadir1);
                    }

                    $id_siswa   = $cek->id_siswa;
                    $username   = $cek->username;
                    $kelas      = $cek->kelas;
                    $pelajaran  = $cek->pelajaran;
                    $image      = $cek->image;
                    $latitude   = $cek->latitude;
                    $longitude  = $cek->longitude;
                    $waktu      = $cek->waktu;
                    $jam        = $cek->jam;
                }else{
                    $hadir1 = 'X';
                    $hadir2 = 'Tidak hadir';

                    $absen = array(
                        'id_siswa'  => $key->id,
                        'username'  => $key->username,
                        'kelas'     => $kelas,
                        'pelajaran' => $pelajaran,
                        'tanggal'   => $tanggal,
                        'status'    => $hadir1,
                    );
                    if(!empty($jad)){
                        $this->main_model->Insert_where('absen_siswa', $absen);
                    }
                    
                    $id_siswa   = '';
                    $username   = '';
                    $kelas      = '';
                    $pelajaran  = '';
                    $image      = '';
                    $latitude   = '';
                    $longitude  = '';
                    $waktu      = '';
                    $jam        = '';
                }
                $data[] = array(
                    'id_siswa'  => $id_siswa,
                    'username'  => $username,
                    'kelas'     => $kelas,
                    'pelajaran' => $pelajaran,
                    'image'     => $image,
                    'latitude'  => $latitude,
                    'longitude' => $longitude,
                    'waktu'     => $waktu,
                    'jam'       => $jam,

                    'nisn'      => $key->nisn,
                    'nama'      => $key->nama,
                    'hadir1'    => $hadir1,
                    'hadir2'    => $hadir2,
                    'waktu'     => $waktu
                );
            }else{
                $data = 'zonk';
            }
        }

        echo json_encode([ 'data' => $data ]);
    }

    function hit_data(){
        $tanggal    = date('Y-m-d', strtotime($this->input->post('tanggal')));
        $kelas      = $this->input->post('kelas');
        $pelajaran  = $this->input->post('pelajaran');
        $pengajar   = $this->input->post('pengajar');

        $siswa = $this->db->query("SELECT * FROM siswa WHERE kelas = '$kelas' AND deleted='0'")->num_rows();
        $absen = $this->db->query("SELECT * FROM absen_siswa WHERE tanggal='$tanggal' AND kelas='$kelas' AND pelajaran='$pelajaran' AND deleted='0'")->num_rows();
        echo json_encode([ 'siswa' => $siswa, 'absen' => $absen ]);
    }

    function select2(){
        $data = [
            ['select_id' => 'KL', 'select_nama' => 'Kultum'],
            ['select_id' => 'UP', 'select_nama' => 'Upacara'],
            ['select_id' => 'AP', 'select_nama' => 'Apel'],
            ['select_id' => 'X', 'select_nama' => 'Tidak hadir'],
            ['select_id' => 'L', 'select_nama' => '9'],
            ['select_id' => 'P', 'select_nama' => '10'],
            ['select_id' => 'J', 'select_nama' => '19'],
            ['select_id' => 'H', 'select_nama' => 'Hadir'],
            ['select_id' => 'A', 'select_nama' => 'Alfa'],
            ['select_id' => 'S', 'select_nama' => 'Sakit'],
        ];

        echo json_encode([ 'select' => $data ]);
    }

    function select_status($kode){
        if($kode == 'KL'){
            $status = 'Kultum';
        }else if($kode == 'UP'){
            $status = 'Upacara';
        }else if($kode == 'AP'){
            $status = 'Apel';
        }else if($kode == 'X'){
            $status = 'Tidak hadir';
        }else if($kode == 'L'){
            $status = '9';
        }else if($kode == 'P'){
            $status = '10';
        }else if($kode == 'J'){
            $status = '19';
        }else if($kode == 'H'){
            $status = 'Hadir';
        }else if($kode == 'A'){
            $status = 'Alfa';
        }else if($kode == 'S'){
            $status = 'Sakit';
        }else{
            $status = '';
        }

        return $status;
    }

    function cek_verif(){
        $tanggal    = date('Y-m-d', strtotime($this->input->post('tanggal')));
        $kelas      = $this->input->post('kelas');
        $pelajaran  = $this->input->post('pelajaran');
        $pengajar   = $this->input->post('pengajar');
        $user = $this->session->userdata('id');

        $absen = $this->db->query("SELECT verified FROM absen_siswa WHERE tanggal='$tanggal' AND kelas='$kelas' AND pelajaran='$pelajaran' AND deleted='0'")->row();
        if(!empty($absen)){
            if($absen->verified == NULL){
                if($pengajar == $user){
                    $data = 'sama';
                }else{
                    $data = 'tidak';
                }
                echo json_encode([ 'data' => $data ]);
            }else{
                $data = 'terverifikasi';
                echo json_encode([ 'data' => $data ]);
            }
        }else{
            $data = 'zong';
            echo json_encode([ 'data' => $data ]);
        }
    }

    function cek_status(){
        $tanggal    = date('Y-m-d', strtotime($this->input->post('tanggal')));
        $kelas      = $this->input->post('kelas');
        $pelajaran  = $this->input->post('pelajaran');
        $pengajar   = $this->input->post('pengajar');
    
        $absen = $this->db->query("SELECT verified FROM absen_siswa WHERE tanggal='$tanggal' AND kelas='$kelas' AND pelajaran='$pelajaran' AND deleted='0'")->row();
        $jad = $this->db->query("
            SELECT 
                jadwal.jam_mulai,
                jadwal.jam_akhir,
                master_pelajaran.pelajaran,
                master_kelas.kelas 
            FROM 
                jadwal 
            LEFT JOIN 
                master_kelas
            ON
                jadwal.kelas = master_kelas.id
            LEFT JOIN 
                master_pelajaran
            ON
                jadwal.pelajaran = master_pelajaran.id
            WHERE 
                jadwal.kelas='$kelas' AND
                jadwal.pelajaran='$pelajaran' AND
                jadwal.deleted='0'
            ")->row();
        $data = array();
        if(!empty($jad)){
            if($absen->verified != NULL){
                $data[] = array(
                    'kelas'     => $jad->kelas,
                    'pelajaran' => $jad->pelajaran,
                    'jam_mulai' => $jad->jam_mulai,
                    'jam_akhir' => $jad->jam_akhir,
                    'status'    => 'Terverifikasi',
                );
            }else{
                $data[] = array(
                    'kelas'     => $jad->kelas,
                    'pelajaran' => $jad->pelajaran,
                    'jam_mulai' => $jad->jam_mulai,
                    'jam_akhir' => $jad->jam_akhir,
                    'status'  => 'Belum Terverifikasi',
                );
            }
        }else{
            $data[] = array(
                'kelas'     => '',
                'pelajaran' => '',
                'jam_mulai' => '',
                'jam_akhir' => '',
                'status'  => 'Belum Terverifikasi',
            );
        }
        echo json_encode([ 'data' => $data ]);        
    }

    function simpan(){
        $tanggal    = date('Y-m-d', strtotime($this->input->post('tanggal')));
        $kelas      = $this->input->post('kelas');
        $pelajaran  = $this->input->post('pelajaran');
        $pengajar   = $this->input->post('pengajar');

        $jad = $this->db->query("SELECT * FROM absen_siswa WHERE tanggal='$tanggal' AND kelas='$kelas' AND pelajaran='$pelajaran' AND deleted='0' ORDER BY id ASC")->row();
        if(!empty($jad)){
            $absen = array(
                'verified'      => $this->session->userdata('id'),
                'date_verified' => date_create('now', timezone_open('Asia/Jakarta'))->format('Y-m-d H:i:s')
            );
            $updt = [
                'tanggal'   => $tanggal,
                'kelas'     => $kelas,
                'pelajaran' => $pelajaran,
                'deleted'   => '0'
            ];
            $save = $this->main_model->Update_where('absen_siswa', $absen, $updt);

            if ($save > 0) {
                $data = array(
                    'id_user'   => $this->session->userdata('id'),
                    'username'  => $this->session->userdata('username'),
                    'kelas'     => $this->input->post('kelas'),
                    'pelajaran' => $this->input->post('pelajaran'),
                    'tanggal'   => date_create('now', timezone_open('Asia/Jakarta'))->format('Y-m-d'),
                    'jam'       => date_create('now', timezone_open('Asia/Jakarta'))->format('H:i:s'),
                    'waktu'     => date_create('now', timezone_open('Asia/Jakarta'))->format('Y-m-d H:i:s')
                );
                $this->main_model->Insert_where('absen_mengajar', $data);

                echo json_encode([
                    'status' => 200,
                    'keterangan' => 'Berhasil simpan data',
                ]);
            } else {
                echo json_encode([
                    'status' => 500,
                    'keterangan' => 'Gagal simpan data',
                ]);
            }
        }else{
            echo json_encode([
                'status' => 500,
                'keterangan' => 'Absensi kelas belum dilakukan siswa',
            ]);
        }
    }
}