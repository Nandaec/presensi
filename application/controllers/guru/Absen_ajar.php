<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Absen_ajar extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library(['akses']);
        $this->akses->cek_login();
    }

    public function index()
    {
        $data['teks']        = 'E-Absen';
        $data['title']       = 'Data Presensi Kehadiran Siswa';
        $data['description'] = 'Halaman Data Presensi Kehadiran Siswa';

        $data['file_css']    = 'tabel.css';
        $data['page']        = 'guru/absen_ajar';
        $data['file_js']     = 'guru/absen_ajar';
        $this->load->view('auth/index', $data);
    }

    function ambil_hari(){
        $tanggal = date('D', strtotime($this->input->post('tanggal')));

        if($tanggal == 'Mon'){
            $hari = 'Senin';
        }else if($tanggal == 'Tue'){
            $hari = 'Selasa';
        }else if($tanggal == 'Wed'){
            $hari = 'Rabu';
        }else if($tanggal == 'Thu'){
            $hari = 'Kamis';
        }else if($tanggal == 'Fri'){
            $hari = 'Jumat';
        }else if($tanggal == 'Sat'){
            $hari = 'Sabtu';
        }else{
            $hari = 'Minggu';
        }

        echo json_encode([ 'data' => $hari ]);
    }

    function ambil_kelas(){
        $q = $this->input->post('q');

        $kelas = $this->db->query("SELECT * FROM master_kelas WHERE kelas LIKE '%$q%' AND deleted='0'")->result(); 
        echo json_encode([ 'kelas' => $kelas ]);
    }

    function ambil_pelajaran(){
        $q      = $this->input->post('q');
        $kelas  = $this->input->post('p');
        $hari   = $this->input->post('o');

        $data = $this->db->query("
            SELECT 
                master_pelajaran.id, 
                master_pelajaran.pelajaran 
            FROM 
                jadwal
            LEFT JOIN 
                master_pelajaran
            ON 
                jadwal.pelajaran = master_pelajaran.id
            WHERE
                master_pelajaran.pelajaran LIKE '%$q%' AND
                jadwal.hari='$hari' AND 
                jadwal.kelas='$kelas' AND 
                jadwal.deleted='0'
            ORDER BY jadwal.jam_mulai DESC
        ")->result(); 
        echo json_encode([ 'data' => $data ]);
    }

    function ambil_pengajar(){
        $q      = $this->input->post('q');
        $pelajaran  = $this->input->post('p');
        $kelas  = $this->input->post('r');

        $data = $this->db->query("
            SELECT 
                user.id, 
                user.nama 
            FROM 
                jadwal
            LEFT JOIN 
                user
            ON 
                jadwal.pengajar = user.id
            WHERE
                user.nama LIKE '%$q%' AND
                jadwal.kelas='$kelas' AND 
                jadwal.pelajaran='$pelajaran' AND 
                jadwal.deleted='0'
            ORDER BY id DESC
        ")->result(); 
        echo json_encode([ 'data' => $data ]);
    }

    function ambil_data(){
        $tanggal    = date('Y-m-d', strtotime($this->input->post('tanggal')));
        $kelas      = $this->input->post('kelas');
        $pelajaran  = $this->input->post('pelajaran');
        $pengajar   = $this->input->post('pengajar');

        $get = ['kelas' => $kelas, 'deleted' => '0'];
        $siswa = $this->main_model->Get_where('siswa', $get);

        $get2 = ['absen_siswa.tanggal' => $tanggal, 'absen_siswa.pelajaran' => $pelajaran, 'absen_siswa.kelas' => $kelas, 'absen_siswa.deleted' => '0'];
        $absen = $this->main_model->data_absen('absen_siswa', $get2);

        $jadwal = $this->db->query("SELECT * FROM jadwal WHERE kelas='$kelas' AND pelajaran='$pelajaran' AND deleted='0'")->row();

        $data = array();
        if(!empty($absen)){
            foreach ($absen as $key) {
                $data[] = array(
                    'id_siswa'      => $key->id_siswa,
                    'nisn'          => $key->nisn,
                    'nama'          => $key->nama,
                    'jenis_kelamin' => $key->jenis_kelamin,
                    'username'      => $key->username,
                    'jam_mulai'     => $jadwal->jam_mulai,
                    'jam_akhir'     => $jadwal->jam_akhir,
                    'jumlah_jam'    => $jadwal->jumlah_jam,
                    'status'        => $key->status,
                    'status2'       => $this->select_status($key->status)
                );
            }
        }else{
            foreach ($siswa as $key) {
                $data[] = array(
                    'id_siswa'      => $key->id,
                    'nisn'          => $key->nisn,
                    'nama'          => $key->nama,
                    'jenis_kelamin' => $key->jenis_kelamin,
                    'username'      => $key->username,
                    'jam_mulai'     => $jadwal->jam_mulai,
                    'jam_akhir'     => $jadwal->jam_akhir,
                    'jumlah_jam'    => $jadwal->jumlah_jam,
                    'status'        => '',
                    'status2'       => ''
                );
            }
        }

        echo json_encode([ 'data' => $data, 'jadwal' => $jadwal ]);
    }

    function select_status($kode){
        if($kode == 'KL'){
            $status = 'Kultum';
        }else if($kode == 'UP'){
            $status = 'Upacara';
        }else if($kode == 'AP'){
            $status = 'Apel';
        }else if($kode == 'X'){
            $status = 'Tidak hadir';
        }else if($kode == 'L'){
            $status = '9';
        }else if($kode == 'P'){
            $status = '10';
        }else if($kode == 'J'){
            $status = '19';
        }else if($kode == 'H'){
            $status = 'Hadir';
        }else if($kode == 'A'){
            $status = 'Alfa';
        }else if($kode == 'S'){
            $status = 'Sakit';
        }else{
            $status = '';
        }

        return $status;
    }

    function select2(){
        $data = [
            ['select_id' => 'KL', 'select_nama' => 'Kultum'],
            ['select_id' => 'UP', 'select_nama' => 'Upacara'],
            ['select_id' => 'AP', 'select_nama' => 'Apel'],
            ['select_id' => 'X', 'select_nama' => 'Tidak hadir'],
            ['select_id' => 'L', 'select_nama' => '9'],
            ['select_id' => 'P', 'select_nama' => '10'],
            ['select_id' => 'J', 'select_nama' => '19'],
            ['select_id' => 'H', 'select_nama' => 'Hadir'],
            ['select_id' => 'A', 'select_nama' => 'Alfa'],
            ['select_id' => 'S', 'select_nama' => 'Sakit'],
        ];

        echo json_encode([ 'select' => $data ]);
    }

    function cek_status(){
        $tanggal    = date('Y-m-d', strtotime($this->input->post('tanggal')));
        $kelas      = $this->input->post('kelas');
        $pelajaran  = $this->input->post('pelajaran');
        $pengajar   = $this->input->post('pengajar');
    
        $absen = $this->db->query("SELECT * FROM absen_siswa WHERE tanggal='$tanggal' AND kelas='$kelas' AND pelajaran='$pelajaran' AND pengajar='$pengajar' AND deleted='0'")->row();
        if(!empty($absen)){
            $data = 'ada';
        }else{
            $data = 'kosong';
        }

        $jadwal = $this->db->query("SELECT * FROM jadwal WHERE kelas='$kelas' AND pelajaran='$pelajaran' AND pengajar='$pengajar' AND deleted='0'")->row();

        echo json_encode([ 'data' => $data, 'jadwal' => $jadwal ]);        
    }

    function cek_akses(){
        $id_user    = $this->session->userdata('id');
        $pengajar   = $this->input->post('pengajar');

        if($id_user == $pengajar){
            $data = 'sama';
        }else{
            $data = 'beda';
        }

        echo json_encode([ 'data' => $data ]);
    }

    function simpan(){
        $tanggal    = date('Y-m-d', strtotime($this->input->post('tanggal')));
        $kelas      = $this->input->post('kelas');
        $pelajaran  = $this->input->post('pelajaran');
        $siswa      = $this->input->post('inp_id_siswa');
        $id_jadwal  = $this->input->post('id_jadwal');

        // $result = array();
        foreach($siswa AS $key => $val){
            if($val !=""){
                $id_siswa = $_POST['inp_id_siswa'][$key];
                $absen = $this->db->query("SELECT * FROM absen_siswa WHERE id_siswa='$id_siswa' AND tanggal='$tanggal' AND kelas='$kelas' AND pelajaran='$pelajaran' AND deleted='0'")->row();
                if(empty($absen)){
                    $jad = $this->db->query("SELECT * FROM jadwal WHERE id='$id_jadwal' AND deleted='0'")->row();
                    $jumlah_jam = $jad->jumlah_jam;

                    $result = array(
                        'id_siswa'      => $_POST['inp_id_siswa'][$key],
                        'username'      => $_POST['inp_username'][$key],
                        'id_jadwal'     => $id_jadwal,
                        'hari'          => $this->input->post('hari'),
                        'kelas'         => $this->input->post('kelas'),
                        'pelajaran'     => $this->input->post('pelajaran'),
                        'pengajar'      => $this->input->post('pengajar'),
                        'tanggal'       => date('Y-m-d', strtotime($this->input->post('tanggal'))),
                        'jam_mulai'     => $_POST['inp_jam_mulai'][$key],
                        'jam_akhir'     => $_POST['inp_jam_akhir'][$key],
                        'status'        => $_POST['inp_status'][$key],
                        'verified'      => $this->session->userdata('id'),
                        'date_verified' => date_create('now', timezone_open('Asia/Jakarta'))->format('Y-m-d H:i:s')
                    );
                    $save = $this->main_model->Insert_where('absen_siswa', $result);

                    /* simpan absen dua */
                    $jam_mulai = $_POST['inp_jam_mulai'][$key];
                    $jam_akhir = $_POST['inp_jam_mulai'][$key];
                    for($x=1;$x<=$jad->jumlah_jam;$x++){
                        $date2 = date_create($jam_akhir);
                        date_add($date2, date_interval_create_from_date_string('40 minute'));
                        $jam_akhir = date_format($date2, 'H:i:s');

                        $result = array(
                            'id_siswa'      => $_POST['inp_id_siswa'][$key],
                            'username'      => $_POST['inp_username'][$key],
                            'id_jadwal'     => $id_jadwal,
                            'hari'          => $this->input->post('hari'),
                            'kelas'         => $this->input->post('kelas'),
                            'pelajaran'     => $this->input->post('pelajaran'),
                            'pengajar'      => $this->input->post('pengajar'),
                            'tanggal'       => date('Y-m-d', strtotime($this->input->post('tanggal'))),
                            'jam_mulai'     => $jam_mulai,
                            'jam_akhir'     => $jam_akhir,
                            'status'        => $_POST['inp_status'][$key],
                            'verified'      => $this->session->userdata('id'),
                            'date_verified' => date_create('now', timezone_open('Asia/Jakarta'))->format('Y-m-d H:i:s')
                            );
                        $save = $this->main_model->Insert_where('absen_siswa2', $result);

                        $jam_mulai = $jam_akhir;
                        $jam_akhir = $jam_akhir;

                        // $date2 = date_create($jam_akhir);
                        // date_add($date2, date_interval_create_from_date_string('40 minute'));
                        // $jam_akhir = date_format($date2, 'H:i:s');
                    }
                    /* batas simpan absen dua */
                }else{
                    $update = array('status' => $_POST['inp_status'][$key]);
                    $updt = ['id_siswa' => $id_siswa, 'tanggal' => $tanggal, 'kelas' => $kelas, 'pelajaran' => $pelajaran ];
                    $save = $this->main_model->Update_where('absen_siswa', $update, $updt);
                    $save = $this->main_model->Update_where('absen_siswa2', $update, $updt);
                }
            }
        }
        
        if ($save > 0) {
            $id_user = $this->session->userdata('id');
            $aa = $this->db->query("SELECT * FROM absen_mengajar WHERE id_user='$id_user' AND tanggal='$tanggal' AND kelas='$kelas' AND pelajaran='$pelajaran' AND deleted='0'")->row();

            $mengajar = array(
                'id_user'       => $this->input->post('pengajar'),
                'id_jadwal'     => $id_jadwal,
                'hari'          => $this->input->post('hari'),
                'kelas'         => $this->input->post('kelas'),
                'pelajaran'     => $this->input->post('pelajaran'),
                'tanggal'       => date('Y-m-d', strtotime($this->input->post('tanggal'))),
                'jam_mulai'     => $this->input->post('jam_mulai'),
                'jam_akhir'     => $this->input->post('jam_akhir'),
                'created'       => $this->session->userdata('id'),
                'date_created'  => date_create('now', timezone_open('Asia/Jakarta'))->format('Y-m-d H:i:s')
            );
            if(empty($aa)){
                $this->main_model->Insert_where('absen_mengajar', $mengajar);
            }else{
                $updt = ['id_user' => $id_user, 'tanggal' => $tanggal, 'kelas' => $kelas, 'pelajaran' => $pelajaran ];
                $this->main_model->Update_where('absen_mengajar', $mengajar, $updt);
            }

            echo json_encode([
                'status' => 200,
                'keterangan' => 'Berhasil simpan data',
            ]);
        } else {
            echo json_encode([
                'status' => 500,
                'keterangan' => 'Gagal simpan data',
            ]);
        }
    }
}