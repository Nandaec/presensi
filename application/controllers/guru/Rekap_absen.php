<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekap_absen extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library(['akses']);
        $this->akses->cek_login();
    }

    public function index()
    {
        $data['teks']        = 'E-Absen';
        $data['title']       = 'Data Rekap Presensi Mengajar';
        $data['description'] = 'Halaman Data Rekap Presensi Mengajar';

        // $data['file_css']    = 'tabel.css';
        $data['page']        = 'guru/rekap_absen';
        $data['file_js']     = 'guru/rekap_absen';
        $this->load->view('auth/index', $data);
    }
}