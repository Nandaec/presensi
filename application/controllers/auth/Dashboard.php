<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library(['akses']);
        $this->akses->cek_login();
    }

    public function index()
	{
        $data['teks']        = 'Halaman Utama';
        $data['title']       = 'Dashboard';
        $data['description'] = 'Halaman Dashboard';

        $data['tot_pns']        = $this->db->query("SELECT * FROM user WHERE akses = 'PNS'")->num_rows();
        $data['tot_guru']       = $this->db->query("SELECT * FROM user WHERE akses = 'GURU'")->num_rows();
        $data['tot_tendik']     = $this->db->query("SELECT * FROM user WHERE akses = 'TENDIK'")->num_rows();
        $data['tot_operator']   = $this->db->query("SELECT * FROM user WHERE akses = 'OPERATOR'")->num_rows();
        $data['tot_siswa']      = $this->db->query("SELECT * FROM siswa WHERE deleted = '0'")->num_rows();
        $data['tot_kelas']      = $this->db->query("SELECT * FROM master_kelas WHERE deleted = '0'")->num_rows();

        $data['kelas'] = $this->db->query("SELECT * FROM master_kelas WHERE deleted ='0'")->result();
        $abs_siswa = $this->db->query("SELECT * FROM absen_siswa WHERE status='A' AND deleted='0' GROUP BY id_siswa  ORDER BY status DESC LIMIT 10")->result();
        foreach ($abs_siswa as $key) {
            $jumlah = $this->db->query("SELECT COUNT(status) as status FROM absen_siswa WHERE id_siswa='$key->id_siswa' AND status='A'")->row();
            $absen_siswa[] = array(
                'nama'      => $this->db->query("SELECT nama FROM siswa WHERE id='$key->id_siswa'")->row('nama'), 
                'jumlah'    => $jumlah->status, 
            );
        }
        $data['absen_siswa'] = $absen_siswa;

        $akses = $this->session->userdata('hak_akses');
        if($akses == 'PNS'){
            $set_menu_logo = ['logo_menu' => 'money'];
            $this->session->set_userdata($set_menu_logo);

            redirect('guru/absen_ajar');
        }else if ($akses == 'GURU' || $akses == 'TENDIK'){
            $set_menu_logo = ['logo_menu' => 'google-wallet'];
            $this->session->set_userdata($set_menu_logo);

            redirect('guru/absen_ajar');
        }else{
            $set_menu_logo = ['logo_menu' => 'google-wallet'];
            $this->session->set_userdata($set_menu_logo);

            $data['page']       = 'auth/dashboard';
            $data['file_js']    = 'auth/dashboard';
            $this->load->view('auth/index', $data);
        }
    }

    function setting(){
        $nik    = $this->input->post('nik');
        $temp   = $this->input->post('temp');

        $sql = "UPDATE sdm.user SET layar = '$temp' WHERE nik = '$nik'";
        $this->db->query($sql);

        $cek = $this->db->query("SELECT * FROM sdm.user WHERE nik='$nik'")->row();
        
        echo json_encode([ 'data' => $cek->layar ]);
    }

    // function setting($page){
    //     $nik    = $this->session->userdata('nik');
    //     $cek = $this->db->query("SELECT * FROM user WHERE nik='$nik'")->row();
    //     if($cek->layar == 'true'){
    //         $sql = "UPDATE user SET layar = 'false' WHERE nik = '$nik'";
    //     }else{
    //         $sql = "UPDATE user SET layar = 'true' WHERE nik = '$nik'";
    //     }
    //     $this->db->query($sql);

    //     redirect('auth/dashboard');
    // }
}