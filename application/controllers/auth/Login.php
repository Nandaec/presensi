<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function index()
    {
        $data['title']       = 'Login';
        $data['description'] = 'Halaman Login';

        if(isset($_POST['login'])){
        	$username = $this->input->post('username');
        	$password = $this->input->post('password');

            $user = $this->main_model->Get_rows('user', array('username' => $username));
            $siswa = $this->main_model->Get_rows('siswa', array('username' => $username));

        	if ($user) {
        		if ($password == $user->password) {
        			$set_sesi = [
                        'id'        => $user->id,
	        			'nama' 		=> $user->nama,
	        			'username' 	=> $user->username,
	        			'hak_akses' => $user->akses,
                        'logged_in' => TRUE
        			];
        			$this->session->set_userdata($set_sesi);

        			if (isset($set_sesi['hak_akses'])) {
        				redirect('auth/dashboard');
        			}
        		}else{
        			$this->session->set_flashdata('pesan', '<div class="alert alert-danger" role="alert"> <strong>Password salah. </strong></div>');
        			redirect('auth/login');
        		}
            }else if($siswa){
                if ($password == $siswa->password) {
                    $set_sesi = [
                        'id'        => $siswa->id,
                        'nama'      => $siswa->nama,
                        'username'  => $siswa->username,
                        'hak_akses' => $siswa->akses,
                        'logged_in' => TRUE
                    ];
                    $this->session->set_userdata($set_sesi);

                    if (isset($set_sesi['hak_akses'])) {
                        redirect('auth/dashboard');
                    }
                }else{
                    $this->session->set_flashdata('pesan', '<div class="alert alert-danger" role="alert"> <strong>Password salah. </strong></div>');
                    redirect('auth/login');
                }
        	}else{
        		$this->session->set_flashdata('pesan', '<div class="alert alert-danger" role="alert"> <strong>Username belum terdaftar. </strong></div>');
        		redirect('auth/login');
        	}
        }else{
        	$this->load->view('auth/login', $data);
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('id');
        $this->session->unset_userdata('nama');
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('hak_akses');
        $this->session->unset_userdata('logged_in');

        $this->session->set_flashdata('callout-success', 'Berhasil Logout.');
        redirect('auth/login');
    }
}