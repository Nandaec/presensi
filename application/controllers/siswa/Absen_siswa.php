<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Absen_siswa extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library(['akses']);
        $this->akses->cek_login();
    }

    public function index()
    {
        $data['teks']        = 'E-Absen';
        $data['title']       = 'Absen Siswa';
        $data['description'] = 'Halaman Absen Siswa';

        // $lokasi = $this->db->query("SELECT * FROM lokasi WHERE kategori='siswa' and deleted='0'")->row();
        // $latitude   = $lokasi->latitude;
        // $longitude  = $lokasi->longitude;

        // $this->load->library('googlemaps_ses');
        // $config=array();
        // $config['center']="-7.674266, 110.615246";
        // $config['zoom']=17;
        // $config['map_height']="500px";
        // $this->googlemaps_ses->initialize($config);
        // $marker=array();
        // $marker['position']="-7.674266, 110.615246";
        // $this->googlemaps_ses->add_marker($marker);
        // $data['map']=$this->googlemaps_ses->create_map();

        // $data['lat'] = '-7.674266';
        // $data['long'] = '110.615246';

        $data['page']        = 'siswa/absen_siswa';
        $data['file_js']     = 'siswa/absen_siswa';
        $this->load->view('auth/index', $data);
    }

    function get_location(){
        $lokasi = $this->db->query("SELECT * FROM lokasi WHERE kategori='siswa' and deleted='0'")->result();
        echo json_encode([
            'lokasi'  => $lokasi
        ]);
    }

    public function save()
    {
        $kelas      = $this->input->post('id_kelas');
        $pelajaran  = $this->input->post('pelajaran');
        $tanggal    = date_create('now', timezone_open('Asia/Jakarta'))->format('Y-m-d');
        $jam        = date_create('now', timezone_open('Asia/Jakarta'))->format('H:i:s');

        $get_jad = $this->db->query("SELECT * FROM jadwal WHERE kelas='$kelas' AND pelajaran='$pelajaran' AND deleted='0'")->row();

        if(!empty($get_jad)){
            $awal = $get_jad->jam_mulai;
            $akhir = $get_jad->jam_akhir;
            
            if($jam <= $awal){
                echo json_encode([
                    'status' => 500,
                    'keterangan' => 'Belum bisa absen',
                ]);
            }else if($jam >= $awal && $jam <= $akhir){
                $image = $this->input->post('image');
                $image = str_replace('data:image/jpeg;base64,','', $image);
                $image = base64_decode($image);
                $filename = 'image_'.time().'.png';
                file_put_contents(FCPATH.'/file/siswa/'.$filename,$image);
                $data = array(
                    'id_siswa'  => $this->session->userdata('id'),
                    'username'  => $this->session->userdata('username'),
                    'kelas'     => $this->input->post('id_kelas'),
                    'pelajaran' => $this->input->post('pelajaran'),
                    'latitude'  => $this->input->post('lat_saya'),
                    'longitude' => $this->input->post('long_saya'),
                    'nama_tempat' => $this->input->post('nama_tempat'),
                    'waktu'     => date_create('now', timezone_open('Asia/Jakarta'))->format('Y-m-d H:i:s'),
                    'tanggal'   => $tanggal,
                    'jam'       => $jam,
                    'sts_absen' => 'gis',
                    'image'     => $filename,
                );

                $res = $this->main_model->Insert_where('absen_siswa', $data);
                if ($res > 0) {
                    echo json_encode([
                        'status' => 200,
                        'keterangan' => 'Berhasil simpan data',
                    ]);
                } else {
                    echo json_encode([
                        'status' => 500,
                        'keterangan' => 'Gagal simpan data',
                    ]);
                }
            }else if($jam >= $akhir){
                echo json_encode([
                    'status' => 500,
                    'keterangan' => 'Waktu absen sudah lewat',
                ]);
            }else{
                echo json_encode([
                    'status' => 500,
                    'keterangan' => 'Time Out',
                ]);
            }
        }else{
            echo json_encode([
                'status' => 500,
                'keterangan' => 'Tidak ada jadwal mapel',
            ]);
        }
    }

    function ambil_kelas(){
        $id_user = $this->session->userdata('id');
        $kelas = $this->db->query("
            SELECT 
                master_kelas.id,
                master_kelas.kelas 
            FROM 
                siswa 
            LEFT JOIN 
                master_kelas
            ON 
                master_kelas.id = siswa.kelas
            WHERE 
                siswa.id='$id_user' AND 
                siswa.deleted='0'
            ")->row();  
        echo json_encode([ 'kelas' => $kelas ]);
    }

    function ambil_pelajaran(){
        $q = $this->input->post('q');

        $pelajaran = $this->db->query("SELECT * FROM master_pelajaran WHERE pelajaran LIKE '%$q%' AND deleted='0'")->result(); 
        echo json_encode([ 'pelajaran' => $pelajaran ]);
    }
}