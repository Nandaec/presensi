<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class History extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library(['akses']);
        $this->akses->cek_login();
    }

    public function index()
    {
        $data['teks']        = 'E-Absen';
        $data['title']       = 'Riwayat Absen';
        $data['description'] = 'Halaman Riwayat Absen Siswa';

        $data['page']        = 'siswa/history';
        $data['file_js']     = 'siswa/history';
        $this->load->view('auth/index', $data);
    }

    public function datatables()
    {
        $username = $this->session->userdata('username');
        $get = $this->main_model->get_data_absen_siswa();
        $data = array();
        $no = $_POST['start'];
        foreach ($get as $r) {
            $pelajaran = $this->db->query("SELECT * FROM master_pelajaran WHERE id='$r->pelajaran'")->row('pelajaran');
            $no++;
            $row = array();
            $folder = '../file/siswa/'.$r->image;

            // $row[] = "<img src='$folder' class='avatar'  style='height: 40px;width:auto;' alt='Avatar'>";
            $row[] = $no;
            $row[] = $r->waktu;
            $row[] = $pelajaran;
            $row[] = $r->latitude.', '.$r->longitude;
            $row[] = $r->id;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->main_model->absen_siswa_count_all(),
            "recordsFiltered" => $this->main_model->absen_siswa_count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    function get_data(){
        $id = $this->input->post('id');

        $data = $this->db->query("
            SELECT 
                absen_siswa.*,
                siswa.nisn,
                siswa.nama,
                master_kelas.kelas
            FROM 
                absen_siswa 
            LEFT JOIN 
                siswa
            ON 
                absen_siswa.id_siswa = siswa.id
            LEFT JOIN 
                master_kelas
            ON 
                absen_siswa.kelas = master_kelas.id
            WHERE 
                absen_siswa.id='$id'
        ")->row();

        echo json_encode([ 'data' => $data ]);
    }
}