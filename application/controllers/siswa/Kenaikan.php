<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kenaikan extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library(['akses']);
        $this->akses->cek_login();
    }

    public function index()
    {
        $data['teks']        = 'E-Absen';
        $data['title']       = 'Data Kenaikan Kelas';
        $data['description'] = 'Halaman Data Kenaikan Kelas';

        $data['page']        = 'siswa/kenaikan';
        $data['file_js']     = 'siswa/kenaikan';
        $this->load->view('auth/index', $data);
    }
}