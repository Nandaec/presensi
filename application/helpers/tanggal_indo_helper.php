<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if ( ! function_exists('bulantigadigit')){
    function bulantigadigit($bulan){
        switch ($bulan) {
            case '01':
                $bulan_ini = "Jan";
                break;
            case '02':
                $bulan_ini = "Feb";
                break;
            case '03':
                $bulan_ini = "Mar";
                break;
            case '04':
                $bulan_ini = "Apr";
                break;
            case '05':
                $bulan_ini = "Mei";
                break;
            case '06':
                $bulan_ini = "Jun";
                break;
            case '07':
                $bulan_ini = "Jul";
                break;
            case '08':
                $bulan_ini = "Agu";
                break;
            case '09':
                $bulan_ini = "Sep";
                break;
            case '10':
                $bulan_ini = "Okt";
                break;
            case '11':
                $bulan_ini = "Nov";
                break;
            case '12':
                $bulan_ini = "Des";
                break;
            default:
                $bulan_ini = "?";
                break;
        }
        return $bulan_ini;
    }
}
if ( ! function_exists('tanggalan')){
    function tanggalan($tanggal = null){
        if ($tanggal) {
            $tgl = date("Y-m-d", strtotime($tanggal));
        } else {
            $tgl = date("Y-m-d");
        }
        $pecah = explode("-",$tgl);
        $d = $pecah[2];
        $m = $pecah[1];
        $y = $pecah[0];
        switch ($m) {
            case '01':
                $bulan_ini = "Januari";
                break;
            case '02':
                $bulan_ini = "Februari";
                break;
            case '03':
                $bulan_ini = "Maret";
                break;
            case '04':
                $bulan_ini = "April";
                break;
            case '05':
                $bulan_ini = "Mei";
                break;
            case '06':
                $bulan_ini = "Juni";
                break;
            case '07':
                $bulan_ini = "Juli";
                break;
            case '08':
                $bulan_ini = "Agustus";
                break;
            case '09':
                $bulan_ini = "September";
                break;
            case '10':
                $bulan_ini = "Oktober";
                break;
            case '11':
                $bulan_ini = "November";
                break;
            case '12':
                $bulan_ini = "Desember";
                break;
            default:
                $bulan_ini = "Tidak di ketahui";
                break;
        }
        return $d.' '. $bulan_ini .' '.$y;
    }
}

if ( ! function_exists('haritanggalan')){
    function haritanggalan($tanggal = null){
        if ($tanggal) {
            $tgl = date("D-Y-m-d", strtotime($tanggal));
        } else {
            $tgl = date("D-Y-m-d");
        }
        $pecah = explode("-",$tgl);
        $d = $pecah[3];
        $m = $pecah[2];
        $y = $pecah[1];
        $h = $pecah[0];
        switch ($h) {
            case 'Sun':
                $hari_ini = "Minggu";
                break;
            case 'Mon':
                $hari_ini = "Senin";
                break;
            case 'Tue':
                $hari_ini = "Selasa";
                break;
            case 'Wed':
                $hari_ini = "Rabu";
                break;
            case 'Thu':
                $hari_ini = "Kamis";
                break;
            case 'Fri':
                $hari_ini = "Jumat";
                break;
            case 'Sat':
                $hari_ini = "Sabtu";
                break;
            default:
                $hari_ini = "Tidak di ketahui";
                break;
        }
        return $hari_ini.', '.$d.'-'. $m .'-'.$y;
    }
}