<?php
class Main_model extends CI_Model
{    
    function __construct()
    {
        parent::__construct();
    }

    function Get_all($table)
    {
    	return $this->db->get($table)->result();
    }
    function Get_where($table,$data)
    {
    	return $this->db->get_where($table,$data)->result();
    }
    function Get_rows($table,$data)
    {
    	return $this->db->get_where($table,$data)->row();
    }
    function Insert_where($table,$data)
    {
        $action = $this->db->insert($table, $data);  
        return $action;
    }
    public function Update_where($table, $data, $key)
    {
        $this->db->set($data);
        $this->db->where($key);
        return $this->db->update($table);
    }



    public function get_data_absen()
    {
        $this->master_absen();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }
    private function master_absen()
    {
        $this->db->select('*');
        $this->db->from('absen_pns');

        $this->db->where([
            'username' => $this->session->userdata('username'),
        ]);
        $this->db->order_by('id','DESC');
    }
    public function absen_count_filtered()
    {
        $this->master_absen();
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function absen_count_all()
    {
        $this->master_absen();
        $this->db->group_by('id');

        return $this->db->count_all_results();
    }



    public function get_data_absen_siswa()
    {
        $this->master_absen_siswa();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }
    private function master_absen_siswa()
    {
        $this->db->select('*');
        $this->db->from('absen_siswa');

        $this->db->where([
            'id_siswa' => $this->session->userdata('id'),
            'sts_absen' => 'gis',
        ]);
        $this->db->order_by('id','DESC');
    }
    public function absen_siswa_count_filtered()
    {
        $this->master_absen_siswa();
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function absen_siswa_count_all()
    {
        $this->master_absen_siswa();
        $this->db->group_by('id');

        return $this->db->count_all_results();
    }


    public function get_data_user()
    {
        $this->master_user();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }
    private function master_user()
    {
        $column_search = ['nama','nip'];
        $this->db->select('*');
        $this->db->from('user');

        $this->db->where([
            'deleted' => '0',
        ]);
        $this->db->order_by('id','ASC');

        $i = 0;
        foreach ($column_search as $item) {
            if ($this->input->post('cari')) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $this->input->post('cari'));
                } else {
                    $this->db->or_like($item, $this->input->post('cari'));
                }
                if (count($column_search) - 1 == $i) {
                    $this->db->group_end();
                }

            }
            $i++;
        }
    }
    public function user_count_filtered()
    {
        $this->master_user();
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function user_count_all()
    {
        $this->master_user();
        $this->db->group_by('id');

        return $this->db->count_all_results();
    }


    public function get_data_siswa()
    {
        $this->master_siswa();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }
    private function master_siswa()
    {
        $column_search = ['nama','nisn','kelas'];
        $this->db->select('*');
        $this->db->from('siswa');

        $this->db->where([
            'deleted' => '0',
        ]);
        $pencarian = [
            'kelas' => $this->input->post('kelas'),
        ];
        foreach($pencarian as $key => $val){
            if($val){
                $this->db->where($key, $val);
            }
        }
        $this->db->order_by('id','ASC');

        $i = 0;
        foreach ($column_search as $item) {
            if ($this->input->post('cari')) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $this->input->post('cari'));
                } else {
                    $this->db->or_like($item, $this->input->post('cari'));
                }
                if (count($column_search) - 1 == $i) {
                    $this->db->group_end();
                }

            }
            $i++;
        }
    }
    public function siswa_count_filtered()
    {
        $this->master_siswa();
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function siswa_count_all()
    {
        $this->master_siswa();
        $this->db->group_by('id');

        return $this->db->count_all_results();
    }


    public function get_data_pelajaran()
    {
        $this->master_pelajaran();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }
    private function master_pelajaran()
    {
        $column_search = ['pelajaran'];
        $this->db->select('*');
        $this->db->from('master_pelajaran');

        $this->db->where([
            'deleted' => '0',
        ]);
        $this->db->order_by('id','ASC');

        $i = 0;
        foreach ($column_search as $item) {
            if ($this->input->post('cari')) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $this->input->post('cari'));
                } else {
                    $this->db->or_like($item, $this->input->post('cari'));
                }
                if (count($column_search) - 1 == $i) {
                    $this->db->group_end();
                }

            }
            $i++;
        }
    }
    public function pelajaran_count_filtered()
    {
        $this->master_pelajaran();
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function pelajaran_count_all()
    {
        $this->master_pelajaran();
        $this->db->group_by('id');

        return $this->db->count_all_results();
    }


    public function get_data_jabatan()
    {
        $this->master_jabatan();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }
    private function master_jabatan()
    {
        $column_search = ['jabatan'];
        $this->db->select('*');
        $this->db->from('master_jabatan');

        $this->db->where([
            'deleted' => '0',
        ]);
        $this->db->order_by('id','ASC');

        $i = 0;
        foreach ($column_search as $item) {
            if ($this->input->post('cari')) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $this->input->post('cari'));
                } else {
                    $this->db->or_like($item, $this->input->post('cari'));
                }
                if (count($column_search) - 1 == $i) {
                    $this->db->group_end();
                }

            }
            $i++;
        }
    }
    public function jabatan_count_filtered()
    {
        $this->master_jabatan();
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function jabatan_count_all()
    {
        $this->master_jabatan();
        $this->db->group_by('id');

        return $this->db->count_all_results();
    }


    public function get_data_kelas()
    {
        $this->master_kelas();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }
    private function master_kelas()
    {
        $column_search = ['kelas'];
        $this->db->select('*');
        $this->db->from('master_kelas');

        $this->db->where([
            'deleted' => '0',
        ]);
        $this->db->order_by('id','ASC');

        $i = 0;
        foreach ($column_search as $item) {
            if ($this->input->post('cari')) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $this->input->post('cari'));
                } else {
                    $this->db->or_like($item, $this->input->post('cari'));
                }
                if (count($column_search) - 1 == $i) {
                    $this->db->group_end();
                }

            }
            $i++;
        }
    }
    public function kelas_count_filtered()
    {
        $this->master_kelas();
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function kelas_count_all()
    {
        $this->master_kelas();
        $this->db->group_by('id');

        return $this->db->count_all_results();
    }


    public function get_jadwal()
    {
        $this->master_jadwal();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }
    private function master_jadwal()
    {
        $column_search = ['jadwal.kelas','jadwal.pelajaran','jadwal.pengajar'];
        $this->db->select([
            'jadwal.id',
            // 'jadwal.kelas',
            // 'jadwal.pelajaran',
            'jadwal.pengajar',
            'jadwal.hari',
            'jadwal.jam_mulai',
            'jadwal.jam_akhir',
            'jadwal.jam',
            'jadwal.jumlah_jam',
            'master_kelas.kelas',
            'master_pelajaran.pelajaran',
            'user.nama',
        ]);
        $this->db->from('jadwal');
        $this->db->join('master_kelas', 'master_kelas.id = jadwal.kelas', 'left');
        $this->db->join('master_pelajaran', 'master_pelajaran.id = jadwal.pelajaran', 'left');
        $this->db->join('user', 'user.id = jadwal.pengajar', 'left');
        $this->db->where([
            'jadwal.deleted' => '0',
        ]);
        $pencarian = [
            'jadwal.hari'       => $this->input->post('hari'),
            'jadwal.kelas'      => $this->input->post('kelas'),
            'jadwal.pelajaran'  => $this->input->post('pelajaran'),
            'jadwal.pengajar'   => $this->input->post('pengajar'),
        ];
        foreach($pencarian as $key => $val){
            if($val){
                $this->db->where($key, $val);
            }
        }
        $this->db->order_by('kelas','ASC');
        $this->db->order_by('hari','DESC');
        $this->db->order_by('jam_mulai','ASC');

        $i = 0;
        foreach ($column_search as $item) {
            if ($this->input->post('cari')) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $this->input->post('cari'));
                } else {
                    $this->db->or_like($item, $this->input->post('cari'));
                }
                if (count($column_search) - 1 == $i) {
                    $this->db->group_end();
                }

            }
            $i++;
        }
    }
    public function jadwal_count_filtered()
    {
        $this->master_jadwal();
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function jadwal_count_all()
    {
        $this->master_jadwal();
        $this->db->group_by('id');

        return $this->db->count_all_results();
    }


    public function get_rekap_siswa()
    {
        $this->master_rekap_siswa();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }
    private function master_rekap_siswa()
    {
        $column_search = ['jadwal.kelas','jadwal.pelajaran','jadwal.pengajar'];
        $this->db->select([
            'jadwal.id',
            'jadwal.kelas as id_kelas',
            'jadwal.pelajaran as id_pelajaran',
            'jadwal.pengajar',
            'jadwal.hari',
            'jadwal.jam_mulai',
            'jadwal.jam_akhir',
            'master_kelas.kelas',
            'master_pelajaran.pelajaran',
            'user.nama',
        ]);
        $this->db->from('jadwal');
        $this->db->join('master_kelas', 'master_kelas.id = jadwal.kelas', 'left');
        $this->db->join('master_pelajaran', 'master_pelajaran.id = jadwal.pelajaran', 'left');
        $this->db->join('user', 'user.id = jadwal.pengajar', 'left');
        $this->db->where([
            'jadwal.hari' => $this->input->post('hari'),
            'jadwal.deleted' => '0',
        ]);
        $pencarian = [
            'jadwal.hari'       => $this->input->post('hari'),
            'jadwal.kelas'      => $this->input->post('kelas'),
            'jadwal.pelajaran'  => $this->input->post('pelajaran'),
            'jadwal.pengajar'   => $this->input->post('pengajar'),
        ];
        foreach($pencarian as $key => $val){
            if($val){
                $this->db->where($key, $val);
            }
        }
        $this->db->order_by('kelas','ASC');
        $this->db->order_by('hari','DESC');
        $this->db->order_by('jam_mulai','ASC');

        $i = 0;
        foreach ($column_search as $item) {
            if ($this->input->post('cari')) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $this->input->post('cari'));
                } else {
                    $this->db->or_like($item, $this->input->post('cari'));
                }
                if (count($column_search) - 1 == $i) {
                    $this->db->group_end();
                }

            }
            $i++;
        }
    }
    public function rekap_siswa_count_filtered()
    {
        $this->master_rekap_siswa();
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function rekap_siswa_count_all()
    {
        $this->master_rekap_siswa();
        $this->db->group_by('id');

        return $this->db->count_all_results();
    }


    public function get_rekap_guru()
    {
        $this->master_rekap_guru();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }
    private function master_rekap_guru()
    {
        $column_search = ['jadwal.tanggal'];
        $this->db->select([
            'absen_mengajar2.id',
            'absen_mengajar2.kelas',
            'absen_mengajar2.pengajar',
            'absen_mengajar2.tanggal',
            'absen_mengajar2.hari',
            'absen_mengajar2.satu',
            'absen_mengajar2.dua',
            'absen_mengajar2.tiga',
            'absen_mengajar2.empat',
            'absen_mengajar2.lima',
            'absen_mengajar2.enam',
            'absen_mengajar2.tujuh',
            'absen_mengajar2.delapan',
            'absen_mengajar2.sembilan',
            'absen_mengajar2.sepuluh',
            'absen_mengajar2.sebelas',
            'user.nama',
        ]);
        $this->db->from('absen_mengajar2');
        $this->db->join('user', 'user.id = absen_mengajar2.pengajar', 'left');
        $this->db->where([
            'absen_mengajar2.tanggal' => date('Y-m-d', strtotime($this->input->post('tanggal'))),
            'absen_mengajar2.deleted' => '0',
        ]);
        // $pencarian = [
        //     'jadwal.hari'       => $this->input->post('hari'),
        //     'jadwal.kelas'      => $this->input->post('kelas'),
        //     'jadwal.pengajar'   => $this->input->post('pengajar'),
        // ];
        // foreach($pencarian as $key => $val){
        //     if($val){
        //         $this->db->where($key, $val);
        //     }
        // }
        $this->db->order_by('absen_mengajar2.id','ASC');

        $i = 0;
        foreach ($column_search as $item) {
            if ($this->input->post('cari')) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $this->input->post('cari'));
                } else {
                    $this->db->or_like($item, $this->input->post('cari'));
                }
                if (count($column_search) - 1 == $i) {
                    $this->db->group_end();
                }

            }
            $i++;
        }
    }
    public function rekap_guru_count_filtered()
    {
        $this->master_rekap_guru();
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function rekap_guru_count_all()
    {
        $this->master_rekap_guru();
        $this->db->group_by('id');

        return $this->db->count_all_results();
    }


    public function get_absen_pns()
    {
        $this->master_absen_pns();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }
    private function master_absen_pns()
    {
        $column_search = ['user.nama','user.nip'];
        $this->db->select([
            'user.id',
            'user.nip',
            'user.nama',
            // 'absen_pns.image',
            // 'absen_pns.latitude',
            // 'absen_pns.longitude',
            // 'absen_pns.radius',
            // 'absen_pns.nama_tempat',
            // 'absen_pns.waktu',
        ]);
        $this->db->from('user');
        // $this->db->join('absen_pns', 'absen_pns.id_pns = user.id', 'left');
        $this->db->where([
            'user.akses !=' => 'OPERATOR',
            'user.deleted' => '0',
        ]);
        $this->db->order_by('user.id','ASC');

        $i = 0;
        foreach ($column_search as $item) {
            if ($this->input->post('cari')) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $this->input->post('cari'));
                } else {
                    $this->db->or_like($item, $this->input->post('cari'));
                }
                if (count($column_search) - 1 == $i) {
                    $this->db->group_end();
                }

            }
            $i++;
        }
    }
    public function absen_pns_count_filtered()
    {
        $this->master_absen_pns();
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function absen_pns_count_all()
    {
        $this->master_absen_pns();
        $this->db->group_by('id');

        return $this->db->count_all_results();
    }

    public function data_absen($table,$id)
    {
        $this->db->select([
            'absen_siswa.id_siswa',
            'absen_siswa.status',
            'siswa.username',
            'siswa.nisn',
            'siswa.nama',
            'siswa.jenis_kelamin',
        ]);
        $this->db->from($table);
        $this->db->join('siswa', 'siswa.id = absen_siswa.id_siswa', 'left');
        $this->db->where($id);
        $this->db->order_by('siswa.id','ASC');
        $query = $this->db->get();
        return $query->result();
    }
}